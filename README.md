
**项目目标**

这个项目可以提供一些工具类，及其详细使用方法

**后端技术栈**

springboot
mybaits

**数据库**

关系型数据库 mysql

**中间件技术**

redis
rabittmq

**运维技术**

服务器 centos7

**构建工具**

maven

**安装指南**

使用maven进行构建打成jar运行即可

