import com.alibaba.fastjson.JSON;
import com.springboot.bean.Person;
import com.springboot.bean.PersonBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class SpringbootApplicationTests {

    private static final Logger logger = LogManager.getLogger(SpringbootApplicationTests.class.getName());

    @Test
    public void test(){
        logger.info("1222222");
    }

    @Test
    public void test1(){
        String aa = "id,name,program,grade";
        String [] arr = aa.split(",");
        logger.info(Arrays.toString(arr));
        List<String> list = Arrays.asList(arr);
        String[] updateLookup = {"id","name","program","grade"};
        Integer[] ind = {1,2,3,4};
        logger.info(Arrays.toString(updateLookup));
    }

    @Test
    public void test2(){
        Person person = new PersonBuilder().setName("aaa").setAge(3).setPhone("1111").build();
        logger.info("test person {}", JSON.toJSONString(person));

    }

    @Test
    public void test3() throws Exception{
        Properties properties = new Properties();
        properties.loadFromXML(new FileInputStream("C:\\springboot\\src\\main\\resources\\test.xml"));
        String func = properties.getProperty("func");
        String test2 = properties.getProperty("test2");
        logger.info("log:{}",func);
        logger.info("log:{}",test2);

    }

}
