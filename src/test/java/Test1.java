import com.alibaba.fastjson.JSON;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.springboot.SpringbootApplication;
import com.springboot.bean.Book;
import com.springboot.quartz.MyJob;
import com.springboot.quartz.QuartzManager;
import com.springboot.service.OperationService;
import com.springboot.task.TaskTest;
import com.springboot.util.JasyptEncryptUtil;
import com.springboot.util.SpringBeanFactoryUtils;
import org.apache.commons.beanutils.PropertyUtilsBean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jasypt.encryption.StringEncryptor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;


/**
 * 启动Springboot 运行测试方法
 */
@SpringBootTest(classes = SpringbootApplication.class)
@RunWith(SpringRunner.class)
public class Test1 {

    private static final Logger logger = LogManager.getLogger(Test1.class.getName());

    @Test
    public void TestBoot(){
        logger.info(".....");
    }

    @Autowired
    private QuartzManager quartzManager;

    private final Long TIME_START = 2*1000L;


    //  测试操作    a
    @Test
    public void testOpA(){
        OperationService operationService = (OperationService) SpringBeanFactoryUtils.getBean("operationA");
        operationService.operation("123");
    }

    //  测试操作    b
    @Test
    public void testOpB(){
        OperationService operationService = (OperationService) SpringBeanFactoryUtils.getBean("operationB");
        operationService.operation("456");
    }

    @Test
    public void test1(){
        quartzManager.startJobs();
        Date oldDate = new Date();
        Long startTime = oldDate.getTime()+TIME_START;
        Date startDate = new Date(startTime);
        quartzManager.addJob("test1", MyJob.class,startDate);
        logger.info("。。。。。。");
    }

    @Test
    public void test2() throws Exception {
        Class<?> clazz = Class.forName("com.springboot.bean.Book");
        Field tag = clazz.getDeclaredField("TAG");
        tag.getType().getName();
    }

    public static void main(String[] args) {
        Book book = new Book();
        book.setAuthor("aaaaa");
        book.setName("bbbbb");

        try {
            Class<?> clazz = Class.forName("com.springboot.bean.Book");

            Object o = clazz.newInstance();

            Field tag = clazz.getDeclaredField("author");

            //AccessibleTest类中的成员变量为private,故必须进行此操作
            tag.setAccessible(true);
            tag.getType().getName();

            //获取字段的值
            tag.get(book);

            Method[] declaredMethods = clazz.getDeclaredMethods();
            Method testMethod = clazz.getDeclaredMethod("testMethod", int.class);
            testMethod.setAccessible(true);

            //调用方法
            Object invoke = testMethod.invoke(book,1);
            logger.info(String.valueOf(invoke));

            int a = 3|1;
            logger.info(a+"");

            Field[] declaredFields = clazz.getDeclaredFields();
            List<Object> list = Lists.newArrayList();
            for(Field field : declaredFields){
                field.setAccessible(true);
                Object o1 = field.get(book);
                list.add(o1);
            }

            logger.info(JSON.toJSONString(list));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Autowired
    StringEncryptor stringEncryptor;

    @Test
    public void testEncrypt(){

        String s = JasyptEncryptUtil.encrypt("12345678poi");
        logger.info(s);

        String decrypt = JasyptEncryptUtil.desEncrypt(s);
        logger.info(decrypt);
    }

    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    @Test
    public void testRedis(){

        redisTemplate.opsForValue().set("1234","test");
        redisTemplate.opsForValue().set("1234","test2");

        String s = redisTemplate.opsForValue().get("1234");
        logger.info(s);
    }

    /**
     * 可用于延时队列
     */
    @Test
    public void testRedisDelayQueue(){
        //设置key  value   分值（时间戳）
        redisTemplate.opsForZSet().add("delay-queue","test1",123459);
        redisTemplate.opsForZSet().add("delay-queue","test2",123461);

        //根据范围获取 value     123460 为当前时间戳，取出过期的任务。   需要依靠定时任务，每秒轮询
        Set<String> strings = redisTemplate.opsForZSet().rangeByScore("delay-queue", 1, 123460);
        logger.info(JSON.toJSONString(strings));


        //根据分值移除区间元素。
        //redisTemplate.opsForZSet().removeRangeByScore("delay-queue", 1, 123460);
    }


}
