package com.springboot.redis.listener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
public class MessageReceiveHandler {

    private static final Logger logger = LogManager.getLogger(MessageReceiveHandler.class.getName());

    public void messagePush(String message){

        logger.info("----------收到消息了message：{}", message);

    }
}
