package com.springboot.redis.listener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationListener;
import org.springframework.data.redis.core.RedisKeyExpiredEvent;
import org.springframework.stereotype.Component;


/**
 *
 * 延时队列 消息接收
 *
 *
 *
 * redis  设置消息
 * 直接通过Redis命令设置消息，就没通过代码发送消息了，消息的key为sanyou，值为task，值不重要，过期时间为5s
 * set sanyou task
 * expire sanyou 5
 *
 * MyRedisKeyExpiredEventListener  会接收到消息  sanyou
 *
 * @author 29678
 */
@Component
public class MyRedisKeyExpiredEventListener implements ApplicationListener<RedisKeyExpiredEvent> {

    private static final Logger logger = LogManager.getLogger(MyRedisKeyExpiredEventListener.class.getName());

    @Override
    public void onApplicationEvent(RedisKeyExpiredEvent event) {
        byte[] source = event.getSource();
        logger.info("获取到的延时消息：{}", new String(source));
    }
}
