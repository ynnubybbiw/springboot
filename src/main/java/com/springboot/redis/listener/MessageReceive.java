package com.springboot.redis.listener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class MessageReceive {

    private static final Logger logger = LogManager.getLogger(MessageReceive.class.getName());

    @Autowired
    private MessageReceiveHandler messageReceiveHandler;

    /**接收消息的方法*/
    public void receiveMessage(String message){
        logger.info("接收到redis消息：{}",message);
        messageReceiveHandler.messagePush(message);
    }
}
