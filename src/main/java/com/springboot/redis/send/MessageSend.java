package com.springboot.redis.send;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

@Component
public class MessageSend {

    @Autowired
    private StringRedisTemplate redisTemplate;

    public void send(String data){
        redisTemplate.convertAndSend("messagepush", data);
        redisTemplate.convertAndSend("messagepush3","qweasd");
    }
}
