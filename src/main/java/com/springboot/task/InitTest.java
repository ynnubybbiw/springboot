package com.springboot.task;

import com.springboot.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@Component
@Order(value = 2)
public class InitTest implements CommandLineRunner {


    @Autowired
    private IUserService userService;

    @Override
    public void run(String... args) {

        new Thread(() -> {
            try {
        Properties properties = new Properties();
        // 使用ClassLoader加载properties配置文件生成对应的输入流
        InputStream in = InitTest.class.getClassLoader().getResourceAsStream("book.properties");
        // 使用properties对象加载输入流
        properties.load(in);
        //获取key对应的value值
        System.out.println(properties.getProperty("book.price"));
    } catch (IOException e) {
        e.printStackTrace();
    }

            /*User user = new User();
            user.setUserName("ttyyyttyy");
            user.setUserPassword("123ttttrr");
            user.setCreateTime(new Date());
            user.setUpdateTime(new Date());
            int t = userService.addUser(user);
            System.out.println("ddddddddddddddddddd"+t);*/
        }).start();
    }
}
