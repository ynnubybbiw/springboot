package com.springboot.task;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 使用定时任务，需要在启动类上添加注解    @EnableScheduling
 */
@Component
public class TaskTest {

    private static final Logger logger = LogManager.getLogger(TaskTest.class.getName());


    //间隔 5秒钟执行一次
    // @Scheduled(fixedRate = 60 * 1000)
    public void test1(){
        logger.info("test1........................{}",getTime());
    }


    //执行完后，间隔5秒再执行
    // @Scheduled(fixedDelay = 60 * 1000)
    public void test2(){
        logger.info("test2........................{}",getTime());
    }

    //每小时10分后执行
    // @Scheduled(cron = "0 10 * * * *")
    public void test3(){
        logger.info("test3........................{}",getTime());
    }

    public String getTime(){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime localDateTime = LocalDateTime.now();


        //LocalDate  本地日期（没有时间，没有时区）
        //LocalTime  本地时间（没有日期，没有时区）
        //LocalDateTime   本地日期时间（没有时区）
        return localDateTime.format(formatter);
    }

    @Test
    public void getTime2(){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime localDateTime = LocalDateTime.now();

        //获取所有时区id
        Set<String> zone = ZoneId.getAvailableZoneIds();
        logger.info(zone);

        ZonedDateTime zonedDateTime = localDateTime.atZone(ZoneId.of("Asia/Shanghai"));

        logger.info(zonedDateTime.format(formatter));

        logger.info(localDateTime.format(formatter));

        int aa = 1/0;

    }

    @Test
    public void testP(){
        /*Long a = 129433456L;
        Long b = 380633136L;

        Double c = a* 1.0/b*100;
        logger.info(c);
        logger.info(String.format("%.0f",c));*/

        double cd = 100.30040222246967;
        double aa = 100.0;
        if(cd > aa || cd == aa){
            logger.info("dsdsdsdsd");
        }

        //total 50016|-rw-r-----. 1 cmz cmz   196656 May  6 15:45 054a54cf-f51a-473c-b9cb-f61b001c85a3|-rw-r-----. 1 cmz cmz      291 May  6 15:45 054a54cf-f51a-473c-b9cb-f61b001c85a3.txt|-rw-r-----. 1 cmz cmz 51003516 May  6 15:45 064c3de2-2ac3-4ac5-94c1-4474e3aa3111|-rw-r-----. 1 cmz cmz      384 May  6 15:45 064c3de2-2ac3-4ac5-94c1-4474e3aa3111.txt|

        String ss = "-rw-r-----. 1 cmz cmz   196656 May  6 15:45 054a54cf-f51a-473c-b9cb-f61b001c85a3";

        String[] strarr = ss.split(" ");

        List<String> strings = Lists.newArrayList();
        List<String> collect = Arrays.stream(strarr).filter(StringUtils::isNotBlank).collect(Collectors.toList());

        logger.info(collect);

    }


}
