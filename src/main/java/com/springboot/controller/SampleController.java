package com.springboot.controller;


import cn.hutool.core.net.Ipv4Util;
import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.springboot.aspect.CountExecTime;
import com.springboot.aspect.Logs;
import com.springboot.bean.*;
import com.springboot.drools.TransactionEvent;
import com.springboot.exception.CustomizException;
import com.springboot.listener.ExcelListener;
import com.springboot.liteflow.LiteflowConfig;
import com.springboot.pool.PoolUtil;
import com.springboot.redis.send.MessageSend;
import com.springboot.sender.RabbitmqSenderOne;
import com.springboot.service.ExcelVmService;
import com.springboot.service.MenuService;
import com.springboot.service.NetPoolNativeIpService;
import com.springboot.util.CIDRUtils;
import com.springboot.util.CommonUtil;
import com.springboot.util.ValidateUtil;
import com.yomahub.liteflow.core.FlowExecutor;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.kie.api.KieBase;
import org.kie.api.KieServices;
import org.kie.api.logger.KieRuntimeLogger;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.SetOperations;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Api(value = "sample Interfaces", tags = "sample Interfaces")
@RestController
@RequestMapping(path = "/sample", produces = {"application/json;charset=UTF-8"})
public class SampleController {

    public static final Logger logger = LogManager.getLogger(SampleController.class.getName());

    @Resource
    private BookBean bookBean;

    @Resource
    private RabbitmqSenderOne rabbitmqSenderOne;

    @Autowired
    private RedisTemplate<String,String> redisTemplate;

    @Autowired
    private NetPoolNativeIpService netPoolNativeIpService;

    @ApiOperation("test")
    @Logs
    @GetMapping("/test")
    public String home(HttpServletRequest request, HttpServletResponse response) {
        logger.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>> 执行方法中");
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("test","Hello World");
        return JSON.toJSONString(jsonObject);
    }

    @ApiOperation("book")
    @CountExecTime
    @GetMapping(value = "/book",produces = "text/plain;charset=UTF-8")
    public String book() {
        Student student = new Student();
        student.setName("2345");

        ValidateUtil.validate(student);

        rabbitmqSenderOne.send("test123456");

        return "Hello Spring Boot! The BookName is "+bookBean.getName()+";and Book Author is "+bookBean.getAuthor()+";and Book price is "+bookBean.getPrice();
    }


    @GetMapping(value = "/testRabbitmqSend")
    public String testRabbitmqSend(@RequestParam("msg") String msg) {

        logger.info("开始发送rabbitmq消息");
        rabbitmqSenderOne.send(msg);
        logger.info("消息发送完成");

        return CommonUtil.successJsonStr();
    }

    @Autowired
    private MessageSend messageSend;

    @PostMapping(value="testRedis")
    public String testRedis(@RequestBody JSONObject data){
        messageSend.send(JSON.toJSONString(data));
        return CommonUtil.successJsonStr();
    }


    @Autowired
    private MenuService menuService;
    @Autowired
    private ExcelVmService excelVmService;


    @PostMapping("uploadExcel")
    public String upload(@RequestParam("file") MultipartFile file) {
        JSONObject jsonObject = new JSONObject();
        try {
            ExcelListener excelListener = new ExcelListener();
            EasyExcelFactory.read(file.getInputStream(), excelListener).sheet().doRead();

            Map<Integer, String> importHeads = excelListener.getImportHeads();
            List<JSONObject> dataList = excelListener.getDataList();
            jsonObject.put("header",importHeads);
            jsonObject.put("dateList",dataList);

            List<Menu> menuList = menuService.findAll();
            Map<String, String> defaultMenu = menuList.stream().collect(Collectors.toMap(Menu::getName, Menu::getCode));
            List<String> defaultMenuCodeList = menuList.stream().filter(menu -> Boolean.FALSE.equals(menu.getAdd())).map(Menu::getCode).collect(Collectors.toList());

            List<String> menuCodeList = Lists.newLinkedList();
            importHeads.forEach((k,v)->{
                String code = defaultMenu.get(v);
                Assert.notNull(code,"表头跟菜单不一致！");
                menuCodeList.add(code);
            });

            List<ExcelVm> excelVmList = Lists.newArrayList();
            dataList.forEach(jsonObject1 -> {
                JSONObject dataJson = new JSONObject();
                menuCodeList.forEach(menuCode -> {
                    Object o = jsonObject1.get(menuCodeList.indexOf(menuCode));
                    if(null != o){
                        dataJson.put(menuCode,o);
                    }
                });
                ExcelVm excelVm = JSON.parseObject(JSON.toJSONString(dataJson), ExcelVm.class);
                defaultMenuCodeList.forEach(dataJson::remove);
                if(!dataJson.isEmpty()){
                    excelVm.setData(JSON.toJSONString(dataJson));
                }
                excelVmList.add(excelVm);
            });

            excelVmList.forEach(excelVm -> excelVmService.saveOrUpdate(excelVm));


            logger.info("");
        } catch (IOException e) {
            logger.error(e.getMessage(),e);
            throw new CustomizException(e.getMessage());
        }
        return CommonUtil.successJsonStr(jsonObject);
    }

    @PostMapping("uploadFile")
    public String uploadFile(@RequestParam("file") MultipartFile file){
        try {
            String fileName = file.getOriginalFilename();
            File uploadedFile = new File("d:\\uploads", fileName);
            file.transferTo(uploadedFile);
        } catch (IOException e) {
            logger.error("上传错误：",e);
        }
        return CommonUtil.successJsonStr();
    }



    @GetMapping("exceExcel")
    public void exceExcel(HttpServletResponse response) throws IOException {

        List<Menu> menuList = menuService.findAll();

        List<String> menuCodeList = menuList.stream().filter(menu -> Boolean.TRUE.equals(menu.getAdd())).map(Menu::getCode).collect(Collectors.toList());

        List<ExcelVm> excelVmList = excelVmService.findAll();

        List<Object> newExcelVmList = Lists.newArrayList();

        if(!menuCodeList.isEmpty()){
            excelVmList.forEach(excelVm -> {
                JSONObject excelVmJson = JSON.parseObject(JSON.toJSONString(excelVm));
                String data = excelVm.getData();

                menuCodeList.forEach(k -> {
                    Object val = null;
                    if(StringUtils.isNotEmpty(data)){
                        JSONObject dataJson = JSON.parseObject(data);
                        val = dataJson.get(k);
                    }
                    excelVmJson.put(k,val);
                });
                newExcelVmList.add(excelVmJson);
            });
        }else {
            newExcelVmList.addAll(excelVmList);
        }

        JSONObject jsonone = new JSONObject();
        jsonone.put("menuList",menuList);
        jsonone.put("excelVmList",newExcelVmList);

        //生成表头
        List<List<String>> resMenu = Lists.newArrayList();
        menuList.forEach(menu -> {
            List<String> menuName = Lists.newArrayList();
            menuName.add(menu.getName());
            resMenu.add(menuName);
        });

        //生成表数据
        List<List<Object>> resData = Lists.newArrayList();
        newExcelVmList.forEach(o -> {
            List<Object> list = Lists.newArrayList();
            JSONObject newExcelVm = JSON.parseObject(JSON.toJSONString(o), JSONObject.class);
            menuList.forEach(menu -> list.add(newExcelVm.get(menu.getCode())));
            resData.add(list);
        });

        response.reset();
        response.setContentType("application/x-download");
        String fileName = "测试导出.xlsx";
        response.addHeader("Content-Disposition", "inline;filename=" + URLEncoder.encode(fileName, "UTF-8"));
        //response.addHeader("Content-Length", "" + file.length());

        ServletOutputStream outputStream = response.getOutputStream();
        //String fileName = "C:\\Users\\29678\\Desktop\\test\\测试.xlsx";
        EasyExcelFactory.write(outputStream).head(resMenu).sheet("资产清单").doWrite(resData);

        outputStream.flush();
    }

    /**
     * 测试redis锁
     * @return
     */
    @GetMapping("testLock")
    public String testLock(){
        Boolean lock = redisTemplate.opsForValue().setIfAbsent("test", "123456", 60, TimeUnit.SECONDS);
        logger.info("获取redis锁：lock： {}",lock);

        for(int i=0;i<=100; i++){
            lock = redisTemplate.opsForValue().setIfAbsent("test", "123456", 60, TimeUnit.SECONDS);
            logger.info("获取redis锁：lock： {}",lock);
            if(Boolean.TRUE.equals(lock)){
                break;
            }
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                logger.error("aaaa",e);
                Thread.currentThread().interrupt();
            }
        }
        logger.info("获取redis锁：lock： {} 执行完成",lock);
        return "";
    }


    @GetMapping("redisCount")
    public String redisCount(){
        int count = 1;
        String key = "key11";
        Boolean existsFlag = redisTemplate.opsForValue().setIfAbsent(key,String.valueOf(count),5, TimeUnit.MINUTES);

        logger.info("setIfAbsent  是否成功：{}",existsFlag);


        if(Boolean.FALSE.equals(existsFlag)){
            Long test11 = redisTemplate.opsForValue().increment(key, 1);
            logger.info("increment: {}", test11);
        }

        //redisTemplate.expire(key,10,TimeUnit.MINUTES);


        String test1 = redisTemplate.opsForValue().get(key);
        logger.info("test1:{}",test1);
        return CommonUtil.successJsonStr();
    }


    @GetMapping("redisSet")
    public String redisSet(@RequestParam(value = "param")  String param){
        /*User user = new User();
        user.setUserName(param);*/
        SetOperations stringObjectSetOperations = redisTemplate.opsForSet();
        //stringObjectSetOperations.add("uploadId", JSON.toJSONString(user));
        stringObjectSetOperations.add("uploadId", param);
        Set<Object> members = stringObjectSetOperations.members("uploadId");
        logger.info("result：{}", JSON.toJSONString(members));

        stringObjectSetOperations.remove("uploadId", "1237ggg");

        //redisTemplate.expire("uploadId",100,TimeUnit.SECONDS);

        //redisTemplate.delete("123321");

        //直接删除数据
        //redisTemplate.delete("uploadId");
        return CommonUtil.successJsonStr(members);
    }



    @GetMapping("getIpPageByIpAndMask")
    public String getIpPageByIpAndMask(
            @RequestParam(value = "ip")    String ip,
            @RequestParam(value = "mask")  Integer mask,
            @RequestParam(value = "page")  Long page,
            @RequestParam(value = "size")  Long size

    ){

        try {

            List<String> list = Lists.newArrayList();

            String beginIpStr = Ipv4Util.getBeginIpStr(ip, mask);
            String endIpStr = Ipv4Util.getEndIpStr(ip, mask);
            logger.info("beginIpStr {} ; endIpStr : {}", beginIpStr,endIpStr);
            long startIpNum = CIDRUtils.ipToLong(beginIpStr);
            long endIpNum = CIDRUtils.ipToLong(endIpStr);

            //每页开始数
            long start = (page-1)*size;

            //去掉开始和结束的ip
            long startIpPageNum = startIpNum+start+1;
            endIpNum = endIpNum-1;

            for(long i=startIpPageNum; i< startIpPageNum+size; i++){
                logger.info("i  为  {};   结束的ip数为：{}",i, endIpNum);
                String ipAddress = CIDRUtils.longToIPAddress(i);
                logger.info("生成的ip为：{}; ", ipAddress);
                if(i>endIpNum){
                    break;
                }

                NetPoolNativeIp netPoolNativeIp = new NetPoolNativeIp();
                netPoolNativeIp.setIp(ipAddress);
                netPoolNativeIp.setPlatformId("2c90909c87b618690187e623ce9f0005");
                netPoolNativeIp.setNetpoolid("66c36e63e2d74739a04bd5bf4b9d67d5");
                netPoolNativeIp.setStatus("2");

                netPoolNativeIpService.add(netPoolNativeIp);
                list.add(ipAddress);
            }
            return CommonUtil.successJsonStr(list);
        } catch (Exception e) {
            logger.error("错误：", e);
        }
        return "";
    }

    @GetMapping("poolutil")
    public String poolutil(@RequestParam(value = "param",required = false)  String param) throws Exception{

        Book book = PoolUtil.getConnectionFromPool(param);

        Thread.sleep(60*1000);

        PoolUtil.returnConnectionToPool(param,book);
        return null;
    }

    @GetMapping(value = "download")
    public void downloadDoc(HttpServletResponse response) throws UnsupportedEncodingException {
        String filePath = "C:\\Users\\29678\\Desktop\\CentOS-6.1-x86_64-minimal.iso";
        File file = new File(filePath);
        if (!file.exists()) {
            throw new CustomizException("文件不存在");
        }

        // 清空response
        response.reset();
        response.setContentType("application/x-download");
        // 设置response的Header
        // 转码之后下载的文件不会出现中文乱码
        String fileName = "testdel.iso";
        response.addHeader("Content-Disposition", "inline;filename=" + URLEncoder.encode(fileName, "UTF-8"));
        response.addHeader("Content-Length", "" + file.length());

        try(
                OutputStream output = response.getOutputStream();
                FileInputStream fis = new FileInputStream(filePath)
        ){
            byte[] b = new byte[1024];
            int i;
            while ((i = fis.read(b)) != -1) {
                output.write(b, 0, i);
            }
            output.flush();
        }catch (IOException e){
            logger.error("导出文件异常：", e);
        }
    }

    @Resource
    private FlowExecutor flowExecutor;

    @GetMapping("testChain")
    public String testChain(@RequestParam(value = "str") String str){

        String chain = "123456";
        //LiteflowConfig.getLiteflowChain(chain,str);

        LiteflowConfig.getFreedomChain2(chain);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("str",str);
        jsonObject.put("aaa","one");
        jsonObject.put("bbb","two");
        jsonObject.put("ccc","three");
        //LiteflowResponse response = flowExecutor.execute2Resp(chain, jsonObject);

        Book book = new Book();
        book.setTest("test");
        book.setAuthor("aaaa");
        book.setName("bbbb");

        flowExecutor.execute2Future(chain, book,Book.class);

        return "success";
    }

    @Resource
    public KieBase kieBase;

    @PostMapping("testDrools")
    public String testDrools(@RequestBody Data dataParam){
        KieSession kieSession = kieBase.newKieSession();

        Data data = new Data();
        List<String> bladeNodeList = Lists.newArrayList();
        bladeNodeList.add("node1");
        bladeNodeList.add("node2");
        data.setBladeNodeList(bladeNodeList);

        List<String> boxNodeList = Lists.newArrayList();
        boxNodeList.add("node1");
        boxNodeList.add("node2");
        boxNodeList.add("node3");
        boxNodeList.add("node4");
        data.setBoxNodeList(boxNodeList);

        List<String> cabinetNodeList = Lists.newArrayList();
        cabinetNodeList.add("node1");
        cabinetNodeList.add("node2");
        cabinetNodeList.add("node3");
        cabinetNodeList.add("node4");
        cabinetNodeList.add("node5");
        cabinetNodeList.add("node6");
        cabinetNodeList.add("node7");
        cabinetNodeList.add("node8");
        data.setCabinetNodeList(cabinetNodeList);



        data.getTargetNodeList().addAll(dataParam.getTargetNodeList());

        //logger.info("整刀：{}",data.getBladeNodeList().containsAll(data.getTargetNodeList()));
        //logger.info("整箱：{}",data.getBoxNodeList().containsAll(data.getTargetNodeList()));
        //logger.info("整柜：{}",data.getCabinetNodeList().containsAll(data.getTargetNodeList()));

        kieSession.insert(data);

        KieRuntimeLogger logger = KieServices.Factory.get().getLoggers().newFileLogger(kieSession, "rules-log");


        kieSession.fireAllRules();
        kieSession.dispose();
        logger.close();

        return "success";
    }



    @GetMapping("testDroolsCep")
    public String testDroolsCep() throws InterruptedException {
        KieSession kieSession = kieBase.newKieSession();

        kieSession.setGlobal("number",1000d);

        // 模拟插入一些交易事件
        kieSession.insert(new TransactionEvent("user1", 900, System.currentTimeMillis()));
        kieSession.insert(new TransactionEvent("user1", 1500, System.currentTimeMillis() + 1000));
        kieSession.insert(new TransactionEvent("user1", 2000, System.currentTimeMillis() + 5000));
        kieSession.insert(new TransactionEvent("user1", 3000, System.currentTimeMillis() + 6000));


        Thread.sleep(12*1000);

        kieSession.insert(new TransactionEvent("user1", 4000, System.currentTimeMillis()));

        // 执行规则
        kieSession.fireAllRules();
        kieSession.dispose();

        return "success";
    }




    @GetMapping("testDroolsCepTest1")
    public String testDroolsCepTest1() throws InterruptedException {
        KieSession kieSession = kieBase.newKieSession();

        List<String> sourceNodeList = Lists.newArrayList();
        sourceNodeList.add("node1");
        sourceNodeList.add("node2");
        sourceNodeList.add("node3");
        sourceNodeList.add("node4");

        kieSession.setGlobal("sourceNodeList",sourceNodeList);

        // 模拟插入一些交易事件
        kieSession.insert(new Data("node1", System.currentTimeMillis()));
        kieSession.insert(new Data("node2", System.currentTimeMillis() + 5000));
        kieSession.insert(new Data("node3", System.currentTimeMillis() + 5000));


        kieSession.insert(new Data("node4", System.currentTimeMillis() + 6000));

        // 执行规则
        kieSession.fireAllRules();
        kieSession.dispose();

        return "success";
    }

    @GetMapping("testDroolsCepTest2")
    public String testDroolsCepTest2() throws InterruptedException {
        KieSession kieSession = kieBase.newKieSession();

        EventA eventA = new EventA();
        eventA.setTimestamp(System.currentTimeMillis());
        kieSession.insert(eventA);

        // 等待一段时间，然后插入 EventB
        Thread.sleep(10000);

        EventB eventB = new EventB();
        eventB.setTimestamp(System.currentTimeMillis());
        kieSession.insert(eventB);

        // 执行规则
        kieSession.fireAllRules();
        kieSession.dispose();

        return "success";
    }

    @GetMapping("testDroolsCepTest3")
    public String testDroolsCepTest3() throws InterruptedException {
        KieSession kieSession = kieBase.newKieSession();

        EventA eventA = new EventA();
        eventA.setTimestamp(System.currentTimeMillis());
        kieSession.insert(eventA);

        // 等待一段时间，然后插入 EventB
        //Thread.sleep(10000);

        for (int i = 0; i < 3; i++) {
            EventB eventB = new EventB();
            eventB.setTimestamp(System.currentTimeMillis());
            kieSession.insert(eventB);
        }


        Thread.sleep(3000);
        for (int i = 0; i < 2; i++) {
            EventC eventC = new EventC();
            eventC.setTimestamp(System.currentTimeMillis());
            kieSession.insert(eventC);
        }

        // 执行规则
        kieSession.fireAllRules();
        kieSession.dispose();

        return "success";
    }

    @GetMapping("testDroolsCepTest4")
    public String testDroolsCepTest4() throws InterruptedException {
        KieSession kieSession = kieBase.newKieSession();

        for (int i = 0; i < 5; i++) {
            EventD event = new EventD();
            event.setResourceId("someId");  // 假设都使用相同的 resourceId
            event.setAlarmName("someAlarm");  // 假设都使用相同的 alarmname
            event.setTimestamp(System.currentTimeMillis());
            kieSession.insert(event);
        }

        Thread.sleep(10*1000);
        for (int i = 0; i < 3; i++) {
            EventD event = new EventD();
            event.setResourceId("someId");  // 假设都使用相同的 resourceId
            event.setAlarmName("someAlarm");  // 假设都使用相同的 alarmname
            event.setTimestamp(System.currentTimeMillis());
            kieSession.insert(event);
        }

        // 执行规则
        kieSession.fireAllRules();
        kieSession.dispose();

        return "success";
    }

    private static final String TASK_QUEUE = "taskQueue";

    @GetMapping("testRedisListPush")
    public String testRedisList(){
        for(int i=0;i<5;i++){
            Book book = new Book();
            book.setName("book"+i);
            redisTemplate.opsForList().leftPush(TASK_QUEUE, JSON.toJSONString(book));
        }
        return "success";
    }

    @GetMapping("testRedisListPop")
    public String testRedisPop(){
        //String task = redisTemplate.opsForList().rightPop(TASK_QUEUE);

        //取出第一个元素
        String task = redisTemplate.opsForList().index(TASK_QUEUE,-1);

        //删除第一个元素
        redisTemplate.opsForList().rightPop(TASK_QUEUE);
        return task;
    }

    @GetMapping("testRedisListRemove")
    public String testRedisListRemove(){
        //String task = redisTemplate.opsForList().rightPop(TASK_QUEUE);

        //取出第一个元素
        String task = redisTemplate.opsForList().index(TASK_QUEUE,-1);

        //删除第一个元素
        redisTemplate.opsForList().remove(TASK_QUEUE,0,task);
        return task;
    }
}
