package com.springboot.controller;

import com.alibaba.fastjson.JSON;
import com.springboot.aspect.CountExecTime;
import com.springboot.aspect.Logs;
import com.springboot.bean.User;
import com.springboot.service.IUserService;
import com.springboot.service.impl.CException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "/user", produces = { "application/json;charset=UTF-8" })
public class UserController {

    private static final Logger logger = LogManager.getLogger(UserController.class.getName());

    @Autowired
    private IUserService userService;

    @CountExecTime
    @Logs
    @PostMapping("/addUser")
    @ResponseBody
    public String addUser(@RequestBody User user) {
        logger.info("addUser>>>>>>>>>>>>");

        if(null == user){
            user = new User();
            user.setUserName("tttt");
            user.setUserPassword("123");
        }
        user.setCreateTime(new Date());
        user.setUpdateTime(new Date());
        int t = userService.addUser(user);

        //接口default 方法添加
        //String s = userService.testInterfaceMathod();
        return JSON.toJSONString(user);
    }

    @GetMapping("/getAllUser")
    @ResponseBody
    public String getAllUser(){
        logger.info("getAllUser>>>>>>>>>>>>");
        List<User> userList = userService.getAllUser();
        return JSON.toJSONString(userList);
    }

    @GetMapping("/testAsync")
    @ResponseBody
    public void testAsync() {
        logger.info("testAsync---0");
        userService.testAsync();
        logger.info("testAsync---8");

        throw new CException("testoooooo");
    }

    /**
     * 测试脱敏感    只能返回对象，在返回对象时候，会调用序列化，走脱敏
     * @return
     */
    @GetMapping("getUserD")
    public User getUserD(){
        User user = new User();
        user.setAddress("地球中国-北京市通州区京东总部2号楼");
        user.setPhone("18799898989");
        user.setUserPassword("asdfqwer");
        user.setUserName("zxcv");
        return user;
    }
}
