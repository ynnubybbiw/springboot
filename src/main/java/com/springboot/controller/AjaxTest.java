package com.springboot.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/ajaxtest", produces = {"application/json;charset=UTF-8"})
public class AjaxTest {

    public static final Logger logger = LogManager.getLogger(AjaxTest.class.getName());

    @GetMapping("/getList")
    @ResponseBody
    public String getList() {
        return null;
    }
}
