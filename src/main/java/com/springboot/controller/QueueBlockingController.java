package com.springboot.controller;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.springboot.bean.Menu;
import com.springboot.redisson.RedissonDelayQueue;
import com.springboot.redisson.config.RedisDelayQueueEnum;
import com.springboot.service.QueueBlockingService;
import com.springboot.util.CommonUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author 29678
 */
@RestController
@RequestMapping(path = "/queueBlocking", produces = {"application/json;charset=UTF-8"})
public class QueueBlockingController {

    public static final Logger logger = LogManager.getLogger(QueueBlockingController.class.getName());


    @Autowired
    private QueueBlockingService queueBlockingService;

    @GetMapping("/producer")
    public String producer() {
        Menu menu = new Menu();
        menu.setCreateTime(new Date());
        queueBlockingService.producer(menu);
        return CommonUtil.successJsonStr();
    }


    @Autowired
    private RedissonDelayQueue redissonDelayQueue;

    /**
     * 测试  Redission 延时队列
     *
     * localhost:8888/queueBlocking/testRedissonQueue
     *
     * @return
     */
    @GetMapping("testRedissonQueue")
    public String testRedissonQueue() {

        Map<String, String> map1 = Maps.newHashMap();
        map1.put("orderId", "100");
        map1.put("remark", "其他信息");

        JSONObject map2 = new JSONObject();
        map2.put("orderId", "200");
        map2.put("remark", "其他信息222");



        JSONObject jsonObject = new JSONObject();
        List<String> list = Lists.newArrayList();
        list.add("重启");
        list.add("压测");
        list.add("上线");
        jsonObject.put("123456",list);

        redissonDelayQueue.addDelayQueue(map1,10, TimeUnit.SECONDS, RedisDelayQueueEnum.TEST_ONE.getCode());
        redissonDelayQueue.addDelayQueue(jsonObject,20, TimeUnit.SECONDS,RedisDelayQueueEnum.TEST_TWO.getCode());
        return CommonUtil.successJsonStr();
    }


}
