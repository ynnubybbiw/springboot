package com.springboot.reactive;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

public class CompletableFutureUse {

    private static final Logger logger = LogManager.getLogger(CompletableFutureUse.class.getName());

    public static void main(String[] args) throws InterruptedException {
        CompletableFuture
                //让儿子去打酱油
                .supplyAsync(()-> {
                    try {
                        logger.info("儿子跑步去打酱油");
                        TimeUnit.SECONDS.sleep(1);
                        logger.info("酱油打好了");
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        Thread.currentThread().interrupt();
                    }
                    return "酱油";
                })
                //告诉我一声
                .thenAccept(oil-> logger.info("做菜用酱油:{}",oil));
        logger.info("继续做菜");
        Thread.currentThread().join();
    }
}
