package com.springboot.reactive;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

//import java.util.concurrent.Flow;

/*public class MySubscriber<T> implements Flow.Subscriber<T> {

    private static final Logger logger = LogManager.getLogger(MySubscriber.class.getName());

    private Flow.Subscription subscription;

    // Subscriber name
    private String name = "Unknown";
    // Maximum number of items to be processed by this subscriber
    private final long maxCount;
    // keep track of number of items processed
    private long counter;
    public MySubscriber(String name, long maxCount) {
        this.name = name;
        this.maxCount = maxCount <= 0 ? 1 : maxCount;
    }
    public String getName() {
        return name;
    }

    @Override
    public void onSubscribe(Flow.Subscription subscription) {
        this.subscription = subscription;
        subscription.request(maxCount);
        logger.info("onSubscribe  {}",Thread.currentThread().getName());
    }

    @Override
    public void onNext(T t) {

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        subscription.request(maxCount);
        counter++;
        if (counter >= maxCount) {
            logger.info("Received Cancelling {} Processed item count {} ",name,counter);
            // Cancel the subscription    删除订阅后，不会进入 onComplete
            //subscription.cancel();
        }
        logger.info("Received  {}  {}",name,t);
    }

    @Override
    public void onError(Throwable throwable) {
        logger.error(throwable.getMessage(),throwable);

    }

    @Override
    public void onComplete() {
        logger.info("onComplete  {} ",Thread.currentThread().getName());
        synchronized ("A"){
            "A".notifyAll();
        }
    }


}*/
