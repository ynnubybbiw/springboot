package com.springboot.test;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.StringTokenizer;

/**
 *    添加注解    // NOSONAR    会跳过  Sonar扫描
 */
public class Test3 {

    private static final Logger logger = LogManager.getLogger(Test3.class.getName());

    public static void main(String[] args) {
        StringTokenizer tokenizer = new StringTokenizer("192.168.1.140",".");
        logger.info(tokenizer.nextToken());  // NOSONAR
        logger.info(tokenizer.nextToken());  // NOSONAR
        logger.info(tokenizer.nextToken());  // NOSONAR
        logger.info(tokenizer.nextToken());  // NOSONAR

        System.out.println(isIpv6Address("2001:db8:2de:0:0:0:0:e13"));
        System.out.println(isIpv6Address("10.168.13.29"));
        System.out.println(isIpv6Address(null));
        System.out.println(isIpv6Address(""));


        // 创建一个 String 对象
        String lock = "lock";

        // 创建两个线程
        Thread thread1 = new Thread(() -> {
            // 线程 1 执行代码块
            synchronized (lock) {
                System.out.println("线程 1 开始执行");
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("线程 1 执行结束");
            }
        });

        Thread thread2 = new Thread(() -> {
            // 线程 2 执行代码块
            synchronized (lock) {
                System.out.println("线程 2 开始执行");
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("线程 2 执行结束");
            }
        });

        // 启动两个线程
        thread1.start();
        thread2.start();

    }

    public static boolean isIpv6Address(String address) {
        try {
            final InetAddress inetAddress = InetAddress.getByName(address);
            return inetAddress instanceof Inet6Address;
        } catch (UnknownHostException e) {
            return false;
        }
    }
}
