package com.springboot.test;


import com.google.common.hash.BloomFilter;
import com.google.common.hash.Funnels;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.charset.StandardCharsets;

/**
 * guava工具实现布隆过滤器
 */
public class BloomFilterTest {

    private static final Logger logger = LogManager.getLogger(BloomFilterTest.class.getName());

    public static void main(String[] args) {

        //指定了预期插入的元素数量为 1000，期望的误判率为 0.01
        BloomFilter<CharSequence> bloomFilter = BloomFilter.create(Funnels.stringFunnel(StandardCharsets.UTF_8), 1000, 0.01);

        // 向 Bloom 过滤器中添加元素
        bloomFilter.put("element1");
        bloomFilter.put("element2");
        bloomFilter.put("element3");

        // 判断元素是否存在于 Bloom 过滤器中
        boolean exists1 = bloomFilter.mightContain("element1"); // 返回 true
        boolean exists2 = bloomFilter.mightContain("element4"); // 返回 false

        logger.info("exists1 : {};  exists2: {}",exists1,exists2);


    }
}
