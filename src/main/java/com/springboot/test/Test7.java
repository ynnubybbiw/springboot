package com.springboot.test;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class Test7 {

    private static final Logger logger = LogManager.getLogger(Test7.class.getName());

    public static void main(String[] args) {
        testClass(Test3.class);
    }

    public static void testClass(Class clazz){
        /*Method[] methods = clazz.getMethods();
        logger.info(methods);*/
        testThread();
    }

    /**
     *corePoolSize: 线程池的核心线程数目
     * maximumPoolSize: 线程池的最大线程数目
     * keepAliveTime：线程的最大空闲时间。当线程池中线程数量大于核心线程数，并且线程的空闲时间大于keepAliveTime的时候，线程将会被终止
     * BlockingQueue：线程池的阻塞队列，用来传递和存储提交的任务。根据使用的阻塞队列的不同，线程池对外表现的形式不同
     * ThreadFactory: 指定生成新线程的方式
     * RejectedExecutionHandler：线程池拒绝执行新任务时的handler。
     */
    public static void testThread(){
        ThreadPoolExecutor executor = new ThreadPoolExecutor(4, 8, 2, TimeUnit.MINUTES
                , new ArrayBlockingQueue<>(10), new ThreadFactory() {
            private int num = 0;

            @Override
            public Thread newThread(Runnable r) {
                return new Thread(r, String.format("AsyncThreadProcessor-%d", num++));
            }
        }, new ThreadPoolExecutor.AbortPolicy());

        executor.execute(()->{
            logger.info("testThreadPoolExecutor");
            logger.info(Thread.currentThread().getName());
        });
        executor.shutdown();
    }
}
