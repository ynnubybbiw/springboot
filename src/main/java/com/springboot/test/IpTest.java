package com.springboot.test;

import cn.hutool.core.net.Ipv4Util;
import com.google.common.collect.Lists;
import com.huawei.openstack4j.model.network.IP;
import com.springboot.controller.AjaxTest;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class IpTest {

    public static final Logger logger = LogManager.getLogger(IpTest.class.getName());

    public static void main(String[] args) throws UnknownHostException {
        String startIP = "192.168.0.1";
        String endIP = "192.168.0.10";

        try {
            InetAddress start = InetAddress.getByName(startIP);
            InetAddress end = InetAddress.getByName(endIP);

            List<String> availableIPs = getAvailableIPs(start, end);

            logger.info("Available IPs:");
            for (String ip : availableIPs) {
                logger.info(ip);
            }
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        String ipStr = "10.10.43.0/24";
        String[] split = ipStr.split("/");
        long total = 254;
        long pageSize = 10;
        long pageCount = (total - 1) / pageSize + 1;
        logger.info("页数：{}",pageCount);



        String beginIpStr = Ipv4Util.getBeginIpStr(split[0], Integer.parseInt(split[1]));
        String endIpStr = Ipv4Util.getEndIpStr(split[0], Integer.parseInt(split[1]));
        logger.info("beginIpStr {} ; endIpStr : {}", beginIpStr,endIpStr);

        InetAddress beginIp = InetAddress.getByName(beginIpStr);
        byte[] startAddress = beginIp.getAddress();
        long startIpNum = byteArrayToLong(startAddress);




        List<String> list = Lists.newArrayList();

        for (long pageNo = 1; pageNo < pageCount ; pageNo++){
            long start = (pageNo-1)*pageSize;
            logger.info("起始数：{}; 第几页：{}",start,pageNo);



            String ipAddress = longToIPAddress(startIpNum+start);
            logger.info("每页起始的ip为：{}", ipAddress);

           //logger.info("每页数据：{}",sb);

        }




    }

    public static List<String> getAvailableIPs(InetAddress start, InetAddress end) {
        List<String> availableIPs = new ArrayList<>();

        byte[] startAddress = start.getAddress();
        byte[] endAddress = end.getAddress();

        long startIP = byteArrayToLong(startAddress);
        long endIP = byteArrayToLong(endAddress);

        for (long ip = startIP; ip <= endIP; ip++) {

            logger.info("ip:{}",ip);
            String ipAddress = longToIPAddress(ip);

            //检测ip是否可达
            /*try {
                InetAddress inetAddress = InetAddress.getByName(ipAddress);
                if (inetAddress.isReachable(1000)) {
                    availableIPs.add(ipAddress);
                }
                availableIPs.add(ipAddress);
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }*/

            availableIPs.add(ipAddress);
        }

        //获取当前ip的下一个ip
        String ipAddress = longToIPAddress(endIP + 1);
        availableIPs.add(ipAddress);


        return availableIPs;
    }

    public static long byteArrayToLong(byte[] bytes) {
        long result = 0;
        for (byte b : bytes) {
            result = (result << 8) | (b & 0xFF);
        }
        return result;
    }

    public static String longToIPAddress(long ip) {
        StringBuilder sb = new StringBuilder(15);
        for (int i = 3; i >= 0; i--) {
            sb.append((ip >> (i * 8)) & 0xFF);
            if (i > 0) {
                sb.append(".");
            }
        }
        return sb.toString();
    }
}
