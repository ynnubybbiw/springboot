package com.springboot.test;

import com.springboot.bean.TreeNodeNew;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class CauseEffectAnalyzerNewUse {

    // 优化4: 使用ConcurrentHashMap提高并发性能
    private final Map<Integer, TreeNodeNew> nodes;

    public CauseEffectAnalyzerNewUse() {
        this.nodes = new ConcurrentHashMap<>();
    }

    // 优化5: 使用Builder模式创建节点
    public void addNode(int id, String name) {
        nodes.putIfAbsent(id, new TreeNodeNew.Builder(id, name).build());
    }

    public void addRelation(int fromId, int toId) {
        TreeNodeNew fromNode = nodes.get(fromId);
        TreeNodeNew toNode = nodes.get(toId);
        if (fromNode != null && toNode != null) {
            fromNode.addChild(toNode);
        }
    }

    // 优化6: 使用深度优先搜索代替递归，避免栈溢出
    public TreeNodeNew getUpstreamCauseTree(int targetId) {
        TreeNodeNew source = nodes.get(targetId);
        if (source == null) return null;

        TreeNodeNew rootNode = new TreeNodeNew.Builder(targetId, source.getName()).build();
        Set<Integer> visited = Collections.newSetFromMap(new ConcurrentHashMap<>());
        Stack<TreeNodePair> stack = new Stack<>();
        stack.push(new TreeNodePair(rootNode, source));

        while (!stack.isEmpty()) {
            TreeNodePair current = stack.pop();
            if (!visited.add(current.original.getId())) continue;

            for (TreeNodeNew parent : current.original.getParents()) {
                TreeNodeNew parentCopy = new TreeNodeNew.Builder(parent.getId(), parent.getName()).build();
                current.copy.getParents().add(parentCopy);
                stack.push(new TreeNodePair(parentCopy, parent));
            }
        }

        return rootNode;
    }

    // 优化7: 同样使用深度优先搜索优化下游树的构建
    public TreeNodeNew getDownstreamEffectTree(int targetId) {
        TreeNodeNew source = nodes.get(targetId);
        if (source == null) return null;

        TreeNodeNew rootNode = new TreeNodeNew.Builder(targetId, source.getName()).build();
        Set<Integer> visited = Collections.newSetFromMap(new ConcurrentHashMap<>());
        Stack<TreeNodePair> stack = new Stack<>();
        stack.push(new TreeNodePair(rootNode, source));

        while (!stack.isEmpty()) {
            TreeNodePair current = stack.pop();
            if (!visited.add(current.original.getId())) continue;

            for (TreeNodeNew child : current.original.getChildren()) {
                TreeNodeNew childCopy = new TreeNodeNew.Builder(child.getId(), child.getName()).build();
                current.copy.addChild(childCopy);
                stack.push(new TreeNodePair(childCopy, child));
            }
        }

        return rootNode;
    }

    // 优化8: 使用BFS计算树的深度和宽度
    public int getTreeDepth(TreeNodeNew node) {
        if (node == null) return 0;

        int depth = 0;
        Queue<TreeNodeNew> queue = new LinkedList<>();
        Queue<Integer> levels = new LinkedList<>();
        queue.offer(node);
        levels.offer(1);

        while (!queue.isEmpty()) {
            TreeNodeNew current = queue.poll();
            int level = levels.poll();
            depth = Math.max(depth, level);

            for (TreeNodeNew child : current.getChildren()) {
                queue.offer(child);
                levels.offer(level + 1);
            }
        }

        return depth;
    }

    // 优化9: 改进宽度计算的效率
    public int getTreeWidth(TreeNodeNew node) {
        if (node == null) return 0;

        int maxWidth = 0;
        Queue<TreeNodeNew> queue = new LinkedList<>();
        queue.offer(node);

        while (!queue.isEmpty()) {
            int levelSize = queue.size();
            maxWidth = Math.max(maxWidth, levelSize);

            while (levelSize-- > 0) {
                TreeNodeNew current = queue.poll();
                queue.addAll(current.getChildren());
            }
        }

        return maxWidth;
    }

    // 优化10: 添加辅助类来存储原始节点和复制节点的对应关系
    public static class TreeNodePair {
        final TreeNodeNew copy;
        final TreeNodeNew original;

        TreeNodePair(TreeNodeNew copy, TreeNodeNew original) {
            this.copy = copy;
            this.original = original;
        }
    }


    public static void main(String[] args) {
        // 准备测试数据
        Map<Integer, String> nodeDefinitions = new HashMap<>();
        nodeDefinitions.put(1, "氟泵异常");
        nodeDefinitions.put(2, "供液压力低告警");
        nodeDefinitions.put(3, "刀片液位低");
        nodeDefinitions.put(4, "CPU超温");
        nodeDefinitions.put(5, "HCA卡高温");
        nodeDefinitions.put(6, "DCU故障不识别");
        nodeDefinitions.put(7, "HCA卡PCIe链路异常");
        nodeDefinitions.put(8, "节点关机");
        nodeDefinitions.put(9, "节点HCA卡掉落");
        nodeDefinitions.put(10, "节点slurm离线");
        nodeDefinitions.put(11, "DCU高温");
        nodeDefinitions.put(12, "IB交换机掉落");
        nodeDefinitions.put(13, "计算网不通");
        nodeDefinitions.put(14, "节点宕机黑屏");
        nodeDefinitions.put(17, "交换机超温");
        nodeDefinitions.put(18, "节点负载过高");
        nodeDefinitions.put(19, "光模块超温");
        nodeDefinitions.put(20, "左侧PDM1#插箱供电模块前舱输出1路熔断器故障");
        nodeDefinitions.put(21, "整刀箱掉电");
        nodeDefinitions.put(22, "节点掉电");
        nodeDefinitions.put(23, "CDM系统压力高");
        nodeDefinitions.put(24, "CDM水路过滤器压差高");
        nodeDefinitions.put(25, "HCA交换机掉落");
        nodeDefinitions.put(26, "HCA端口LINK状态异常");
        nodeDefinitions.put(27, "共享存储未挂载");
        nodeDefinitions.put(28, "IB端口掉落");

        int[][] relations = {
                {1, 2}, {2, 3}, {3, 4}, {4, 8}, {8, 9}, {8, 10},
                {3, 11}, {11, 6}, {3, 5}, {5, 7}, {12, 13},
                {14, 10}, {14, 9}, {17, 19}, {18, 19}, {5, 19},
                {20, 21}, {21, 22}, {22, 9}, {22, 10}, {24, 23},
                {23, 3}, {3, 4}, {25, 26}, {26, 13}, {13, 27},
                {28, 13}
        };

        // 测试优化后的版本
        CauseEffectAnalyzerNewUse optimizedAnalyzer = new CauseEffectAnalyzerNewUse();
        long startTime = System.nanoTime();

        // 初始化节点
        for (Map.Entry<Integer, String> entry : nodeDefinitions.entrySet()) {
            optimizedAnalyzer.addNode(entry.getKey(), entry.getValue());
        }

        // 添加关系
        for (int[] relation : relations) {
            optimizedAnalyzer.addRelation(relation[0], relation[1]);
        }

        // 测试目标节点的分析
        int targetNodeId = 3;  // 刀片液位低

        TreeNodeNew upstreamTree = optimizedAnalyzer.getUpstreamCauseTree(targetNodeId);
        TreeNodeNew downstreamTree = optimizedAnalyzer.getDownstreamEffectTree(targetNodeId);

        System.out.println("\n统计信息：");
    }
}
