package com.springboot.test;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Date;

/**
 * 线程打断测试
 */
public class ThreadInterrupt implements Runnable {

    private static final Logger logger = LogManager.getLogger(ThreadInterrupt.class.getName());

    @Override
    public void run() {

        //当将线程标记为打断时，Thread.sleep(1000);  会抛出异常  去掉  Thread.sleep(1000);  则可正常停止线程
        while (!Thread.interrupted()){
            /*try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }*/
            logger.info(new Date());
        }
    }

    public static void main(String[] args) {
        ThreadInterrupt threadInterrupt = new ThreadInterrupt();
        Thread thread = new Thread(threadInterrupt);
        thread.start();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        thread.interrupt();
        logger.info("完成");
    }


}
