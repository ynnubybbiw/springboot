package com.springboot.test;

import java.util.*;

public class GraphBFS {

    // 使用邻接表表示有向图
    private Map<String, List<String>> graph;

    public GraphBFS() {
        this.graph = new HashMap<>();
    }

    // 添加因果关系（有向边）
    public void addCausalRelation(String cause, String effect) {
        graph.putIfAbsent(cause, new ArrayList<>());
        graph.get(cause).add(effect);
    }

    // 找出包含目标节点的所有链条
    public List<List<String>> findChainsContainingNode(String targetNode) {
        List<List<String>> result = new ArrayList<>();

        // 找出所有可能的起点
        for (String startNode : graph.keySet()) {
            Set<String> visited = new HashSet<>();
            List<String> currentPath = new ArrayList<>();
            currentPath.add(startNode);
            visited.add(startNode);

            // 从每个起点开始DFS
            dfs(startNode, targetNode, visited, currentPath, result);
        }

        return result;
    }

    // 深度优先搜索找出所有路径
    private void dfs(String currentNode, String targetNode, Set<String> visited,
                     List<String> currentPath, List<List<String>> result) {
        // 如果当前路径包含目标节点，保存这条路径
        if (currentPath.contains(targetNode)) {
            result.add(new ArrayList<>(currentPath));
        }

        // 如果当前节点没有后继节点，返回
        if (!graph.containsKey(currentNode)) {
            return;
        }

        // 遍历所有可能的下一个节点
        for (String nextNode : graph.get(currentNode)) {
            if (!visited.contains(nextNode)) {
                visited.add(nextNode);
                currentPath.add(nextNode);

                dfs(nextNode, targetNode, visited, currentPath, result);

                // 回溯
                visited.remove(nextNode);
                currentPath.remove(currentPath.size() - 1);
            }
        }
    }

    // 测试用例
    public static void main(String[] args) {
        GraphBFS finder = new GraphBFS();

        // 添加测试数据
        finder.addCausalRelation("A", "B");
        finder.addCausalRelation("B", "C");
        finder.addCausalRelation("C", "D");
        finder.addCausalRelation("B", "E");
        finder.addCausalRelation("E", "F");

        // 查找包含节点"C"的所有链条
        String targetNode = "C";
        List<List<String>> chains = finder.findChainsContainingNode(targetNode);

        // 打印结果
        System.out.println("包含节点 " + targetNode + " 的所有链条：");
        for (List<String> chain : chains) {
            System.out.println(String.join(" -> ", chain));
        }
    }
}
