package com.springboot.test;

import java.util.*;

public class CausalChain {

    // 定义因果关系图，Key是原因节点的ID，Value是后果节点ID列表
    private static Map<Integer, List<Integer>> causalGraph = new HashMap<>();

    // 定义告警名称到ID的映射
    private static Map<String, Integer> alarmNameToId = new HashMap<>();

    public static void main(String[] args) {
        // 初始化因果关系图
        initCausalGraph();

        // 初始化告警名称到ID的映射
        initAlarmNameToId();

        // 告警信息
        // 告警信息
        List<Map<String, String>> alarms = new ArrayList<>();
        Map<String, String> alarm1 = new HashMap<>();
        alarm1.put("alarmName", "计算网不通");
        alarm1.put("alarmId", "4324321");
        alarm1.put("resource", "126");
        alarms.add(alarm1);

        Map<String, String> alarm2 = new HashMap<>();
        alarm2.put("alarmName", "HCA端口LINK状态异常");
        alarm2.put("alarmId", "88888");
        alarm2.put("resource", "126");
        alarms.add(alarm2);

        Map<String, String> alarm3 = new HashMap<>();
        alarm3.put("alarmName", "计算网不通");
        alarm3.put("alarmId", "4324322");
        alarm3.put("resource", "124");
        alarms.add(alarm3);

        Map<String, String> alarm4 = new HashMap<>();
        alarm4.put("alarmName", "HCA交换机掉落");
        alarm4.put("alarmId", "4324324");
        alarm4.put("resource", "888");
        alarms.add(alarm4);

        Map<String, String> alarm5 = new HashMap<>();
        alarm5.put("alarmName", "共享存储未挂载");
        alarm5.put("alarmId", "432432432");
        alarm5.put("resource", "136");
        alarms.add(alarm5);

        Map<String, String> alarm6 = new HashMap<>();
        alarm6.put("alarmName", "节点掉电");
        alarm6.put("alarmId", "4324325");
        alarm6.put("resource", "1237");
        alarms.add(alarm6);

        // 获取因果链
        List<String> causalChain = getCausalChain(alarms);

        // 输出因果链
        System.out.println("因果链: " + String.join(" -> ", causalChain));
    }

    // 初始化因果关系图
    private static void initCausalGraph() {
        // 按照给出的关系初始化
        addCausalRelation(1, 2);
        addCausalRelation(2, 3);
        addCausalRelation(3, 4);
        addCausalRelation(4, 8);
        addCausalRelation(8, 9);
        addCausalRelation(8, 10);
        addCausalRelation(3, 11);
        addCausalRelation(11, 6);
        addCausalRelation(3, 5);
        addCausalRelation(5, 7);
        addCausalRelation(12, 13);
        addCausalRelation(13, 14);
        addCausalRelation(14, 15);
        addCausalRelation(14, 16);
        addCausalRelation(17, 19);
        addCausalRelation(18, 19);
        addCausalRelation(5, 19);
        addCausalRelation(19, 20);
        addCausalRelation(21, 22);
        addCausalRelation(22, 9);
        addCausalRelation(22, 10);
        addCausalRelation(24, 23);
        addCausalRelation(23, 3);
        addCausalRelation(3, 4);
        addCausalRelation(25, 26);
        addCausalRelation(26, 23);
        addCausalRelation(13, 27);
    }

    // 添加因果关系到图中
    private static void addCausalRelation(int cause, int effect) {
        causalGraph.computeIfAbsent(cause, k -> new ArrayList<>()).add(effect);
    }

    // 初始化告警名称到ID的映射
    private static void initAlarmNameToId() {
        alarmNameToId.put("氟泵异常", 1);
        alarmNameToId.put("供液压力低告警", 2);
        alarmNameToId.put("刀片液位低", 3);
        alarmNameToId.put("CPU超温", 4);
        alarmNameToId.put("HCA卡高温", 5);
        alarmNameToId.put("DCU故障不识别", 6);
        alarmNameToId.put("HCA卡PCIe链路异常", 7);
        alarmNameToId.put("节点关机", 8);
        alarmNameToId.put("节点HCA卡掉落", 9);
        alarmNameToId.put("节点slurm离线", 10);
        alarmNameToId.put("DCU高温", 11);
        alarmNameToId.put("IB交换机掉落", 12);
        alarmNameToId.put("计算网不通", 13);
        alarmNameToId.put("节点宕机黑屏", 14);
        alarmNameToId.put("节点slurm离线", 15);
        alarmNameToId.put("节点HCA卡掉落", 16);
        alarmNameToId.put("交换机超温", 17);
        alarmNameToId.put("节点负载过高", 18);
        alarmNameToId.put("光模块超温", 19);
        alarmNameToId.put("左侧PDM1#插箱供电模块前舱输出1路熔断器故障", 20);
        alarmNameToId.put("整刀箱掉电", 21);
        alarmNameToId.put("节点掉电", 22);
        alarmNameToId.put("CDM系统压力高", 23);
        alarmNameToId.put("CDM水路过滤器压差高", 24);
        alarmNameToId.put("HCA交换机掉落", 25);
        alarmNameToId.put("HCA端口LINK状态异常", 26);
        alarmNameToId.put("共享存储未挂载", 27);
        alarmNameToId.put("IB端口掉落", 28);
    }

    // 获取因果链
    private static List<String> getCausalChain(List<Map<String, String>> alarms) {
        Set<Integer> alarmIds = new HashSet<>();
        for (Map<String, String> alarm : alarms) {
            String alarmName = alarm.get("alarmName").trim();
            if (alarmNameToId.containsKey(alarmName)) {
                alarmIds.add(alarmNameToId.get(alarmName));
            }
        }

        List<String> resultChain = new ArrayList<>();
        Set<Integer> visited = new HashSet<>();
        for (Integer alarmId : alarmIds) {
            dfs(alarmId, visited, resultChain);
        }
        return resultChain;
    }

    // 深度优先搜索
    private static void dfs(Integer current, Set<Integer> visited, List<String> resultChain) {
        if (visited.contains(current)) {
            return;
        }
        visited.add(current);
        resultChain.add(getAlarmNameById(current));
        if (causalGraph.containsKey(current)) {
            for (Integer next : causalGraph.get(current)) {
                dfs(next, visited, resultChain);
            }
        }
    }

    // 根据ID获取告警名称
    private static String getAlarmNameById(Integer id) {
        for (Map.Entry<String, Integer> entry : alarmNameToId.entrySet()) {
            if (entry.getValue().equals(id)) {
                return entry.getKey();
            }
        }
        return "Unknown";
    }
}
