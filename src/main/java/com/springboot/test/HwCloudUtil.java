package com.springboot.test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.huawei.openstack4j.api.OSClient.OSClientAKSK;
import com.huawei.openstack4j.model.compute.Flavor;
import com.huawei.openstack4j.model.compute.Server;
import com.huawei.openstack4j.openstack.OSFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class HwCloudUtil {

	private static final Logger logger = LogManager.getLogger(HwCloudUtil.class.getName());

	public static void main(String[] args) {
		// package demo;

		// 设置认证参数
		String ak = "UHLSBM9S9BFUMN0HCMYS";
		String sk = "esfoGLuvZ5d3UpEtUKpWgg3uMqVYN8GQcokYljoO";
		String projectId = "cn-north-4";
		String region = "cn-north-4"; // example: region = "cn-north-1"
		String cloud = "myhuaweicloud.com";

		OSClientAKSK osclient = OSFactory.builderAKSK().credentials(ak, sk, region, projectId, cloud).authenticate();

		// 设置查询参数
		Map<String, String> filter = new HashMap<String, String>();
		// 将需要输入的参数都放入filter里面
		filter.put("limit", "3");

		List<? extends Flavor> list = osclient.compute().flavors().list();
		logger.info(list);


		List<? extends Server> serverList = osclient.compute().servers().list();
		// 调用查询虚拟机列表的接口
//		List<? extends Server> serverList = osclient.compute().servers().list(filter);
		if (serverList.size() > 0) {
			System.out.println("get serverList success, size = " + serverList.size());
			for (Server server : serverList) {
				System.out.println(server);
			}
		} else {
			System.out.println("no server exists.");
		}
	}
}
