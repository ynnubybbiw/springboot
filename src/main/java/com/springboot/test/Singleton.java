package com.springboot.test;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Singleton {

    private static final Logger logger = LogManager.getLogger(Singleton.class.getName());

    public static int SSSS = 1;

    //私有构造方法
    private Singleton() {
        logger.info("fffffffffff");
    }

    //私有静态内部类创建对象
    private static class SingletonHolder{
        private static Singleton singleton = new Singleton();
    }

    //提供公开的获取实例的接口
    public static Singleton getInstance(){
        return SingletonHolder.singleton;
    }
}
