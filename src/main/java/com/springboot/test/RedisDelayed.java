package com.springboot.test;

import cn.hutool.core.date.DateUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;
import redis.clients.jedis.resps.Tuple;

import java.util.Calendar;
import java.util.List;

/**
 * 利用redis实现订单 未支付删除
 *
 * 利用redis的zset,zset是一个有序集合，每一个元素(member)都关联了一个score,通过score排序来取集合中的值
 *
 * 添加元素:ZADD key score member [[score member] [score member] …]
 *
 * 按顺序查询元素:ZRANGE key start stop [WITHSCORES]
 *
 * 查询元素score:ZSCORE key member
 *
 * 移除元素:ZREM key member [member …]
 *
 *
 * 将订单超时时间戳与订单号分别设置为score和member,系统扫描第一个元素判断是否超时
 */
public class RedisDelayed {

    private static final Logger logger = LogManager.getLogger(RedisDelayed.class.getName());

    private static final String ADDR = "10.0.38.5";

    private static final int PORT = 6379;

    public static Jedis getJedis(){
        JedisPoolConfig config = new JedisPoolConfig();
        config.setMaxTotal(1024);
        config.setMaxIdle(10);
        config.setMaxWaitMillis(1000);
        config.setTestOnBorrow(true);
        config.setTestOnReturn(true);

        int timeOut = 2000;

        JedisPool jedisPool = new JedisPool(config,ADDR, PORT,timeOut,"XinJiangKlmy@cmp12",5);
        return jedisPool.getResource();
    }


    public void productionDelayMessage(){

        for(int i=0;i<5;i++){

            //延迟3秒

            Calendar cal1 = Calendar.getInstance();

            cal1.add(Calendar.SECOND, 10);

            int second3later = (int) (cal1.getTimeInMillis() / 1000);

            RedisDelayed.getJedis().zadd("OrderId",second3later,"OID0000001"+i);
            long l = System.currentTimeMillis();
            String s = DateUtil.formatDateTime(DateUtil.date(l));
            logger.info("{} ms: {} :redis生成了一个订单任务：订单ID为"+"OID0000001{}",l, s,i);


        }
    }

    //消费者，取订单
    public void consumerDelayMessage(){

        Jedis jedis = RedisDelayed.getJedis();

        while(true){

            List<Tuple> items = jedis.zrangeWithScores("OrderId", 0, 1);

            if(items == null || items.isEmpty()){

               logger.info("当前没有等待的任务");

                try {

                    Thread.sleep(500);

                } catch (InterruptedException e) {

                    // TODO Auto-generated catch block

                    e.printStackTrace();

                }

                continue;

            }

            int  score = (int) ((Tuple)items.toArray()[0]).getScore();

            Calendar cal = Calendar.getInstance();

            int nowSecond = (int) (cal.getTimeInMillis() / 1000);

            if(nowSecond >= score){

                String orderId = ((Tuple)items.toArray()[0]).getElement();

                //删除成功数
                Long num = jedis.zrem("OrderId", orderId);

                if(null!=num && num>0){
                    long l = System.currentTimeMillis();
                    String s = DateUtil.formatDateTime(DateUtil.date(l));
                    logger.info("{} ms : {} :redis消费了一个任务：消费的订单OrderId为 {}",l,s,orderId);
                }
            }
        }

    }

    public static void main(String[] args) {

        RedisDelayed redisDelayed =new RedisDelayed();

        redisDelayed.productionDelayMessage();

        redisDelayed.consumerDelayMessage();

    }



}
