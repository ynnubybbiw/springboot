package com.springboot.test;

import com.springboot.bean.Alarm;
import com.springboot.bean.Node;

import java.util.*;

public class AlarmChainAnalyzer {

    private Map<Integer, Node> nodes = new HashMap<>();
    private Map<Integer, List<Integer>> graph = new HashMap<>();
    private Map<String, Integer> nameToId = new HashMap<>();

    // 初始化节点信息
    public void initNodes() {
        addNode(1, "氟泵异常");
        addNode(2, "供液压力低告警");
        addNode(3, "刀片液位低");
        addNode(4, "CPU超温");
        addNode(5, "HCA卡高温");
        addNode(6, "DCU故障不识别");
        addNode(7, "HCA卡PCIe链路异常");
        addNode(8, "节点关机");
        addNode(9, "节点HCA卡掉落");
        addNode(10, "节点slurm离线");
        addNode(11, "DCU高温");
        addNode(12, "IB交换机掉落");
        addNode(13, "计算网不通");
        addNode(14, "节点宕机黑屏");
        addNode(15, "节点slurm离线");
        addNode(16, "节点HCA卡掉落");
        addNode(17, "交换机超温");
        addNode(18, "节点负载过高");
        addNode(19, "光模块超温");
        addNode(20, "左侧PDM1#插箱供电模块前舱输出1路熔断器故障");
        addNode(21, "整刀箱掉电");
        addNode(22, "节点掉电");
        addNode(23, "CDM系统压力高");
        addNode(24, "CDM水路过滤器压差高");
        addNode(25, "HCA交换机掉落");
        addNode(26, "HCA端口LINK状态异常");
        addNode(27, "共享存储未挂载");
        addNode(28, "IB端口掉落");
    }

    // 初始化因果关系
    public void initRelations() {
        addRelation(1, 2);
        addRelation(2, 3);
        addRelation(3, 4);
        addRelation(4, 8);
        addRelation(8, 9);
        addRelation(8, 10);
        addRelation(3, 11);
        addRelation(11, 6);
        addRelation(3, 5);
        addRelation(5, 7);
        addRelation(12, 13);
        addRelation(14, 15);
        addRelation(14, 16);
        addRelation(17, 19);
        addRelation(18, 19);
        addRelation(5, 19);
        addRelation(20, 21);
        addRelation(21, 22);
        addRelation(22, 9);
        addRelation(22, 10);
        addRelation(24, 23);
        addRelation(23, 3);
        addRelation(3, 4);
        addRelation(25, 26);
        addRelation(26, 23);
        addRelation(13, 27);
    }

    private void addNode(int id, String name) {
        Node node = new Node(id, name);
        nodes.put(id, node);
        nameToId.put(name, id);
        graph.put(id, new ArrayList<>());
    }

    private void addRelation(int from, int to) {
        graph.get(from).add(to);
    }

    // 查找因果链
    public List<String> findCausationChain(List<Alarm> alarms) {
        // 将告警名称转换为节点ID
        Set<Integer> alarmNodeIds = new HashSet<>();
        for (Alarm alarm : alarms) {
            String name = alarm.getAlarmName().trim();
            if (nameToId.containsKey(name)) {
                alarmNodeIds.add(nameToId.get(name));
            }
        }

        // 找出所有可能的路径
        List<List<Integer>> allPaths = new ArrayList<>();
        for (Integer startId : alarmNodeIds) {
            for (Integer endId : alarmNodeIds) {
                if (!startId.equals(endId)) {
                    List<List<Integer>> paths = findAllPaths(startId, endId, alarmNodeIds);
                    allPaths.addAll(paths);
                }
            }
        }

        // 找出最长的路径
        List<Integer> longestPath = allPaths.stream()
                .max(Comparator.comparingInt(List::size))
                .orElse(new ArrayList<>());

        // 转换为告警名称链
        return longestPath.stream()
                .map(id -> nodes.get(id).getName())
                .collect(ArrayList::new, ArrayList::add, ArrayList::addAll);
    }

    // 查找所有可能的路径
    private List<List<Integer>> findAllPaths(int start, int end, Set<Integer> validNodes) {
        List<List<Integer>> result = new ArrayList<>();
        Set<Integer> visited = new HashSet<>();
        List<Integer> currentPath = new ArrayList<>();

        dfs(start, end, validNodes, visited, currentPath, result);

        return result;
    }

    // 深度优先搜索查找路径
    private void dfs(int current, int end, Set<Integer> validNodes,
                     Set<Integer> visited, List<Integer> currentPath,
                     List<List<Integer>> result) {
        visited.add(current);
        currentPath.add(current);

        if (current == end) {
            result.add(new ArrayList<>(currentPath));
        } else {
            for (int next : graph.get(current)) {
                if (!visited.contains(next) && validNodes.contains(next)) {
                    dfs(next, end, validNodes, visited, currentPath, result);
                }
            }
        }

        visited.remove(current);
        currentPath.remove(currentPath.size() - 1);
    }

    // 主方法示例
    public static void main(String[] args) {
        // 初始化分析器
        AlarmChainAnalyzer analyzer = new AlarmChainAnalyzer();
        analyzer.initNodes();
        analyzer.initRelations();

        // 创建告警列表
        List<Alarm> alarms = Arrays.asList(
                new Alarm("HCA交换机掉落", "4324324", "888"),
                new Alarm("HCA端口LINK状态异常", "88888", "126"),
                new Alarm("计算网不通", "4324321", "126"),
                new Alarm("共享存储未挂载", "432432432", "136"),
                new Alarm("节点掉电", "4324325", "1237")
        );

        // 分析因果链
        List<String> chain = analyzer.findCausationChain(alarms);

        // 打印结果
        System.out.println("因果链：");
        String result = String.join(" -> ", chain);
        System.out.println(result);
    }
}
