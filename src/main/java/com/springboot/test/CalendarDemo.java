package com.springboot.test;

import com.springboot.util.TimeFormatUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class CalendarDemo {

    private static final Logger logger = LogManager.getLogger(CalendarDemo.class.getName());

    @Test
    public void testCalendar() throws ParseException {
        Calendar calendar = Calendar.getInstance();

        logger.info(calendar.get(Calendar.SECOND));

        String startTime = "2020-09-10 01:27:36";
        String endTime = "2020-09-27 20:27:36";

        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date parse1 = sd.parse(startTime);
        Date parse2 = sd.parse(endTime);

        logger.info("s: {}", parse1.getTime());
        logger.info("e: {}", parse2.getTime());

    }

    @Test
    public void testDate(){

        Calendar calender = Calendar.getInstance();
        Date date = new Date();
        calender.setTime(date);

        logger.info(calender.get(Calendar.YEAR));
        logger.info(calender.get(Calendar.MONTH));
        logger.info(calender.get(Calendar.DAY_OF_WEEK)-1);

        Instant instant = Instant.ofEpochMilli(System.currentTimeMillis());
        LocalDateTime dateTime = LocalDateTime.ofInstant(instant, ZoneId.of("+8"));
        LocalDateTime dateTime1 = LocalDateTime.parse("2020-05-21 17:11:46",DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH));

        String pattern = "MMM";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern, Locale.ENGLISH);
        String format = formatter.format(dateTime1);

        String pattern1 = "E";
        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern(pattern1, Locale.ENGLISH);
        String format1 = formatter1.format(dateTime1);

        logger.info(format);
        logger.info(format1);

        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd yyyy", Locale.UK);
        logger.info(sdf.format(date));

    }

    @Test
    public void getWeek(){

        Calendar calendar1 = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();
        int dayOfWeek = calendar1.get(Calendar.DAY_OF_WEEK) - 1;
        /*int offset1 = 1 - dayOfWeek;
        int offset2 = 7 - dayOfWeek;
        calendar1.add(Calendar.DATE, offset1 - 7);
        calendar2.add(Calendar.DATE, offset2 - 7);*/

        int offset1 = -(dayOfWeek+7-1);
        int offset2 = - dayOfWeek;
        calendar1.add(Calendar.DATE, offset1);
        calendar2.add(Calendar.DATE, offset2);

        // System.out.println(sdf.format(calendar1.getTime()));// last Monday
        String lastBeginDate = TimeFormatUtil.getDateStr(calendar1.getTime().getTime());
        // System.out.println(sdf.format(calendar2.getTime()));// last Sunday
        String lastEndDate = TimeFormatUtil.getDateStr(calendar2.getTime().getTime());

        logger.info("{},{}",lastBeginDate,lastEndDate);

    }

    @Test
    public void getWeekStartDate(){

        Calendar calendar = Calendar.getInstance();
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK)-1;

        calendar.add(Calendar.DATE,-(dayOfWeek-1));

        String dateStr = TimeFormatUtil.getDateStr(calendar.getTime().getTime());

        logger.info(dateStr);
    }

}
