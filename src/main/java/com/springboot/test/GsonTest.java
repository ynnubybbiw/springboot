package com.springboot.test;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.google.gson.*;
import com.springboot.bean.Book;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import java.util.List;

/**
 * gson使用
 */
public class GsonTest {

    private static final Logger logger = LogManager.getLogger(GsonTest.class.getName());

    @Test
    public void test1(){
        Gson gson = new Gson();

        String s = gson.toJson(123);
        logger.info(s);

        String str = "{\"total\":1,\"start\":0,\"limit\":10,\"systemAccountLists\":[{\"uuid\":\"09ec2524f7bbe7d56642819e8ac1e30auQ09KuMGAs4\",\"createTime\":\"2021-07-30 17:07\",\"archived\":false,\"username\":\"zouyhua\",\"phoneNumber\":\"18379072593\",\"email\":\"1257236001@qq.com\",\"locked\":false,\"externalId\":\"483624835262008127\",\"ouId\":\"5983107880675531094\",\"ouDirectory\":\"/\",\"displayName\":\"邹勇华\",\"attributes\":{}}]}";

        JSONObject jsonObject = gson.fromJson(str, JSONObject.class);

        logger.info(jsonObject);

        JsonElement jsonElement = JsonParser.parseString(str);
        JsonArray systemAccountLists = jsonElement.getAsJsonObject().get("systemAccountLists").getAsJsonArray();
        String s1 = gson.toJson(systemAccountLists);
        logger.info(s1);

        systemAccountLists.forEach(systemAccount -> {
            logger.info("jsonElement1:{}",systemAccount);
            String username = systemAccount.getAsJsonObject().get("username").getAsString();
            logger.info("username:{}", username);
        });

    }

    /**
     * gson  JsonObject
     */
    @Test
    public void test2(){

        Book book = new Book();
        book.setTest(null);
        book.setAuthor("eee");
        book.setName("mmm");

        Gson gson = new GsonBuilder()
                // 是否显示 null
                .serializeNulls()
                .create();

        String s = gson.toJson(book);
        logger.info(s);

        JsonObject jsonObject = gson.fromJson(s, JsonObject.class);
        logger.info(jsonObject);
        String author = jsonObject.get("author").getAsString();
        logger.info(author);

    }

    /**
     * gson   JsonArray
     */
    @Test
    public void test3(){

        List<String> list = Lists.newArrayList();
        list.add("aaa");
        list.add("234");
        list.add("ojf");

        Gson gson = new GsonBuilder().create();

        String s = gson.toJson(list);

        JsonArray jsonArray = gson.fromJson(s, JsonArray.class);
        logger.info(jsonArray);
    }
}
