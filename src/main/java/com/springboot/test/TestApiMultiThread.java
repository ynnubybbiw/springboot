package com.springboot.test;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.springboot.util.SSLSocketClientUtil;
import okhttp3.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.X509TrustManager;
import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicInteger;

public class TestApiMultiThread {

    private static final Logger logger = LoggerFactory.getLogger(TestApiMultiThread.class);

    public static void main(String[] args) {

        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(50);

        AtomicInteger o = new AtomicInteger();
        AtomicInteger g = new AtomicInteger();
        for (int i = 1; i<=1; i++){
            int finalI = i;
            scheduledExecutorService.execute(()->{
                logger.info("开始执行第{}个任务", finalI);
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("pvdcId","012d6668ce77bdde0263505608722c0d");
                jsonObject.put("vcpu",4);
                jsonObject.put("memory",4*1024*1024*1024L);
                jsonObject.put("type","vm");

                X509TrustManager manager = SSLSocketClientUtil.getX509TrustManager();

                OkHttpClient client = new OkHttpClient().newBuilder()
                        .sslSocketFactory(SSLSocketClientUtil.getSocketFactory(manager), manager)// 忽略校验
                        .hostnameVerifier(SSLSocketClientUtil.getHostnameVerifier())
                        .build();
                MediaType mediaType = MediaType.parse("application/json");
                RequestBody body = RequestBody.create(mediaType, JSON.toJSONString(jsonObject));
                Request request = new Request.Builder()
                        .url("https://10.0.31.174:31666/api_server/cv-console-asset/api/asset/providervdc/handelProviderQuota")
                        .method("POST", body)
                        .addHeader("Content-Type", "application/json")
                        .addHeader("Authorization", "Bearer 6fcada3c-0827-477f-8ac8-e6d6de280f75")
                        .build();
                Response response = null;
                try {
                    response = client.newCall(request).execute();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                assert response.body() != null;
                String string = null;
                try {
                    string = Objects.requireNonNull(response.body()).string();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
                logger.info(string);
                logger.info("结束执行第{}个任务", finalI);
                if(string.contains("执行成功")){
                    o.getAndIncrement();
                }else {
                    g.getAndIncrement();
                }

                JSONObject disk = new JSONObject();
                disk.put("pvdcId","012d6668ce77bdde0263505608722c0d");
                disk.put("storagepoolId","0e996e56b24852d6e96e13db123885a6");
                disk.put("disk",5*1024*1024*1024L);

                RequestBody bodyDisk = RequestBody.create(mediaType, JSON.toJSONString(disk));
                Request requestDisk = new Request.Builder()
                        .url("https://10.0.31.174:31666/api_server/cv-console-asset/api/asset/providervdc/handelProviderQuota")
                        .method("POST", bodyDisk)
                        .addHeader("Content-Type", "application/json")
                        .addHeader("Authorization", "Bearer 6fcada3c-0827-477f-8ac8-e6d6de280f75")
                        .build();

                try {
                    Response responseDisk = client.newCall(requestDisk).execute();
                    assert responseDisk.body() != null;
                    string = Objects.requireNonNull(responseDisk.body()).string();
                    logger.info("扣减磁盘返回：{}",string);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }

                logger.info("执行成功的个数：{}",o.get());
                logger.info("执行失败的个数：{}",g.get());
            });
        }


    }
}
