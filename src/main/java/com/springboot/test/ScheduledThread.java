package com.springboot.test;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * 开启线程执行周期性任务
 */
public class ScheduledThread {

    private static final Logger logger = LogManager.getLogger(ScheduledThread.class.getName());

    //线程周期性任务 要用main方法才行
    public static void main(String[] args) {
        ScheduledExecutorService scheduledExecutorService = Executors.newScheduledThreadPool(5);
        scheduledExecutorService.scheduleAtFixedRate(ScheduledThread::testFilew,0,3000, TimeUnit.MILLISECONDS);
    }

    @Test
    public static void testFilew(){
        File file = new File("d:\\a.txt");
        if(!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time = df.format(new Date());
        logger.info(time);
        FileWriter fw = null;
        try {
            fw = new FileWriter("d:\\a.txt",true);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            if (fw != null) {
                fw.write(time);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            if (fw != null) {
                fw.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
