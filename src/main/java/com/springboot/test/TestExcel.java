package com.springboot.test;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.EasyExcelFactory;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.springboot.listener.ExcelListener;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import java.util.Date;
import java.util.List;
import java.util.Map;

public class TestExcel {

    private static final Logger logger = LogManager.getLogger(Test6_xiancheng.class.getName());

    @Test
    public void createTest(){// 生成Excel路径
        String fileName = "C:\\Users\\29678\\Desktop\\test\\测试.xlsx";
        EasyExcelFactory.write(fileName).head(head()).sheet("模板").doWrite(dataList());
    }

    private List<List<String>> head() {
        List<List<String>> list = Lists.newArrayList();
        List<String> head0 = Lists.newArrayList();
        head0.add("姓名");
        List<String> head1 = Lists.newArrayList();
        head1.add("年龄");
        List<String> head2 = Lists.newArrayList();
        head2.add("生日");
        list.add(head0);
        list.add(head1);
        list.add(head2);
        return list;
    }

    private List<List<Object>> dataList() {
        List<List<Object>> list = Lists.newArrayList();
        for (int i = 0; i < 10; i++) {
            List<Object> data = Lists.newArrayList();
            data.add("张三");
            data.add(25+i);
            data.add(new Date());
            list.add(data);
        }
        return list;
    }


    @Test
    public void test() {
        // 生成Excel路径
        String fileName =  "C:\\Users\\29678\\Desktop\\test\\测试.xlsx";
        ExcelListener excelListener = new ExcelListener();
        EasyExcelFactory.read(fileName, excelListener).sheet().doRead();
        // 表格头数据
        Map<Integer, String> importHeads = excelListener.getImportHeads();
        logger.info(importHeads);
        // 每一行数据
        List<JSONObject> dataList = excelListener.getDataList();
        for (JSONObject object : dataList) {
            logger.info(object);
        }
    }
}
