package com.springboot.test;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.lang.UUID;
import cn.hutool.core.net.Ipv4Util;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.RuntimeUtil;
import cn.hutool.cron.CronUtil;
import cn.hutool.cron.task.Task;
import cn.hutool.crypto.digest.DigestUtil;
import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.springboot.bean.Book;
import com.springboot.bean.Quota;
import com.springboot.bean.QuotaBuilder;
import com.springboot.bean.User;
import com.springboot.util.MockUtil;
import com.springboot.util.SSHConnection;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.drools.compiler.compiler.DrlParser;
import org.drools.compiler.compiler.DroolsError;
import org.drools.compiler.compiler.DroolsParserException;
import org.drools.compiler.lang.descr.PackageDescr;
import org.drools.core.io.impl.ClassPathResource;
import org.libvirt.Connect;
import org.libvirt.Domain;
import org.libvirt.LibvirtException;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Test {

    private static final Logger logger = LogManager.getLogger(Test.class.getName());

    public static void main(String[] args) {

       while (true){
           List<String> labelList = Lists.newArrayList();
           labelList.add("aaaa_2");
           labelList.add("aaaa_3");

           int num = 10;
           String ss = String.valueOf(num);
           if(num != 1){
               StringBuilder sb = new StringBuilder();
               for(int i = 1; i <= ss.length(); i++){
                   sb.append(0);
               }
               ss = ss.replace(ss,sb.toString());
               if(ss.length() > 1){
                   ss = ss.substring(0,ss.length()-1)+1;
               }else {
                   ss = ss.replace(ss,"1");
               }
           }
           logger.info(ss);
           int t = Integer.parseInt(ss);
           String label = "aaaa";
           for (int i = 1; i <= num; i++){
               String newLabel = label+"_"+(t+i-1);
               if(labelList.contains(newLabel)){
                   num++;
                   continue;
               }

               logger.info(newLabel);
           }
       }
    }

    @org.junit.Test
    public void testname(){
        String ss = "approvalprocess.bpmn20.xml";

        logger.info(ss.endsWith("bpmn20.xml"));
    }

    @org.junit.Test
    public void testNum(){
        int a = 512;
        double b = 1024.0d;
        logger.info(a/b);
    }

    @org.junit.Test
    public void testMap(){
        Map<String,String> map = Maps.newHashMap();
        map.put("aa","11");
        map.put("bb","22");
        map.put("cc","33");
        map.put("dd","44");

        map.forEach((k,v)-> logger.info(k+">>>>>>"+v));

    }

    @org.junit.Test
    public void testStr(){
        String ss = "tenant默认安全组";
        boolean b = ss.endsWith("默认安全组");
        logger.info(b);

        Map<String,String> map = Maps.newHashMap();
        map.put("aa","11");
        map.put("bb","22");
        map.put("cc","33");
        map.put("dd","44");

        Gson gson = new Gson();
        logger.info(gson.toJson(map));
    }

    @org.junit.Test
    public void test123(){
        List<String> list = Lists.newArrayList();
        list.add("aa");
        list.add("bb");
        list.add("cc");
        list.add("dd");
        /*logger.info(list.indexOf("cc"));

        list.set(5,"test");

        logger.info(JSON.toJSONString(list));*/

        for(int i=2;i<list.size();i++){
            logger.info(list.get(i));
        }


    }

    @org.junit.Test
    public void test234(){
        String[] strings = new String[10];

        strings[3] = "fff";
        List<String> collect = Arrays.stream(strings).filter(Objects::nonNull).collect(Collectors.toList());
        logger.info(JSON.toJSONString(collect));

    }

    @org.junit.Test
    public void test123456(){
        Snowflake snowflake = IdUtil.createSnowflake(1, 1);
        logger.info(snowflake.nextId());

        String s = IdUtil.simpleUUID();
        logger.info(s);

        Log log = LogFactory.get();
        log.debug("fffffff");

        //执行win命令
        String str = RuntimeUtil.execForStr("ipconfig");
        logger.info(str);
    }

    @org.junit.Test
    public void test898(){
        CronUtil.schedule("*/2 * * * * *", (Task) () -> {

            logger.info("Task excuted.");
        });

        // 支持秒级别定时任务
        CronUtil.setMatchSecond(true);
        CronUtil.start();

    }


    @org.junit.Test
    public void testListJava8(){
        List<String> strings = Lists.newArrayList();
        strings.add("a");
        strings.add("b");
        strings.add("c");
        strings.add("d");
        strings.add("e");
        strings.add("f");

        strings.forEach(this::testTrue);


    }

    private void testTrue(String s) {
        if("b".equals(s)){
            return;
        }
        logger.info(s);
    }

    @org.junit.Test
    public void testMap1(){
        List<String> strings = Lists.newArrayList();
        strings.add("a");
        strings.add("b");
        strings.add("c");
        strings.add("d");
        strings.add("e");
        strings.add("f");
        HashMap<String,String> mapRelateApp= new HashMap<>();
        strings.forEach(s -> {

            mapRelateApp.putIfAbsent("url-" + s, s);
            logger.info(mapRelateApp.get("url-"+s));
        });
        logger.info(JSON.toJSONString(mapRelateApp));
    }

    @org.junit.Test
    public void testformat(){
        logger.info(String.format("%.2f",0.00));
    }

    @org.junit.Test
    public void testzero(){
        int a = 0;
        int b = 0;
        logger.info(a/b);
    }

    @org.junit.Test
    public void testSSHPyton(){
        //将脚本代码写入文件
        String s = SSHConnection.exceCodePython();
        logger.info("...................... {} ", s);
        //通过调用脚本文件执行
        String s1 = SSHConnection.exceCodeEofPython();
        logger.info("======================== {}",s1);

        //SSHConnection.uploadFile();
    }

    @org.junit.Test
    public void testSSHShell(){
        //将脚本代码写入文件
       /* String s = SSHConnection.exceCodeShell();
        logger.info("...................... {} ", s);*/
        //通过调用脚本文件执行
        /*String s1 = SSHConnection.exceCodeEofShell();
        logger.info("======================== {}",s1);*/

        //SSHConnection.uploadFile();

        //SSHConnection.checkFileExist();

        SSHConnection.connect();
    }

    @org.junit.Test
    public void testALiid(){
        List list = Lists.newArrayList();
        List<User> list1 =Lists.newArrayList();
        User user1  = new User();
        user1.setId(1);
        user1.setUserName("name1");
        list1.add(user1);
        List<User> list2 =Lists.newArrayList();
        User user2  = new User();
        user2.setId(1);
        user2.setUserName("name2");
        list2.add(user2);
        List<User> list3 =Lists.newArrayList();
        User user3  = new User();
        user3.setId(1);
        user3.setUserName("name3");
        list3.add(user3);
        list.addAll(list1);
        list.addAll(list2);
        list.addAll(list3);

        logger.info(JSON.toJSONString(list));
    }

    @org.junit.Test
    public void testMap123(){
        Map map = Maps.newHashMap();
        map.put(null,"123");

        logger.info(JSON.toJSONString(map));

    }

    @org.junit.Test
    public void printArray(){
        String[] jayArray = {"jay", "boy"};

        //打印数组方式1
        Stream.of(jayArray).forEach(jayArray1 -> logger.info(jayArray1));

        //打印数组方式2
        logger.info(Arrays.toString(jayArray));

        Vector vector = new Vector();
        vector.add("123");
    }

    @org.junit.Test
    public void printStr(){
        String str = "insert into `bill_info`(id,bill_cycle_id,instance_id,tenant_id,asset_sort_code,bill_cycle_cost,instance_name)\n" +
                "SELECT \n" +
                "  UUID(),\n" +
                "  c.`id`,\n" +
                "  e.`instance_id`,\n" +
                "  c.tenant_id,\n" +
                "  e.asset_sort_code,\n" +
                "  SUM(d.`cost`),\n" +
                "  e.`instance_name` \n" +
                "FROM\n" +
                "  `accounts_info` e \n" +
                "  JOIN `accounts_detail` d \n" +
                "    ON e.`id` = d.`accounts_id` \n" +
                "  JOIN `bill_cycle` c \n" +
                "    ON c.`tenant_id` = e.`tenant_id` \n" +
                "WHERE c.is_operation = 1 \n" +
                "  AND e.`start_date` >= c.start_date \n" +
                "  AND e.`end_date` <= c.end_date \n" +
                "GROUP BY e.`instance_id`  \n" +
                "\n" +
                "ON DUPLICATE KEY UPDATE bill_cycle_cost = VALUES(bill_cycle_cost)";

        //logger.info(str);

        String time = "2021-02-18 13:50:02";
        DateTime dateTime1 = DateUtil.parseDateTime(time);
        Date date = new Date();
        long l = DateUtil.betweenDay(dateTime1, date, Boolean.TRUE);
        for(int i=1; i<l; i++){
            logger.info(i);
            Date dateTime = DateUtil.offsetDay(dateTime1, i).toJdkDate();
            String s = DateUtil.formatDateTime(dateTime);
            logger.info(s);
        }
    }

    @org.junit.Test
    public void testMouth(){
        String time1 = "2021-02-18 13:50:02";
        DateTime dateTime1 = DateUtil.parseDateTime(time1);
        String time2 = "2021-04-20 13:50:02";
        DateTime dateTime2 = DateUtil.parseDateTime(time2);

        long l = DateUtil.betweenMonth(dateTime1, dateTime2, Boolean.TRUE);

        logger.info("======================: {}" ,l);

        for(int i=1; i<=l; i++){
            Date date = DateUtil.beginOfMonth(DateUtil.offsetMonth(dateTime1, i)).toJdkDate();
            logger.info("nnnnnnnnnn: {}", DateUtil.formatDateTime(date));
        }
    }


    final String aa = "123";

    /**
     * 测试final 是无法重新分配值
     */
    @org.junit.Test
    public void testFinal(){

        logger.info("start aa : {}", aa);

        //无法为最终变量“ aa”分配值
        //aa = "456";
        logger.info("end aa : {}", aa);

        Book2 book2 = new Book2();
        book2.setNum(10);
        logger.info("book2 : {}",JSON.toJSONString(book2));
    }

    /**
     * final 修饰的对象不能被继承，可以被修改属性
     * 如果book类被 final修饰，刚不能被 继承
     */
    static class Book2 extends Book{

        private Integer num;

        public Integer getNum() {
            return num;
        }

        public void setNum(Integer num) {
            this.num = num;
        }
    }


    /**
     * 很多时候会容易把static和final关键字混淆，
     * static作用于成员变量用来表示只保存一份副本，
     * 而final的作用是用来保证变量不可变。
     */
    @org.junit.Test
    public void testFinalAndStatic(){
        MyClass myClass1 = new MyClass();
        MyClass myClass2 = new MyClass();
        System.out.println(myClass1.i);
        System.out.println(myClass1.j);
        System.out.println(myClass2.i);
        System.out.println(myClass2.j);
    }
    static class MyClass {
        public final double i = Math.random();
        public static double j = Math.random();
    }

    @org.junit.Test
    public void testBudier(){
        Quota quota = new QuotaBuilder().setCpu(4).setGpu(1).setRam(2048).setDisk(10).setVmNum(1).setTenantId("tenant1").build();

        logger.info(JSON.toJSONString(quota));

        String str = "dsafds_from_AC";
        if(str.endsWith("_from_AC")){
            logger.info("dddddd");
        }
    }

    @org.junit.Test
    public void testSplit(){
        String authToken = "123434_qweeew";
        authToken = authToken.split("_")[0];
        logger.info(authToken);
    }

    @org.junit.Test
    public void testListToArray(){
        /*List<String> list = Lists.newArrayList();
        list.add("123");
        list.add("asd");
        list.add("oiu");
        list.add("123");

        String[] srtArr = list.stream().distinct().toArray(String[]::new);
        logger.info(srtArr);

        List<Object> objects = Arrays.asList(null);*/

        for (int i = -1; i <= 1; i++){
            logger.info(6-i);
        }

        for(int i=1; i<=10; i++){
            logger.info(System.currentTimeMillis());
        }

    }


    @org.junit.Test
    public void testJSON(){
        String str = "{\"total\":1,\"start\":0,\"limit\":10,\"systemAccountLists\":[{\"uuid\":\"09ec2524f7bbe7d56642819e8ac1e30auQ09KuMGAs4\",\"createTime\":\"2021-07-30 17:07\",\"archived\":false,\"username\":\"zouyhua\",\"phoneNumber\":\"18379072593\",\"email\":\"1257236001@qq.com\",\"locked\":false,\"externalId\":\"483624835262008127\",\"ouId\":\"5983107880675531094\",\"ouDirectory\":\"/\",\"displayName\":\"邹勇华\",\"attributes\":{}}]}";

        JSONObject jsonObject = JSON.parseObject(str);
        logger.info(JSON.toJSONString(jsonObject));

        JSONArray systemAccountLists = jsonObject.getJSONArray("systemAccountLists");

        logger.info(JSON.toJSONString(systemAccountLists));

        systemAccountLists.forEach(o -> {
            String s = JSON.toJSONString(o);
            JSONObject jsonObject1 = JSON.parseObject(s);
            String externalId = jsonObject1.getString("externalId");
            logger.info("externalId: {}",externalId);
        });
    }

    @org.junit.Test
    public void testIf(){
        String str = "-1";

        if(!("-1".equals(str) || "不限".equals(str))){
            logger.info("123455");
        }

        String s = "asfdsfd";
        String[] s1 = s.split("_");
        logger.info(s1[0]);
    }

    @org.junit.Test
    public void testBase64(){
        String acUserName = "sugon";
        String s = Base64.getEncoder().encodeToString(acUserName.getBytes(StandardCharsets.UTF_8));
        logger.info(s);
    }

    @org.junit.Test
    public void testListIndex(){
        List<String> list = Lists.newArrayList();
        list.add("aaa");
        list.add("bbb");
        list.add("ccc");
        list.add("xxx");
        list.add("zzz");

        logger.info(list.indexOf("ooo"));

        List<Object> objects = Arrays.asList(null);

        String str = "a,b,c,d,e";
        List<String> strings = Splitter.on(",").splitToList(str);
        logger.info(strings);

    }

    @org.junit.Test
    public void testElseIf(){
        int a = 10;
        int b = 20;
        int c = 30;

        if(a>b){
            logger.info("a<b");
        }else if(a<c){
            logger.info("a<c");
        }
    }

    @org.junit.Test
    public void testSystemParam(){
        String property = System.getProperty("java.io.tmpdir");
        logger.info("resule:{}",property);
    }

    /**
     * 测试连接libvirt
     * @throws LibvirtException
     */
    @org.junit.Test
    public void testLibvirt() throws LibvirtException {
        Connect connect  = new Connect("",true);
        Domain domain = connect.domainLookupByName("");
        String uuid = domain.getUUIDString();
        String osType = domain.getOSType();
        logger.info("uuid:{}",uuid);
        logger.info("osType:{}",osType);
    }

    @org.junit.Test
    public void md5Test(){
        try {
            FileInputStream file1 = new FileInputStream("C:\\Users\\29678\\Desktop\\desktop\\XJ-MCM-update1.0.0-R20210901.tar.gz");
            FileInputStream file2 = new FileInputStream("C:\\Users\\29678\\Desktop\\desktop\\XJ-MCM-update1.0.0-R20210901.tar.gz");
            logger.info(DigestUtil.md5Hex(file1));
            logger.info(DigestUtil.md5Hex(file2));
        } catch (FileNotFoundException e) {
            logger.error("核验失败：",e);
        }
    }

    @org.junit.Test
    public void testStrSub(){
        String str = "/dev/sda2       10882760 9328496    981756  91% /";
        String[] arrs = str.split(" ");
        for(String s : arrs){
            if(!"".equals(s)){
                logger.info("得到的数据：{}",s);
            }
        }
    }

    @org.junit.Test
    public void testPage(){
        List<String> list = new ArrayList<>();
        list.add("0");
        list.add("1");
        list.add("2");
        list.add("3");
        list.add("4");
        list.add("5");
        list.add("6");
        list.add("7");
        list.add("8");
        list.add("9");
        list.add("10");
        list.add("11");
        list.add("12");
        list.add("13");
        list.add("14");
        list.add("15");
        list.add("16");
        list.add("17");
        list.add("18");
        list.add("19");
        list.add("20");
        list.add("21");
        list.add("22");
        list.add("23");
        list.add("24");
        list.add("25");
        list.add("26");
        list.add("27");


        int total = list.size();
        int pageSize = 5;
        int pageCount = (total - 1) / pageSize + 1;
        logger.info("页数：{}",pageCount);
        for (int pageNo = 1; pageNo < pageCount ; pageNo++){
           int start = (pageNo-1)*pageSize;
           logger.info("起始数：{}; 第几页：{}",start,pageNo);
           StringBuilder sb = new StringBuilder();
            for (int i = 0; i < pageSize; i++){
                String s = list.get(start + i);
                sb.append(s).append("-");
            }
            logger.info("每页数据：{}",sb);

        }

    }

    @org.junit.Test
    public void testDateTime(){
        Date date = new Date();
        long a = date.getTime();
        long b = System.currentTimeMillis();
        long c = a-24 * 60 * 60 * 1000;
        logger.info(a);
        logger.info(b);
        logger.info(c);
        DateTime date1 = DateUtil.date(c);
        logger.info(DateUtil.format(date1.toJdkDate(), DatePattern.NORM_DATETIME_PATTERN));

        String str = "asfggDDDfff";
        logger.info(str.toLowerCase());
    }

    @org.junit.Test
    public void testRetainAll(){

        ArrayList<String> sites = new ArrayList<>();

        sites.add("Google");
        sites.add("Runoob2");
        sites.add("Taobao");

        ArrayList<String> sites2 = new ArrayList<>();

        sites2.add("Wiki");
        sites2.add("Runoob");
        sites2.add("Google");
        sites2.add("435");

        sites.retainAll(sites2);

        logger.info(sites);


    }

    @org.junit.Test
    public void testStrComp(){
        String ip = "10.0.38.5";

        String ipStr = "10.0.38.6";

        //logger.info("contains {}", ip.contains(ipStr));
        //logger.info("indexOf {}", ip.indexOf(ipStr));

        DateTime dateTime = DateUtil.beginOfDay(new Date());
        String syncDate = DateUtil.formatDateTime(dateTime);
        //logger.info("fffff:{}",syncDate);

        List<String> stringList = Lists.newArrayList();
        String join = StringUtils.join(stringList, ',');

        logger.info("fffff: {}",join);

    }

    @org.junit.Test
    public void testCalender(){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.MONTH,7);
        logger.info("计算后的时间：{}",DateUtil.formatDateTime(calendar.getTime()));

        Map<String,Object> map  = Maps.newHashMap();

        Object put = map.put("123", "");
        Object put1 = map.put("1234", " ");

        logger.info(""+put);
        logger.info(""+put1);


        String format = DateUtil.format(calendar.getTime(), "M/d/yyyy");
        logger.info("fffff: {}",format);

    }

    @org.junit.Test
    public void testPattern(){
        String target = "Router.*?could not be found";
        String str = "Router rwwrwf32432 could not be found";
        Pattern pattern = Pattern.compile(target, Pattern.CASE_INSENSITIVE);

        Matcher matcher = pattern.matcher(str);

        boolean matches = matcher.matches();

        logger.info("result : {}", matches);

    }

    @org.junit.Test
    public void testListGet(){
        /*List<String> list = Lists.newArrayList();
        String s = list.get(Math.min(1, list.size()-1));
        logger.info("aaaaa: {}",s);*/

        String ipv4CidrStr = "192.16.0.0/24";
        String[] split = ipv4CidrStr.split("/");
        String beginIp = Ipv4Util.getBeginIpStr(split[0], Integer.parseInt(split[1]));
        String endIp = Ipv4Util.getEndIpStr(split[0], Integer.parseInt(split[1]));

        logger.info("mmmmmmmmmmmmmmmmmmmm {}   {}",beginIp,endIp);
    }

    @org.junit.Test
    public void disjointList(){
        List<String> list1 = Lists.newArrayList();
        list1.add("新疆油田");
        list1.add("数据公司1");
        list1.add("实习生");

        List<String> list2 = Lists.newArrayList();
        list2.add("数据公司");
        list2.add("公司机关");
        list2.add("外部用户");

        //查看两个集合中是否没有交集
        boolean disjoint = Collections.disjoint(list1, list2);

        logger.info("ddddddddddddddd: {}", disjoint);

    }

    @org.junit.Test
    public void ipv6Ipget(){
        String networkPrefix = "2001:db8:1234::"; // 指定 IPv6 网段（前缀）
        int prefixLength = 64; // 指定前缀长度

        try {
            InetAddress networkAddress = InetAddress.getByName(networkPrefix);
            BigInteger networkPrefixValue = new BigInteger(1, networkAddress.getAddress());

            BigInteger networkSize = BigInteger.ONE.shiftLeft(128 - prefixLength);
            BigInteger hostCount = networkSize.subtract(BigInteger.ONE);

            List<String> ipList = Lists.newArrayList();
            for (BigInteger i = BigInteger.ZERO; i.compareTo(hostCount) <= 0; i = i.add(BigInteger.ONE)) {
                BigInteger addressValue = networkPrefixValue.add(i);
                InetAddress address = InetAddress.getByAddress(addressValue.toByteArray());
                logger.info(address.getHostAddress());
                ipList.add(address.getHostAddress());

                if(ipList.size() > 100){
                    break;
                }
            }
            logger.info("生成的ip数：{}",ipList.size());
        } catch (UnknownHostException e) {
            logger.error("生成错误：",e);
        }
    }

    @org.junit.Test
    public void test1223(){
        String str = "fdsfds";
        String[] split = str.split(",");
        logger.info(split);

        boolean b = areListsEqual(Lists.newArrayList(), Lists.newArrayList());
        logger.info(b);

        String aa = "/cloudservers";
        String[] split1 = aa.split("/");
        logger.info(split1);
    }

    public static boolean areListsEqual(List<String> list1, List<String> list2) {
        if (list1.size() != list2.size()) {
            return false;
        }

        // 比较两个列表的元素内容
        for (String str : list1) {
            if (!list2.contains(str)) {
                return false;
            }
        }

        return true;
    }

    /**
     * json 转的时候，下划线会转驼峰
     */
    @org.junit.Test
    public void testJsonC(){
        String usrStr = "{\"age\":10,\"user_password\":\"userPassword\",\"user_name\":\"userName\"}";

        User user = JSON.parseObject(usrStr, User.class);

        logger.info("user: {}",user);
    }


    @org.junit.Test
    public void testBufferInsert(){
        String uuid = "123456789011121345678";
        StringBuffer buffer = new StringBuffer(uuid);
        if (uuid.length() <= 32) {
            buffer.insert(20, '-');
            buffer.insert(16, '-');
            buffer.insert(12, '-');
            buffer.insert(8, '-');
        }
        logger.info("testBufferInsert: {}", buffer);
    }

    @org.junit.Test
    public void testJsonArray(){
        //String str = "[\"ad61d6d515314c66a3cb981d7e4a3fa2\",\"47cf259008ce4a83a3ea5e7e10509279\"]";
        //String str = "[]";
        String str = "";
        JSONArray jsonArray = JSON.parseArray(str);
        jsonArray.forEach(s -> {
            logger.info("fdsfs:{}",s);
        });
    }


    @org.junit.Test
    public void testListremoveAll(){
        ArrayList<String> list1 = Lists.newArrayList("a", "b", "c", "d", "e");
        ArrayList<String> list2 = Lists.newArrayList("c", "d", "e","ff");


        ArrayList<String> list3 = Lists.newArrayList(list1);
        //boolean b = list2.removeAll(list1);

        list3.retainAll(list2);

        logger.info("fdsfds");
    }

    @org.junit.Test
    public void testDate() throws Exception {

        SimpleDateFormat from = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date fromParse = from.parse("2023-09-01T03:51:39.000+0000");
        Timestamp timestamp = new Timestamp(fromParse.getTime());
        System.out.println("");


        String aaa = "true";

        logger.info("zzzzz：{}",Boolean.TRUE.equals(aaa));

    }

    @org.junit.Test
    public void testString(){

        String str = "name";
        String s1 = new String(str);
        String s2 = new String(str);

        logger.info("print {}, {}, {},{}",s1,s2,s1==s2,s1.intern()==s2.intern());

    }

    public final static ThreadLocal<String> RESOURCE = new ThreadLocal<String>();

    @org.junit.Test
    public void testThreadLoacl(){
        Thread t1 = new Thread(()->{

            RESOURCE.set("aaaaa");

            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            System.out.println("aaaa | "+RESOURCE.get());
        });

        Thread t2 = new Thread(()->{
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            RESOURCE.set("bbbb");
            System.out.println("bbbb | "+RESOURCE.get());

        });


        t1.start();
        t2.start();

    }

    @org.junit.Test
    public void getData(){

        String s = MockUtil.readJson("tableData.json");
        JSONObject jsonObject = JSON.parseObject(s);
        JSONArray tableData = jsonObject.getJSONArray("tableData");

        List<String> list = Lists.newArrayList();

        for (int i = 0; i < tableData.size(); i++) {
            StringBuilder table_number = new StringBuilder();
            JSONObject tableD = tableData.getJSONObject(i);
            table_number.append(tableD.getInteger("id") + "_" + tableD.getDoubleValue("size") + ",");
            // 因为 所有的id不一定叫id，有可能是cpuId、memoryId、dataDish、id、operatingSystem
            JSONArray children = tableD.getJSONArray("children");
            for (int j = 0; j < children.size(); j++) {
                Integer id = null;
                JSONObject child = children.getJSONObject(j);

                String dataType = child.getString("dataType");
                if(org.apache.commons.lang3.StringUtils.isNotBlank(dataType) && !"interIp".equalsIgnoreCase(dataType)){
                    id = child.getInteger("id");
                    if (id == null) {
                        id = child.getInteger("cpuId");
                        if (id == null) {
                            id = child.getInteger("memoryId");
                            if (id == null) {
                                id = child.getInteger("dataDish");
                                if (id == null) {
                                    id = child.getInteger("operatingSystem");
                                    if(id == null){
                                        id = child.getInteger("bandwidth");
                                        if (id == null) {
                                            id = child.getInteger("interIp");
                                            if(id == null){
                                                id = child.getInteger("systemActive");
                                                if(id == null){
                                                    id = child.getInteger("hardDisk");
                                                    if(id == null){
                                                        id = child.getInteger("vcpu");
                                                        if(id == null){
                                                            id = child.getInteger("interWk");
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    list.add(child.getString("dataType") +"    "+id);
                }
            }

        }
        logger.info(list);

    }


    @org.junit.Test
    public void getToken(){
        StringBuffer sb = new StringBuffer();
        JSONObject jsonObject = JSON.parseObject("{\"result\":{\"token\":\"XTEJtIQwaOhfeAp0c2HJkZ7AJjGI34kL4ihO4g\",\"fromrootvsys\":\"true\",\"vsysId\":\"0\",\"vsysName\":\"root\",\"username\":\"hillstone\",\"role\":\"admin\",\"authServer\":\"local\",\"isLocalAuth\":true,\"passwordExpired\":0,\"defaultAccount\":0,\"haAdminMaster\":1,\"uuid\":\"c0426743-abd4-4723-2312-f85d6288733b\"},\"success\":true,\"total\":0}");
        JSONObject resultJsn = jsonObject.getJSONObject("result");
        String HS_frame_lang = "zh_CN";
        sb.append("token=" + resultJsn.getString("token") + ";");
        sb.append("platform=" + resultJsn.getString("platform") + ";");
        sb.append("hw_platform=" + resultJsn.getString("hw_platform") + ";");
        sb.append("host_name=" + resultJsn.getString("host_name") + ";");
        sb.append("company=" + resultJsn.getString("company") + ";");
        sb.append("oemid=" + resultJsn.getString("oemId") + ";");
        sb.append("vsysId=" + resultJsn.getString("vsysId") + ";");
        sb.append("vsysName=" + resultJsn.getString("vsysName") + ";");
        sb.append("role=" + resultJsn.getString("role") + ";");
        sb.append("license=" + resultJsn.getString("license") + ";");
        sb.append("httpProtocol=" + resultJsn.getString("httpProtocol") + ";");
        JSONObject sysinfo = resultJsn.getJSONObject("sysInfo");
        String soft_version = "soft_version=";
        if (sysinfo != null && StringUtils.isNotBlank(sysinfo.getString("soft_version"))) {
            soft_version = soft_version + sysinfo.getString("soft_version");
        }
        sb.append(soft_version+";");
        sb.append("username=hillstone;");
        sb.append("overseaLicense=" + resultJsn.getString("overseaLicense") + ";");
        sb.append("HS.frame.lang=" + HS_frame_lang + ";");

        logger.info("test {}",sb.toString());
    }

    public List<String> getNames(){
        String s1 = MockUtil.readJson("8ec2a609264f4b1fac800ff280597162.txt");
        String s2 = MockUtil.readJson("56adf0b71bc540cea66e27215e2cd662.txt");
        String s3 = MockUtil.readJson("86bf04abfba5494582648b14a51d304a.txt");
        String s4 = MockUtil.readJson("137ffa6cdde3430ab0ea20c520d1c185.txt");
        String s5 = MockUtil.readJson("736b7b5088c4416b86a04f80c6cc4e65.txt");

        List<String>  dataList = Lists.newArrayList(s1,s2,s3,s4,s5);

        List<String> strings = Lists.newArrayList();
        dataList.forEach(data-> {
            JSONObject jsonObject = JSON.parseObject(data);
            JSONArray results = jsonObject.getJSONArray("results");

            results.forEach(s -> {
                JSONObject jsonObject1 = JSON.parseObject(JSON.toJSONString(s));
                strings.add(jsonObject1.getString("name"));
            });
        });



        return strings;

    }

    @org.junit.Test
    public void testUUid(){
        UUID uuid = UUID.fastUUID();
        logger.info("test:{}", uuid.toString());
        logger.info("长度:{}", "000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000001".length());

        logger.info("aaaaa:{}", "power_cycle_osinstall_a5dcfe959c82427ebb1042dca5890171".length());

    }

    @org.junit.Test
    public void testArray(){
        List<String> linkList = Lists.newLinkedList();
        linkList.add("clusconf");
        linkList.add("run");
        linkList.add("-f");
        linkList.add("/aaa");

        String[] cmds = linkList.toArray(new String[0]);
        logger.info("testArray：{}", Arrays.toString(cmds));

        // 定义规则文件内容
        ClassPathResource classPathResource = new ClassPathResource("droolsrules/drools.drl");

        // 使用 DrlParser 解析规则
        DrlParser parser = new DrlParser();
        //  RuleDescrBuilder ruleDescrBuilder = new RuleDescrBuilder("ExampleRule");
        // RuleDescr ruleDescr = null;
        PackageDescr ruleDescr=null;
        try {
            ruleDescr = parser.parse(classPathResource);
        } catch (DroolsParserException e) {

            System.err.println("DroolsParserException "+e.getMessage());

        } catch (IOException e) {
            System.err.println("IOException "+e.getMessage());
        }
        // parser失败 ruleDescr为空
        if (ruleDescr==null  ){
            List<DroolsError>  errs = parser.getErrors();
            errs.forEach( e->{
                System.err.println("error: "+e.getMessage());
            });
        }


        // 输出解析后的规则描述
        System.out.println("Parsed Rule: " + ruleDescr);
    }

}
