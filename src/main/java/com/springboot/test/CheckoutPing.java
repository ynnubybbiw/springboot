package com.springboot.test;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.*;

public class CheckoutPing {

    private static final Logger logger = LogManager.getLogger(GsonTest.class.getName());

    public static void main(String[] args) {
        // 要检测的目标主机
        String host = "127.0.0.1"; // 替换为实际目标
        long timeoutInMinutes = 1; // 超时时间，单位：分钟

        boolean b = executePingWithTimeout("172.16.4.25", 5);
        logger.info("aaaaaa:{}", b);
    }

    public static boolean executePingWithTimeout(String host, Integer timeoutMin) {

        boolean reachable = false;
        try {
            InetAddress inet = InetAddress.getByName(host);
            long startTime = System.currentTimeMillis();
            long endTime = startTime + (timeoutMin * 60 * 1000); // 5分钟后超时

            while (!reachable && System.currentTimeMillis() < endTime) {
                reachable = inet.isReachable(5000);
                if (reachable) {
                    logger.info("Host {} is reachable.", host);
                    break;
                }
                logger.info("Host {} is not reachable, retrying...", host);
                Thread.sleep(1000);
            }

        } catch (UnknownHostException e) {
            logger.error("Unknown host: {}",  host);
        } catch (IOException e) {
            logger.error("IO Exception occurred while trying to reach {}", host);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        return reachable;
    }

    public static boolean executePingWithTimeout(String host, long timeout, TimeUnit unit) {
        // 创建线程池，用于异步执行
        ExecutorService executorService = Executors.newSingleThreadExecutor();

        // 提交任务检测主机可达性
        Future<Boolean> pingTask = executorService.submit(() -> {
            try {
                InetAddress inet = InetAddress.getByName(host);
                System.out.println("Pinging " + host + " ...");
                while (true) {
                    // 每次检测是否可达，超时时间为 5000ms（5秒）
                    boolean reachable = inet.isReachable(5000);
                    if (reachable) {
                        System.out.println(host + " is reachable!");
                    } else {
                        System.out.println(host + " is not reachable!");
                    }
                    // 延迟一段时间（例如 1 秒）再进行下一次检测
                    Thread.sleep(1000);
                }
            } catch (Exception e) {
                System.out.println("Ping failed with exception: " + e.getMessage());
                return false;
            }
        });

        // 等待任务完成或超时
        try {
            pingTask.get(timeout, unit); // 等待任务完成
            return true; // 正常完成
        } catch (TimeoutException e) {
            System.out.println("Ping task timed out. Stopping...");
            pingTask.cancel(true); // 超时后中断任务
            return false;
        } catch (Exception e) {
            return false;
        } finally {
            executorService.shutdown(); // 关闭线程池
        }
    }
}
