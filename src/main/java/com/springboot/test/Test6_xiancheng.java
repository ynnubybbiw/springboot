package com.springboot.test;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.CountDownLatch;

public class Test6_xiancheng {

    private static final Logger logger = LogManager.getLogger(Test6_xiancheng.class.getName());

    private static final int threadNum = 2000;

    private CountDownLatch cdl = new CountDownLatch(threadNum);

    long timed = 0L;

    @Before
    public void start(){
        logger.info("开始测试");
    }

    @After
    public void end(){
        logger.info("结束测试，执行时长："+(System.currentTimeMillis() - timed));
    }

    @Test
    public void benchmark() throws InterruptedException {
        Thread[] threads = new Thread[threadNum];
        for (int i=0; i<threadNum; i++){
            String userId = "Tony"+i;
            logger.info("创建多线程用户："+userId);
            Thread thread = new Thread(new UserRequest(userId));
            threads[i] = thread;

            thread.start();
            cdl.countDown();
        }

        for(Thread thread : threads){
            thread.join();
        }
    }

    private class UserRequest implements Runnable {

        String userId;

        UserRequest(String userId){
            this.userId = userId;
        }

        @Override
        public void run() {
            try {
                cdl.await();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            //多线程调用方法
            logger.info("用户："+userId);
        }
    }
}
