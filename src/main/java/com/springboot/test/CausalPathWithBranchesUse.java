package com.springboot.test;

import com.springboot.bean.TreeNode;
import org.apache.commons.compress.utils.Lists;

import java.util.*;

public class CausalPathWithBranchesUse {

    // 定义因果关系图，Key是原因节点的ID，Value是后果节点ID列表
    private static Map<Integer, List<Integer>> causalGraph = new HashMap<>();

    // 定义告警名称到ID的映射
    private static Map<String, Integer> alarmNameToId = new HashMap<>();

    private static Map<Integer, List<Integer>> graph = new HashMap<>();

    // 添加因果关系
    public void addEdge(int from, int to) {
        graph.putIfAbsent(from, new ArrayList<>());
        graph.get(from).add(to);
    }

    // 构建带分支的路径树，合并多个起点为一个根节点
    public TreeNode buildMergedPathTree(List<Integer> startNodes) {
        // 创建一个合并根节点
        TreeNode mergedRoot = new TreeNode(-1);  // -1代表合并的根节点

        List<Integer> allIdList = Lists.newArrayList();

        // 对每个起点创建子节点并添加到合并根节点
        for (int start : startNodes) {
            TreeNode root = new TreeNode(start);
            mergedRoot.getChildren().add(root);
            dfsBuildTree(root, allIdList);  // 对每个子树进行DFS构建
        }

        return mergedRoot;
    }

    // 深度优先构建带分支的树
    private void dfsBuildTree(TreeNode currentNode, List<Integer> allIdList) {
        int currentValue = currentNode.getValue();
        allIdList.add(currentValue);
        if (graph.containsKey(currentValue)) {
            for (int neighbor : graph.get(currentValue)) {
                TreeNode childNode = new TreeNode(neighbor);
                currentNode.getChildren().add(childNode);  // 添加子节点（分支）
                dfsBuildTree(childNode, allIdList);  // 递归处理子节点
            }
        }
    }

    public static void main(String[] args) {
        CausalPathWithBranchesUse cp = new CausalPathWithBranchesUse();

        /*List<String> alarmList = Arrays.asList("氟泵异常", "供液压力低告警","刀片液位低","CPU超温","DCU高温","HCA卡高温","DCU故障不识别","HCA卡PCIe链路异常","节点关机","节点HCA卡掉落","节点slurm离线");


        initAlarmNameToId();

        List<Integer> nodeIdList = Lists.newArrayList();
        alarmList.forEach(alarmName -> {
            if(alarmNameToId.containsKey(alarmName)){
                nodeIdList.add(alarmNameToId.get(alarmName));
            }
        });

        initCausalGraph();*/

        //根据node表 所有数据初始化nodeMap  nodeName:nodeId

        //循环遍历实时告警   把告警信息放到对应 NodeDto下 alarmInfoList

        //alarmInfoList 不为空的留下







        // 添加因果关系
        cp.addEdge(1, 2);
        cp.addEdge(2, 3);
        cp.addEdge(3, 4);
        cp.addEdge(3, 7);
        cp.addEdge(4, 8);
        cp.addEdge(8, 9);
        cp.addEdge(8, 10);
        cp.addEdge(5, 6);
        cp.addEdge(6, 7);

        // 构建从多个起点开始的路径树，并将起点合并为一个根节点
        List<Integer> startNodes = Arrays.asList(1);  // 多个起点：1 和 5
        TreeNode root = cp.buildMergedPathTree(startNodes);

        // 打印树结构
        System.out.println("Path with merged start nodes:");
        root.printTree("");
    }


    // 初始化因果关系图
    private static void initCausalGraph() {
        // 按照给出的关系初始化
        addCausalRelation(1, 2);
        addCausalRelation(2, 3);
        addCausalRelation(3, 4);
        addCausalRelation(4, 8);
        addCausalRelation(8, 9);
        addCausalRelation(8, 10);
        addCausalRelation(3, 11);
        addCausalRelation(11, 6);
        addCausalRelation(3, 5);
        addCausalRelation(5, 7);
        /*addCausalRelation(12, 13);
        addCausalRelation(13, 14);
        addCausalRelation(14, 10);
        addCausalRelation(14, 9);
        addCausalRelation(17, 19);
        addCausalRelation(18, 19);
        addCausalRelation(5, 19);
        addCausalRelation(19, 20);
        addCausalRelation(21, 22);
        addCausalRelation(22, 9);
        addCausalRelation(22, 10);
        addCausalRelation(24, 23);
        addCausalRelation(23, 3);
        addCausalRelation(3, 4);
        addCausalRelation(25, 26);
        addCausalRelation(26, 23);
        addCausalRelation(13, 27);
        addCausalRelation(28, 13);*/
    }

    // 添加因果关系到图中
    private static void addCausalRelation(int cause, int effect) {
        causalGraph.computeIfAbsent(cause, k -> new ArrayList<>()).add(effect);

        graph.computeIfAbsent(cause, k -> new ArrayList<>()).add(effect);

    }

    // 初始化告警名称到ID的映射
    private static void initAlarmNameToId() {
        alarmNameToId.put("氟泵异常", 1);
        alarmNameToId.put("供液压力低告警", 2);
        alarmNameToId.put("刀片液位低", 3);
        alarmNameToId.put("CPU超温", 4);
        alarmNameToId.put("HCA卡高温", 5);
        alarmNameToId.put("DCU故障不识别", 6);
        alarmNameToId.put("HCA卡PCIe链路异常", 7);
        alarmNameToId.put("节点关机", 8);
        alarmNameToId.put("节点HCA卡掉落", 9);
        alarmNameToId.put("节点slurm离线", 10);
        alarmNameToId.put("DCU高温", 11);
        alarmNameToId.put("IB交换机掉落", 12);
        alarmNameToId.put("计算网不通", 13);
        alarmNameToId.put("节点宕机黑屏", 14);
        alarmNameToId.put("交换机超温", 17);
        alarmNameToId.put("节点负载过高", 18);
        alarmNameToId.put("光模块超温", 19);
        alarmNameToId.put("左侧PDM1#插箱供电模块前舱输出1路熔断器故障", 20);
        alarmNameToId.put("整刀箱掉电", 21);
        alarmNameToId.put("节点掉电", 22);
        alarmNameToId.put("CDM系统压力高", 23);
        alarmNameToId.put("CDM水路过滤器压差高", 24);
        alarmNameToId.put("HCA交换机掉落", 25);
        alarmNameToId.put("HCA端口LINK状态异常", 26);
        alarmNameToId.put("共享存储未挂载", 27);
        alarmNameToId.put("IB端口掉落", 28);
    }

}
