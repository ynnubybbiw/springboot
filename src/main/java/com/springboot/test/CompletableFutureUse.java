package com.springboot.test;

import cn.hutool.core.date.DateUtil;
import com.springboot.util.FileWriterUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Date;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;

/**
 * test use CompletableFuture  实现异步    可编排，可同步执行两个操作
 */
public class CompletableFutureUse {

    private static final Logger logger = LogManager.getLogger(CompletableFutureUse.class.getName());

    public static void main(String[] args) throws Exception {

       /* runAsync();
        supplyAsync();*/

        paralleHandl();
    }

    public static void runAsync() throws Exception {

        logger.info("方法开始执行");

        CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
            try {
                TimeUnit.SECONDS.sleep(10);
                FileWriterUtil.writerFile("runAsync test writer file. "+ DateUtil.formatDateTime(new Date()),"runAsync.txt");
            } catch (InterruptedException e) {
                logger.error("error", e);
                Thread.currentThread().interrupt();
            }
            logger.info("run end ...");
        });

        logger.info("调用CompletableFuture 完成");

        //get 会阻塞主线程  不加会不走 runAsync 里的方法
        future.get();
        logger.info("方法执行完成");
    }

    //有返回值
    public static void supplyAsync() throws Exception {
        logger.info("方法开始执行");
        CompletableFuture<String> future = CompletableFuture.supplyAsync(() -> {
            String time = DateUtil.formatDateTime(new Date());
            try {
                TimeUnit.SECONDS.sleep(10);
                FileWriterUtil.writerFile(" supplyAsync test writer file. "+ time, "supplyAsync.txt");
            } catch (InterruptedException e) {
                logger.error("error", e);
                Thread.currentThread().interrupt();
            }
            logger.info("run end ...");
            return time;
        });

        String time = future.get();
        logger.info("time = {}", time);
        logger.info("方法执行完成");
    }

    public static void paralleHandl(){

        CompletableFuture<Void> future1 = CompletableFuture.runAsync(()->{
            try {
                logger.info("future1方法开始执行");
                TimeUnit.SECONDS.sleep(10);
                FileWriterUtil.writerFile("paralleHandl test writer file. "+ DateUtil.formatDateTime(new Date()),"future1.txt");
            } catch (InterruptedException e) {
                logger.error("error", e);
                Thread.currentThread().interrupt();
            }
            logger.info("future1 run end ...");
        });

        CompletableFuture<Void> future2 = CompletableFuture.runAsync(()->{
            try {
                logger.info("future2方法开始执行");
                TimeUnit.SECONDS.sleep(15);
                FileWriterUtil.writerFile("paralleHandl test writer file. "+ DateUtil.formatDateTime(new Date()),"future2.txt");
            } catch (InterruptedException e) {
                logger.error("error", e);
                Thread.currentThread().interrupt();
            }
            logger.info("future2 run end ...");
        });

        CompletableFuture<Void> all = CompletableFuture.allOf(future1, future2).exceptionally(e -> {
            logger.error("执行失败：", e);
            return null;
        }).thenRun(() -> logger.info("执行完成。"));

        long a= System.currentTimeMillis();
        logger.info("{}:阻塞",a);
        all.join();
        long b = System.currentTimeMillis();
        logger.info("{}:阻塞结束",b);
        logger.info("用时：{}",b-a);
    }
}
