package com.springboot.test;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class testCmd {
    public static void main(String[] args)  {
        try {
            List<String> rspList = new ArrayList<>();
            Runtime run = Runtime.getRuntime();
            Process proc = run.exec("df /");
            BufferedReader in = new BufferedReader(new InputStreamReader(proc.getInputStream()));
            PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(proc.getOutputStream())), true);
            out.println("exit");
            String rspLine = "";
            while ((rspLine = in.readLine()) != null) {
                System.out.println(rspLine);
                rspList.add(rspLine);
            }
            proc.waitFor();
            in.close();
            out.close();
            proc.destroy();
            String s = rspList.get(1);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
