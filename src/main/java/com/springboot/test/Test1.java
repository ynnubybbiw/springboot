package com.springboot.test;

import com.google.common.collect.Lists;
import com.springboot.util.CPUMonitorCalc;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;
import java.util.List;

public class Test1 {

    private static final Logger logger = LogManager.getLogger(Test1.class.getName());

    public static void main(String[] args) {
        char ideDevice = 'a';
        List<String> targetDevAll = Lists.newArrayList();
        targetDevAll.add("vda");
        targetDevAll.add("vdb");
        targetDevAll.add("vdc");
        targetDevAll.add("vdd");
        String dev = "vd" + ideDevice;
        while (true){
            if(targetDevAll.contains(dev)){
                ideDevice = (char) (ideDevice + '\001');
                dev = "vd"+ideDevice;
            }else {
                break;
            }
        }
        logger.info(dev);

        for(int i=0;i<200;i++){
            ideDevice = (char)i;
            logger.info(ideDevice);
        }

        for(int i=0; i>=-200;i--){
            ideDevice = (char)i;
            logger.info(ideDevice);
        }
    }

    @Test
    public void getcpurate() throws InterruptedException {
        for (int i = 0; i < 2; i++) {
            new Thread(() -> {
                while (true) {
                    long bac = 1000000;
                    bac = bac >> 1;
                }
            }).start();
        }
        while (true) {
            Thread.sleep(5000);
            logger.info(CPUMonitorCalc.getInstance().getProcessCpu());
        }
    }
}
