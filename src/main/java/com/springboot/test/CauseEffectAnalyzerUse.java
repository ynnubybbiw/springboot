package com.springboot.test;

import com.springboot.bean.TreeNodeTest;
import org.apache.commons.compress.utils.Lists;

import java.util.*;

public class CauseEffectAnalyzerUse {

    private Map<Integer, TreeNodeTest> nodes;

    public CauseEffectAnalyzerUse() {
        this.nodes = new HashMap<>();
    }

    // 初始化节点
    public void addNode(int id, String name) {
        nodes.put(id, new TreeNodeTest(id, name));
    }

    // 添加关联关系
    public void addRelation(int fromId, int toId) {
        TreeNodeTest fromNode = nodes.get(fromId);
        TreeNodeTest toNode = nodes.get(toId);
        if (fromNode != null && toNode != null) {
            fromNode.addChild(toNode);
        }
    }

    // 获取完整的上游因果树
    public TreeNodeTest getUpstreamCauseTree(int targetId, List<Integer> nodeIdList) {
        TreeNodeTest rootNode = new TreeNodeTest(targetId, nodes.get(targetId).getName());
        buildUpstreamTree(rootNode, targetId, new HashSet<>(), nodeIdList);
        return rootNode;
    }

    private void buildUpstreamTree(TreeNodeTest currentTreeNodeTest, int currentId, Set<Integer> visited, List<Integer> nodeIdList) {
        if (visited.contains(currentId)) {
            return;
        }
        visited.add(currentId);
        nodeIdList.add(currentId);

        TreeNodeTest originalNode = nodes.get(currentId);
        for (TreeNodeTest parent : originalNode.getParents()) {
            TreeNodeTest parentTreeNodeTest = new TreeNodeTest(parent.getId(), parent.getName());
            currentTreeNodeTest.getParents().add(parentTreeNodeTest);
            buildUpstreamTree(parentTreeNodeTest, parent.getId(), visited, nodeIdList);
        }
    }

    // 获取完整的下游影响树
    public TreeNodeTest getDownstreamEffectTree(int targetId, List<Integer> nodeIdList) {
        TreeNodeTest rootNode = new TreeNodeTest(targetId, nodes.get(targetId).getName());
        buildDownstreamTree(rootNode, targetId, new HashSet<>(), nodeIdList);
        return rootNode;
    }

    private void buildDownstreamTree(TreeNodeTest currentTreeNodeTest, int currentId, Set<Integer> visited, List<Integer> nodeIdList) {
        if (visited.contains(currentId)) {
            return;
        }
        visited.add(currentId);
        nodeIdList.add(currentId);

        TreeNodeTest originalNode = nodes.get(currentId);
        for (TreeNodeTest child : originalNode.getChildren()) {
            TreeNodeTest childTreeNodeTest = new TreeNodeTest(child.getId(), child.getName());
            currentTreeNodeTest.addChild(childTreeNodeTest);
            buildDownstreamTree(childTreeNodeTest, child.getId(), visited, nodeIdList);
        }
    }

    // 获取树的最大深度
    public int getTreeDepth(TreeNodeTest node) {
        if (node == null) {
            return 0;
        }
        int maxDepth = 0;
        for (TreeNodeTest child : node.getChildren()) {
            maxDepth = Math.max(maxDepth, getTreeDepth(child));
        }
        return maxDepth + 1;
    }

    // 获取树的最大宽度
    public int getTreeWidth(TreeNodeTest node) {
        if (node == null) {
            return 0;
        }

        int maxWidth = 0;
        Queue<TreeNodeTest> queue = new LinkedList<>();
        queue.offer(node);

        while (!queue.isEmpty()) {
            int levelSize = queue.size();
            maxWidth = Math.max(maxWidth, levelSize);

            for (int i = 0; i < levelSize; i++) {
                TreeNodeTest current = queue.poll();
                queue.addAll(current.getChildren());
            }
        }
        return maxWidth;
    }


    // 打印因果树（上游）
    private static void printCauseTree(TreeNodeTest node, String prefix, boolean isRoot) {
        if (isRoot) {
            System.out.println(prefix + "└── " + node);
        }
        for (TreeNodeTest parent : node.getParents()) {
            System.out.println(prefix + "    ↑");
            System.out.println(prefix + "    " + parent);
            printCauseTree(parent, prefix + "    ", false);
        }
    }

    // 打印影响树（下游）
    private static void printEffectTree(TreeNodeTest node, String prefix, boolean isRoot) {
        if (isRoot) {
            System.out.println(prefix + "└── " + node);
        }
        for (TreeNodeTest child : node.getChildren()) {
            System.out.println(prefix + "    ↓");
            System.out.println(prefix + "    " + child);
            printEffectTree(child, prefix + "    ", false);
        }
    }

    public static void main(String[] args) {
        CauseEffectAnalyzerUse analyzer = new CauseEffectAnalyzerUse();

        // 添加所有节点
        Map<Integer, String> nodeDefinitions = new HashMap<>();
        nodeDefinitions.put(1, "氟泵异常");
        nodeDefinitions.put(2, "供液压力低告警");
        nodeDefinitions.put(3, "刀片液位低");
        nodeDefinitions.put(4, "CPU超温");
        nodeDefinitions.put(5, "HCA卡高温");
        nodeDefinitions.put(6, "DCU故障不识别");
        nodeDefinitions.put(7, "HCA卡PCIe链路异常");
        nodeDefinitions.put(8, "节点关机");
        nodeDefinitions.put(9, "节点HCA卡掉落");
        nodeDefinitions.put(10, "节点slurm离线");
        nodeDefinitions.put(11, "DCU高温");
        nodeDefinitions.put(12, "IB交换机掉落");
        nodeDefinitions.put(13, "计算网不通");
        nodeDefinitions.put(14, "节点宕机黑屏");
        nodeDefinitions.put(17, "交换机超温");
        nodeDefinitions.put(18, "节点负载过高");
        nodeDefinitions.put(19, "光模块超温");
        nodeDefinitions.put(20, "左侧PDM1#插箱供电模块前舱输出1路熔断器故障");
        nodeDefinitions.put(21, "整刀箱掉电");
        nodeDefinitions.put(22, "节点掉电");
        nodeDefinitions.put(23, "CDM系统压力高");
        nodeDefinitions.put(24, "CDM水路过滤器压差高");
        nodeDefinitions.put(25, "HCA交换机掉落");
        nodeDefinitions.put(26, "HCA端口LINK状态异常");
        nodeDefinitions.put(27, "共享存储未挂载");
        nodeDefinitions.put(28, "IB端口掉落");

        // 初始化所有节点
        for (Map.Entry<Integer, String> entry : nodeDefinitions.entrySet()) {
            analyzer.addNode(entry.getKey(), entry.getValue());
        }

        // 添加所有关联关系
        int[][] relations = {
                {1, 2}, {2, 3}, {3, 4}, {4, 8}, {8, 9}, {8, 10},
                {3, 11}, {11, 6}, {3, 5}, {5, 7}, {12, 13},
                {14, 10}, {14, 9}, {17, 19}, {18, 19}, {5, 19},
                {20, 21}, {21, 22}, {22, 9}, {22, 10}, {24, 23},
                {23, 3}, {25, 26}, {26, 13}, {13, 27},
                {28, 13}
        };

        for (int[] relation : relations) {
            analyzer.addRelation(relation[0], relation[1]);
        }


        List<Integer> nodeIdList = Lists.newArrayList();

        // 分析节点3的因果链
        int targetNodeId = 3;

        // 获取上游因果树
        System.out.println("导致刀片液位低的原因链：");
        TreeNodeTest upstreamTree = analyzer.getUpstreamCauseTree(targetNodeId, nodeIdList);
        printCauseTree(upstreamTree, "", true);

        // 获取下游影响树
        System.out.println("\n刀片液位低导致的影响链：");
        TreeNodeTest downstreamTree = analyzer.getDownstreamEffectTree(targetNodeId,  nodeIdList);
        printEffectTree(downstreamTree, "", true);

        // 打印统计信息
        System.out.println("\n统计信息：");
        System.out.println("最大影响深度: " + analyzer.getTreeDepth(downstreamTree));
        System.out.println("最大影响宽度: " + analyzer.getTreeWidth(downstreamTree));
    }

}
