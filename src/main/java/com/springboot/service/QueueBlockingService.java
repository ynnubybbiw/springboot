package com.springboot.service;

import com.springboot.bean.Menu;

/**
 * @author 29678
 */
public interface QueueBlockingService {

    void producer(Menu menu);

}
