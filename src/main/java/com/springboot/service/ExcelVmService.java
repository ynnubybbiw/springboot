package com.springboot.service;

import com.springboot.bean.ExcelVm;

import java.util.List;

public interface ExcelVmService {

    List<ExcelVm> findAll();

    void save(ExcelVm excelVm);

    void update(ExcelVm excelVm);

    void saveOrUpdate(ExcelVm excelVm);

}
