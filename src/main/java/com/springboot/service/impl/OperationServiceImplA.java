package com.springboot.service.impl;

import com.springboot.service.OperationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

@Service("operationA")
public class OperationServiceImplA implements OperationService {

    public static final Logger logger = LogManager.getLogger(OperationServiceImplA.class.getName());

    @Override
    public String operation(String op) {
        logger.info("经过方法A：{}",op);
        return null;
    }
}
