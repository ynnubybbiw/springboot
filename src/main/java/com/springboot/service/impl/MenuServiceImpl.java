package com.springboot.service.impl;

import com.springboot.bean.Menu;
import com.springboot.mapper.MenuMapper;
import com.springboot.service.MenuService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MenuServiceImpl implements MenuService {

    private static final Logger logger = LogManager.getLogger(MenuServiceImpl.class.getName());

    @Autowired
    private MenuMapper menuMapper;

    @Override
    public List<Menu> findAll() {
        return menuMapper.findAll();
    }
}
