package com.springboot.service.impl;

import com.springboot.bean.LoginUser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * 用于获取数据库用户信息。  根据用户输入的用户名
 */
@Service
public class UserDetailServiceImpl implements UserDetailsService {

    public static final Logger logger = LogManager.getLogger(UserDetailServiceImpl.class.getName());

    @Override
    public UserDetails loadUserByUsername(String userName){
        LoginUser loginUser = new LoginUser();
        loginUser.setUserName(userName);
        loginUser.setPassword(new BCryptPasswordEncoder().encode("123456"));
        logger.info("获取loginuser完成");
        return loginUser;
    }
}
