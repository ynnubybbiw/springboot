package com.springboot.service.impl;

import com.springboot.service.OperationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

@Service("operationB")
public class OperationServiceImplB implements OperationService {

    public static final Logger logger = LogManager.getLogger(OperationServiceImplB.class.getName());

    @Override
    public String operation(String op) {
        logger.info("经过方法B：{}",op);
        return null;
    }
}
