package com.springboot.service.impl;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;

/**
 * 全局异常处理   可以获取所有抛出的异常
 */
@ControllerAdvice
public class ExceptionTest {

    private static final Logger logger = LogManager.getLogger(ExceptionTest.class.getName());

    @org.springframework.web.bind.annotation.ExceptionHandler(value = Exception.class)
    public void handlerSellerException(Exception e) {
        if(e instanceof CException){
            logger.info("55555555555555555{}",e.getMessage());
        }else {
            logger.info("77777777777777777{}",e.getMessage());
        }
    }



}
