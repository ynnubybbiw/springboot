package com.springboot.service.impl;

import com.alibaba.fastjson.JSON;
import com.springboot.bean.Menu;
import com.springboot.queue.QueueBlocking;
import com.springboot.service.QueueBlockingService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author 29678
 */
@Service
public class QueueBlockingServiceImpl implements QueueBlockingService {

    private static final Logger logger = LogManager.getLogger(QueueBlockingServiceImpl.class.getName());

    @Autowired
    private QueueBlocking queueBlocking;

    @Override
    public void producer(Menu menu) {
        queueBlocking.produce(menu);
        synchronized (this) {
            notify();
        }
    }

    @PostConstruct
    public void handler(){
        ExecutorService executorService = Executors.newCachedThreadPool();
        executorService.execute(()->{
            while (true){
                synchronized (this) {
                    try {
                        wait();
                    } catch (Exception e) {
                       logger.error("error",e);
                    }
                }
                Menu menu = queueBlocking.consume();
                logger.info("handler : {}", JSON.toJSONString(menu));
            }
        });
    }
}
