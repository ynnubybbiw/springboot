package com.springboot.service.impl;

import com.springboot.bean.ExcelVm;
import com.springboot.mapper.ExcelVmMapper;
import com.springboot.service.ExcelVmService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExcelVmServiceImpl implements ExcelVmService {

    private static final Logger logger = LogManager.getLogger(ExcelVmServiceImpl.class.getName());

    @Autowired
    private ExcelVmMapper excelVmMapper;

    @Override
    public List<ExcelVm> findAll() {
        return excelVmMapper.findAll();
    }

    @Override
    public void save(ExcelVm excelVm) {
        excelVmMapper.addExcelVm(excelVm);
    }

    @Override
    public void update(ExcelVm excelVm) {
        excelVmMapper.updateExcelVm(excelVm);
    }

    @Override
    public void saveOrUpdate(ExcelVm excelVm) {
        excelVmMapper.saveOrUpdate(excelVm);
    }
}
