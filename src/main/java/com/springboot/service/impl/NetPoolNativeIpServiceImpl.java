package com.springboot.service.impl;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.springboot.bean.NetPoolNativeIp;
import com.springboot.mapper.NetPoolNativeIpMapper;
import com.springboot.service.NetPoolNativeIpService;
import org.springframework.stereotype.Service;

@Service
public class NetPoolNativeIpServiceImpl extends ServiceImpl<NetPoolNativeIpMapper, NetPoolNativeIp> implements NetPoolNativeIpService {


    @Override
    public void add(NetPoolNativeIp netPoolNativeIp) {
        this.baseMapper.add(IdUtil.getSnowflakeNextIdStr(),netPoolNativeIp.getIp(),netPoolNativeIp.getNetpoolid(), netPoolNativeIp.getPlatformId(),netPoolNativeIp.getStatus());
    }
}
