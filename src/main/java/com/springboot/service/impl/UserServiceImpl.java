package com.springboot.service.impl;

import com.springboot.bean.User;
import com.springboot.mapper.UserMapper;
import com.springboot.service.IUserService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements IUserService {

    private static final Logger logger = LogManager.getLogger(UserServiceImpl.class.getName());

    @Autowired
    private UserMapper userMapper;

    @Override
    public int addUser(User user) {
        return userMapper.addUser(user);
    }

    @Override
    public List<User> getAllUser() {
        return userMapper.findAll();
    }

    @Async
    @Override
    public void testAsync() {
        logger.info("testAsync---2");
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        logger.info("testAsync---3");
    }

    @Async
    public void testAsync1() {
        logger.info("testAsync---2");
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        logger.info("testAsync---3");
    }
}
