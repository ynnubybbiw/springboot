package com.springboot.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.springboot.bean.NetPoolNativeIp;

public interface NetPoolNativeIpService extends IService<NetPoolNativeIp> {

    void add(NetPoolNativeIp netPoolNativeIp);



}
