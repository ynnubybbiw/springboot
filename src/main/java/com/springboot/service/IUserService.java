package com.springboot.service;

import com.springboot.bean.User;

import java.util.List;

public interface IUserService {

    int addUser(User user);

    List<User> getAllUser();

    void testAsync();

    default String testInterfaceMathod(){
        return "123";
    }
}
