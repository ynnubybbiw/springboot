package com.springboot.service;

import com.springboot.bean.Menu;

import java.util.List;

public interface MenuService {

    List<Menu> findAll();

}
