package com.springboot.service;

public interface OperationService {

    String operation(String op);
}
