package com.springboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.springboot.bean.NetPoolNativeIp;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface NetPoolNativeIpMapper extends BaseMapper<NetPoolNativeIp> {

    @Select("INSERT IGNORE INTO net_pool_native_ip(`id`,`ip`,`netpoolid`,`platform_id`,`create_time`,`status`,`sync`) "
            +
            " VALUES(#{id},#{ip},#{netpoolid},#{platformId},NOW(),#{status},true)")
    void add(@Param("id") String id, @Param("ip") String ip, @Param("netpoolid") String netpoolId, @Param("platformId") String platformId, @Param("status") String status);

}
