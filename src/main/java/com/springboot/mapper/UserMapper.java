package com.springboot.mapper;


import com.springboot.bean.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * mapper 中的方法名要和 它所对应的xml 中的操作的id所对应。
 */

@Mapper
public interface UserMapper {

    User getById(Integer id);

    int addUser(User user);

    List<User> findAll();
}
