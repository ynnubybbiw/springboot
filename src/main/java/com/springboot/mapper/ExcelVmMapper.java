package com.springboot.mapper;

import com.springboot.bean.ExcelVm;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ExcelVmMapper {

    List<ExcelVm> findAll();

    void addExcelVm(ExcelVm excelVm);

    void updateExcelVm(ExcelVm excelVm);

    void saveOrUpdate(ExcelVm excelVm);

}
