package com.springboot.util;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * okHttp 请求util
 */
public class OkHttpUtil {

    private static final Logger logger = LoggerFactory.getLogger(OkHttpUtil.class);

    public static void main(String[] args) throws IOException {

        OkHttpClient client = new OkHttpClient().newBuilder().callTimeout(60000, TimeUnit.MILLISECONDS).readTimeout(60000,TimeUnit.MILLISECONDS).build();
        Request request = new Request.Builder()
                .url("http://localhost:31123/api/ovmmgmt/asset/getAvailableIP/2d6cb718-1c96-4592-9057-6f500b0d4657/ecfa7839-8e97-485e-afed-5ffbf28238af/123456")
                .method("GET", null)
                .build();
        Response response = client.newCall(request).execute();

        logger.info(Objects.requireNonNull(response.body()).string());
    }
}
