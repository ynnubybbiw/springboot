package com.springboot.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * https调用工具类
 * @author dubin
 */
public class HttpsRestTemplate {

    private static final Logger logger = LoggerFactory.getLogger(HttpsRestTemplate.class);

    private static RestTemplate restTemplate;

    private HttpsRestTemplate() {}

    static {
        HttpsClientRequestFactory httpsClientRequestFactory = new HttpsClientRequestFactory();
        httpsClientRequestFactory.setConnectTimeout(60000);
        httpsClientRequestFactory.setReadTimeout(60000);
        restTemplate = new RestTemplate(httpsClientRequestFactory);
    }


    public static JSONObject sendGet(String url){
        return restTemplate.getForObject(url, JSONObject.class);
    }

    public static JSONObject sendGet(Map<String,String> paramMap, Map<String,String> headerMap,String url){
        HttpHeaders headers = new HttpHeaders();
        headerMap.forEach(headers::set);
        HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<>(null, headers);
        ResponseEntity<JSONObject> response = restTemplate.exchange(url, HttpMethod.GET, httpEntity, JSONObject.class,paramMap);
        return response.getBody();
    }

    public static JSONObject sendPostForm(Map<String,String> params, String url){
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
            headers.set(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON_VALUE);
            headers.set(HttpHeaders.ACCEPT_CHARSET, StandardCharsets.UTF_8.name());
            MultiValueMap<String, String> reqMap= new LinkedMultiValueMap<>();
            params.forEach(reqMap::add);
            HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(reqMap, headers);
            restTemplate.getMessageConverters().add(0, new FastJsonHttpMessageConverter());
            ResponseEntity<String> response = restTemplate.postForEntity(url, requestEntity, String.class);
            return JSON.parseObject(response.getBody());
    }

    /**
     * 发送https post   form 请求
     * @param url
     * @param paramMap
     * @param headers
     * @return
     */
    public static String sendPostform(String url, Map<String, Object> paramMap, Map<String,String> headers) {
        CloseableHttpClient httpClient;
        CloseableHttpResponse httpResponse = null;
        String result = "";
        // 创建httpClient实例
        httpClient = HttpsUtils.createSSLClientDefault();
        // 创建httpPost远程连接实例
        HttpPost httpPost = new HttpPost(url);
        // 配置请求参数实例
        RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(35000)// 设置连接主机服务超时时间
                .setConnectionRequestTimeout(35000)// 设置连接请求超时时间
                .setSocketTimeout(60000)// 设置读取数据连接超时时间
                .build();
        // 为httpPost实例设置配置
        httpPost.setConfig(requestConfig);
        httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded");
        // 设置请求头
        if (headers != null) {
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                httpPost.addHeader(entry.getKey(),entry.getValue());
            }
        }
        // 封装post请求参数
        if (null != paramMap && paramMap.size() > 0) {
            List<NameValuePair> nvps = new ArrayList<>();
            // 通过map集成entrySet方法获取entity
            Set<Map.Entry<String, Object>> entrySet = paramMap.entrySet();
            // 循环遍历，获取迭代器
            for (Map.Entry<String, Object> mapEntry : entrySet) {
                nvps.add(new BasicNameValuePair(mapEntry.getKey(), mapEntry.getValue().toString()));
            }

            // 为httpPost设置封装好的请求参数
            try {
                httpPost.setEntity(new UrlEncodedFormEntity(nvps, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                logger.error("error",e);
            }
        }
        try {
            // httpClient对象执行post请求,并返回响应参数对象
            httpResponse = httpClient.execute(httpPost);
            // 从响应对象中获取响应内容
            org.apache.http.HttpEntity entity = httpResponse.getEntity();
            result = EntityUtils.toString(entity);
        } catch (IOException e) {
            logger.error("error",e);
        } finally {
            // 关闭资源
            if (null != httpResponse) {
                try {
                    httpResponse.close();
                } catch (IOException e) {
                    logger.error("error",e);
                }
            }
            try {
                httpClient.close();
            } catch (IOException e) {
                logger.error("error",e);
            }
        }
        return result;
    }

    /**
     * 发送https   post   json 请求
     * @param url
     * @param json
     * @param headers
     * @return
     */
    public static String sendPostJson(String url,String json, Map<String,String> headers) {
        // 创建连接池
        CloseableHttpClient httpClient = HttpsUtils.createSSLClientDefault();
        CloseableHttpResponse httpResponse = null;

        // 创建httppost对象
        HttpPost httpPost = new HttpPost(url);
        // 配置请求参数实例
        RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(35000)// 设置连接主机服务超时时间
                .setConnectionRequestTimeout(35000)// 设置连接请求超时时间
                .setSocketTimeout(60000)// 设置读取数据连接超时时间
                .build();
        // 为httpPost实例设置配置
        httpPost.setConfig(requestConfig);
        // 设置请求头
        if (headers != null) {
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                httpPost.addHeader(entry.getKey(),entry.getValue());
            }
        }
        // 给httppost对象设置json格式的参数
        StringEntity httpEntity = new StringEntity(json,"utf-8");
        // 设置请求格式
        httpPost.setHeader("Content-type","application/json");
        // 传参
        httpPost.setEntity(httpEntity);

        // 发送请求，并获取返回值
        try {
            httpResponse = httpClient.execute(httpPost);
            // 将response转成String并存储在result中
            org.apache.http.HttpEntity entity = httpResponse.getEntity();
            return EntityUtils.toString(entity);
        } catch (IOException e) {
            logger.error("error",e);
        }// 关闭资源
        if (null != httpResponse) {
            try {
                httpResponse.close();
            } catch (IOException e) {
                logger.error("error",e);
            }
        }
        try {
            httpClient.close();
        } catch (IOException e) {
            logger.error("error",e);
        }
        return "error";
    }

    /**
     * 发送https  get请求
     * @param url
     * @param headers
     * @return
     */
    public static String sendGet(String url, Map<String,String> headers) {
        CloseableHttpClient httpClient = null;
        CloseableHttpResponse response = null;
        String result = "";
        try {
            // 通过址默认配置创建一个httpClient实例
            httpClient = HttpsUtils.createSSLClientDefault();
            // 创建httpGet远程连接实例
            HttpGet httpGet = new HttpGet(url);
            if (headers != null) {
                for (Map.Entry<String, String> entry : headers.entrySet()) {
                    httpGet.addHeader(entry.getKey(),entry.getValue());
                }
            }
            // 设置配置请求参数
            RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(35000)// 连接主机服务超时时间
                    .setConnectionRequestTimeout(35000)// 请求超时时间
                    .setSocketTimeout(60000)// 数据读取超时时间
                    .build();
            // 为httpGet实例设置配置
            httpGet.setConfig(requestConfig);
            // 执行get请求得到返回对象
            response = httpClient.execute(httpGet);
            // 通过返回对象获取返回数据
            org.apache.http.HttpEntity entity = response.getEntity();
            // 通过EntityUtils中的toString方法将结果转换为字符串
            result = EntityUtils.toString(entity);
        } catch (IOException e) {
            logger.error("error",e);
        } finally {
            // 关闭资源
            if (null != response) {
                try {
                    response.close();
                } catch (IOException e) {
                    logger.error("error",e);
                }
            }
            if (null != httpClient) {
                try {
                    httpClient.close();
                } catch (IOException e) {
                    logger.error("error",e);
                }
            }
        }
        return result;
    }

    /**
     * 发送https delete请求
     */
    public static String httpDelete(String url,Map<String,String> headers){

        String content = "";
        CloseableHttpClient closeableHttpClient = HttpsUtils.createSSLClientDefault();
        HttpDelete httpdelete = new HttpDelete(url);
        //设置header
        if (headers != null && headers.size() > 0) {
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                httpdelete.setHeader(entry.getKey(),entry.getValue());
            }
        }
        CloseableHttpResponse httpResponse = null;
        try {
            // 设置配置请求参数
            RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(35000)// 连接主机服务超时时间
                    .setConnectionRequestTimeout(35000)// 请求超时时间
                    .setSocketTimeout(60000)// 数据读取超时时间
                    .build();
            // 为httpGet实例设置配置
            httpdelete.setConfig(requestConfig);
            httpResponse = closeableHttpClient.execute(httpdelete);
            org.apache.http.HttpEntity entity = httpResponse.getEntity();
            content = EntityUtils.toString(entity, StandardCharsets.UTF_8);
        } catch (Exception e) {
            logger.error("error",e);
        }finally{
            try {
                if(null != httpResponse){
                    httpResponse.close();
                }
            } catch (IOException e) {
                logger.error("error",e);
            }
        }
        try {   //关闭连接、释放资源
            closeableHttpClient.close();
        } catch (IOException e) {
            logger.error("error",e);
        }
        return content;
    }
}
