package com.springboot.util;

import org.apache.commons.codec.binary.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;

/**
 * 使用密钥对数据进行加密解密
 */
public class AESUtil {

    private static final Logger logger = LoggerFactory.getLogger(AESUtil.class);

    //信息加密
    public static String encrypt(String secretKey, String data) {
        try {

            KeyGenerator kgen = KeyGenerator.getInstance("AES");
            SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
            secureRandom.setSeed(secretKey.getBytes());
            kgen.init(128, secureRandom);
            SecretKey key = kgen.generateKey();

            Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
            cipher.init(Cipher.ENCRYPT_MODE, key);

            String token = String.valueOf(Hex.encodeHex(cipher.doFinal(data.getBytes(StandardCharsets.UTF_8))));
            logger.info("加密后的token：{}",token);
            return token;

        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            return null;
        }
    }


    /**
     * 解密方法
     * @param data 待解密数据
     * @return
     */
    public static String decrypt(String secretKey, String data) {
        String decryptedData = "";
        try {
            KeyGenerator kgen = KeyGenerator.getInstance("AES");
            SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
            secureRandom.setSeed(secretKey.getBytes());
            kgen.init(128, secureRandom);
            SecretKey key = kgen.generateKey();
            Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
            cipher.init(Cipher.DECRYPT_MODE, key);
            decryptedData = new String(cipher.doFinal(Hex.decodeHex(data.toCharArray())), StandardCharsets.UTF_8);
            return decryptedData;
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        return decryptedData;
    }


    public static void main(String[] args) {

        String aaaaa = encrypt("1233", "aaaaabbbbbcccccdddddeeeeefffffvvvvvdsssssss");
        logger.info(aaaaa);

        String decrypt = decrypt("1233", aaaaa);
        logger.info(decrypt);

    }

}
