package com.springboot.util;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;


/**
 * @author 29678
 */
public class TokenCache {

    private static final Logger logger = LoggerFactory.getLogger(TokenCache.class);
    private static final LoadingCache<String, String> LOCALCACHE;

    private TokenCache() {}

    public static void setKey(String key, String value) {
        LOCALCACHE.put(key, value);
    }

    public static String getKey(String key) {
        String value = null;

        try {
            value = LOCALCACHE.get(key);
            if (StringUtils.isEmpty(value)) {
                return null;
            } else {
                if (key.startsWith("token.")) {

                    //验证token是否过期 过期，调用

                    LOCALCACHE.invalidate(key);
                }

                return value;
            }
        } catch (ExecutionException var4) {
            logger.error("getKey()方法错误", var4);
            return null;
        }
    }

    // 如果能从 LOCALCACHE 中取到值，就返回，否则调用load方法
    static {
        LOCALCACHE = CacheBuilder.newBuilder().initialCapacity(1000).maximumSize(10000L).expireAfterWrite(1700L, TimeUnit.SECONDS).build(new CacheLoader<String, String>() {
            @NotNull
            @Override
            public String load(@NotNull String s) throws Exception {
                logger.info("开始调用load：{}",s);
                return "45678";
            }
        });
    }


    public static void main(String[] args) {
        setKey("123","asdf");
        String key = getKey("1234");
        logger.info("key:{}",key);
    }

}
