package com.springboot.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;

/**
 * 根据时间戳（或id），生成6位编码
 */
public class SeqCodeUtil {
    private static final Logger logger = LoggerFactory.getLogger(SeqCodeUtil.class);

    public static SeqCodeUtil seqCodeUtil;

    @PostConstruct
    public void init() {
        seqCodeUtil = this;
    }

    private static final String MODEL = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    /**
     * 进制长度
     */
    private static final int BIN_LEN = MODEL.length();

    public static String getSeqCode() {
        return timeToCode(System.currentTimeMillis());
    }

    private static String timeToCode(Long id) {
        char[] charArray = MODEL.toCharArray();
        char[] buf = new char[BIN_LEN];
        int charPos = BIN_LEN;

        // 当id除以数组长度结果大于0，则进行取模操作，并以取模的值作为数组的坐标获得对应的字符
        while (id / BIN_LEN > 0) {
            int index = (int) (id % BIN_LEN);
            buf[--charPos] = charArray[index];
            id /= BIN_LEN;
        }

        buf[--charPos] = charArray[(int) (id % BIN_LEN)];
        // 将字符数组转化为字符串
        String result = new String(buf, charPos, BIN_LEN - charPos);
        logger.info("result : {}",result);
        if(result.length()>6){
            result = result.substring(1);
        }
        return result;
    }

    public static void main(String[] args) {
        String seqCode = getSeqCode();
        logger.info(seqCode);
        logger.info(MODEL.length()+"");
    }

}
