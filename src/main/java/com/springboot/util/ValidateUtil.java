package com.springboot.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.validation.annotation.Validated;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;

/**
 * 参数检验，可以统一检验参数，  需要在实体类配置好核验规则。
 */
public class ValidateUtil {

    private static final Logger logger = LoggerFactory.getLogger(ValidateUtil.class);

    private static final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();

    public static void validate(Object bean){

        /*Validated validatedAnn = AnnotationUtils.findAnnotation(bean.getClass(), Validated.class);
        Class<?>[] groups =validatedAnn != null ? validatedAnn.value() : new Class[0];
        Set<ConstraintViolation<Object>> result =  validator.validate(bean,groups);*/
        Set<ConstraintViolation<Object>> result =  validator.validate(bean);

        result.forEach(objectConstraintViolation -> logger.info(objectConstraintViolation.getMessage()));
    }
}
