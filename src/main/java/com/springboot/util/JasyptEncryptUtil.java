package com.springboot.util;

import org.jasypt.encryption.StringEncryptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 提供加密/解密方法工具类
 */
public class JasyptEncryptUtil {

    private static final Logger logger = LoggerFactory.getLogger(JasyptEncryptUtil.class);

    public static String encrypt(String data) {
        try {
            StringEncryptor stringEncryptor = SpringBeanFactoryUtils.getApplicationContext().getBean(StringEncryptor.class);
            return stringEncryptor.encrypt(data);
        } catch (Exception var2) {
            var2.printStackTrace();
            return null;
        }
    }

    public static String desEncrypt(String data) {
        try {
            StringEncryptor stringEncryptor = SpringBeanFactoryUtils.getApplicationContext().getBean(StringEncryptor.class);
            return stringEncryptor.decrypt(data);
        } catch (Exception var2) {
            logger.info("解密失败:{}", data);
            return data;
        }
    }

}
