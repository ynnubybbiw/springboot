package com.springboot.util;

import com.google.common.collect.Maps;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.joda.time.Months;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;


public class TimeFormatUtil {

    private static final Logger logger = LoggerFactory.getLogger(TimeFormatUtil.class);

    /**
     * 八小时时区转换 UTC->CST 东八区 8小时offset
     */
    private static final long TRANS_TIME_ZONE = 28800000L;

    public static final DateTimeFormatter FORMATTER_MONTH_EN = DateTimeFormatter.ofPattern("MMM", Locale.ENGLISH);
    public static final DateTimeFormatter FORMATTER_WEEK_EN = DateTimeFormatter.ofPattern("E", Locale.ENGLISH);
    private static final DateTimeFormatter FORMATTER_TIME_STE = DateTimeFormatter.ofPattern("HH:mm:ss");
    private static final DateTimeFormatter FORMATTER_DATE = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public static String getDuration(Date endsAt, Date startsAt) {
        //logger.info("计算持续时间endsAt:{}，startsAt{}",endsAt,startsAt);
        long mss = (endsAt.getTime() - startsAt.getTime()) / 1000;//除以1000是为了转换成秒
        StringBuffer dateTimes = new StringBuffer();
        long days = mss / (60 * 60 * 24);
        long hours = (mss % (60 * 60 * 24)) / (60 * 60);
        long minutes = (mss % (60 * 60)) / 60;
        long seconds = mss % 60;
        if (days > 0) {
            dateTimes.append(days).append("天").append(hours).append("小时").append(minutes).append("分钟").append(seconds).append("秒");
        } else if (hours > 0) {
            dateTimes.append(hours).append("小时").append(minutes).append("分钟").append(seconds).append("秒");
        } else if (minutes > 0) {
            dateTimes.append(minutes).append("分钟").append(seconds).append("秒");
        } else {
            dateTimes.append(seconds).append("秒");
        }
        return dateTimes.toString();
    }

    /**
     * 根据时间戳 获取  英文缩写 月
     * @param time
     * @return
     */
    public static String getMonth(Long time){
        Instant instant = Instant.ofEpochMilli(time);
        LocalDateTime dateTime = LocalDateTime.ofInstant(instant, ZoneId.of("+8"));
        return FORMATTER_MONTH_EN.format(dateTime);
    }

    /**
     * 根据时间戳 获取  英文缩写 周
     * @param time
     * @return
     */
    public static String getWeek(Long time){
        Instant instant = Instant.ofEpochMilli(time);
        LocalDateTime dateTime = LocalDateTime.ofInstant(instant, ZoneId.of("+8"));
        return FORMATTER_WEEK_EN.format(dateTime);
    }
    /**
     * @Description: 获得时间时分秒
     * @Author: lht
     * @Date: 2020/5/29 17:30
     */
    public static String getTimesStr(Long time){
        Instant instant = Instant.ofEpochMilli(time);
        LocalDateTime dateTime = LocalDateTime.ofInstant(instant, ZoneId.of("+8"));
        return FORMATTER_TIME_STE.format(dateTime);
    }

    public static String getDateStr(Long time){
        if(null == time){
            return null;
        }
        Instant instant = Instant.ofEpochMilli(time);
        LocalDateTime dateTime = LocalDateTime.ofInstant(instant, ZoneId.of("+8"));
        return FORMATTER_DATE.format(dateTime);
    }

    public TimeFormatUtil() {
    }

    public static String getCurrentTimeStr() {
        return dateToString(new Date());
    }

    public static String dateToString(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyy-MM-dd HH:mm:ss");
        return sdf.format(date);
    }

    public static String dateToString1(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        return sdf.format(date);
    }

    public static String dateToString2(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return sdf.format(date);
    }



    public static Date stringToDate(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyy-MM-dd");
        Date parse = null;
        try {
            parse = sdf.parse(date);
        } catch (ParseException var4) {
            logger.error("Date conversion failed!", var4);
        }
        return parse;
    }

    public static Date stringToDate2(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date parse = null;
        try {
            parse = sdf.parse(date);
        } catch (ParseException var4) {
            logger.error("Date conversion failed!", var4);
        }
        return parse;
    }

    public static Date getStartTime() {
        Calendar todayStart = Calendar.getInstance();
        todayStart.set(Calendar.HOUR_OF_DAY, 0);
        todayStart.set(Calendar.MINUTE, 0);
        todayStart.set(Calendar.SECOND, 0);
        todayStart.set(Calendar.MILLISECOND, 0);
        return todayStart.getTime();
    }

    public static Date getnowEndTime() {
        Calendar todayEnd = Calendar.getInstance();
        todayEnd.set(Calendar.HOUR_OF_DAY, 23);
        todayEnd.set(Calendar.MINUTE, 59);
        todayEnd.set(Calendar.SECOND, 59);
        todayEnd.set(Calendar.MILLISECOND, 999);
        return todayEnd.getTime();
    }

    public static String timesPlus8(String oldTime) {
        Date newDate = timesPlus8ToDate(oldTime);
        return dateToString2(newDate);
    }

    public static Date timesPlus8ToDate(String oldTime) {
        Date newDate =null;
        try {
            String createDate = oldTime.replace("T", " ").replace("Z", "");
            Date oldDate = stringToDate2(createDate);
            if(oldDate!=null){
                newDate = new Date(oldDate.getTime() + TRANS_TIME_ZONE);
            }
        }catch (Exception e){
            logger.error("Date conversion failed!", e);
        }
        return newDate;
    }

    public static Set<Date> findDates(Date dBegin, Date dEnd) {
        Set lDate = new HashSet();
        lDate.add(dateToString(dBegin));
        Calendar calBegin = Calendar.getInstance();
        // 使用给定的 Date 设置此 Calendar 的时间
        calBegin.setTime(dBegin);
        Calendar calEnd = Calendar.getInstance();
        // 使用给定的 Date 设置此 Calendar 的时间
        calEnd.setTime(dEnd);
        // 测试此日期是否在指定日期之后
        while (dEnd.after(calBegin.getTime()))
        {
            // 根据日历的规则，为给定的日历字段添加或减去指定的时间量
            calBegin.add(Calendar.DAY_OF_MONTH, 1);
            lDate.add(dateToString(calBegin.getTime()));
        }
        logger.info("一周内的日期集合:{}",lDate);
        return lDate;
    }

    /**
     * 获取当前日期 负偏移 day的时间
     * @param day
     * @return
     */
    public static Long getDateByNowNegativeOffsetDay(Integer day){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE,-(day-1));
        Date monday = calendar.getTime();
        String s = dateToString(monday);
        logger.info(s);
        return null;
    }

    public static void main(String[] args) {
        //getDateByNowNegativeOffsetDay(7);
        logger.info(getDateStr(1604023859198L));
    }

    /**
     * 根据时间,获取天数差
     * @param startDate
     * @param endDate
     * @return
     */
    public static int getDaysByDate(Long startDate, Long endDate){
        return Days.daysBetween(
                new LocalDate(startDate),
                new LocalDate(endDate)).getDays();
    }

    /**
     * 根据时间获取月份差
     * @param startDate
     * @param endDate
     * @return
     */
    public static int getMonthsByDate(Long startDate, Long endDate){
        return Months.monthsBetween(
                new LocalDate(startDate),
                new LocalDate(endDate)).getMonths();
    }

    /**
     * 获取上周日期区间
     * @return
     */
    public static Map<String,Long> getLastWeekByDate(){

        Map<String,Long> map = Maps.newHashMap();
        Calendar calendar1 = Calendar.getInstance();
        Calendar calendar2 = Calendar.getInstance();
        int dayOfWeek = calendar1.get(Calendar.DAY_OF_WEEK) - 1;
        int offset1 = 1 - dayOfWeek;
        int offset2 = 7 - dayOfWeek;
        calendar1.add(Calendar.DATE, offset1 - 7);
        calendar2.add(Calendar.DATE, offset2 - 7);

        /*int offset1 = -(dayOfWeek+7);
        int offset2 = - dayOfWeek;
        calendar1.add(Calendar.DATE, offset1);
        calendar2.add(Calendar.DATE, offset2);*/

        map.put("lastBeginDate",calendar1.getTime().getTime());
        map.put("lastEndDate",calendar2.getTime().getTime());

        return map;
    }

    /**
     * 获取这周开始时间
     * @return
     */
    public static Long getWeekStartDate(){

        Calendar calendar = Calendar.getInstance();
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK) - 1;

        calendar.add(Calendar.DATE,-(dayOfWeek-1));

        return calendar.getTime().getTime();
    }

    /**
     * 根据传入的时间，和天数 获取偏移时间
     * @param time
     * @param day
     * @return
     */
    public static long getDateByTimeNegativeOffsetDay(Long time, Integer day){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(time);
        calendar.add(Calendar.DATE,-(day));
        Date date = calendar.getTime();
        return date.getTime();
    }
}
