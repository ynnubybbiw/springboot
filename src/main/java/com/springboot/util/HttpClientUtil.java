package com.springboot.util;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.*;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author dubin
 */
public class HttpClientUtil {

    private static final Logger logger = LoggerFactory.getLogger(HttpClientUtil.class);

    public static final String HTTPS = "https";
    public static final String HTTP = "http";
    public static final String CONTENT_TYPE_NAME = "Content-type";

    private static RequestConfig getRequestConfig(){
        // 设置配置请求参数
        // 连接主机服务超时时间
        return RequestConfig.custom().setConnectTimeout(35000)
                // 请求超时时间
                .setConnectionRequestTimeout(35000)
                // 数据读取超时时间
                .setSocketTimeout(60000)
                .build();
    }

    /**
     * 发送  post 请求
     * @param url
     * @param json
     * @param headers
     * @param ssl
     * @return
     */
    public static String sendPostJson(String url,String json, Map<String,String> headers,String ssl) {

        try(CloseableHttpClient httpClient = HTTPS.equals(ssl)?HttpsUtils.createSSLClientDefault(): HttpClients.createDefault()){

            // 创建httppost对象
            HttpPost httpPost = new HttpPost(url);

            // 为httpPost实例设置配置
            httpPost.setConfig(getRequestConfig());
            // 设置请求头
            if (headers != null) {
                for (Map.Entry<String, String> entry : headers.entrySet()) {
                    httpPost.addHeader(entry.getKey(),entry.getValue());
                }
            }
            // 给httppost对象设置json格式的参数
            StringEntity httpEntity = new StringEntity(json,StandardCharsets.UTF_8);
            // 设置请求格式
            httpPost.setHeader(CONTENT_TYPE_NAME, MediaType.APPLICATION_JSON_VALUE);
            // 传参
            httpPost.setEntity(httpEntity);

            CloseableHttpResponse httpResponse = httpClient.execute(httpPost);
            // 将response转成String并存储在result中
            org.apache.http.HttpEntity entity = httpResponse.getEntity();
            return EntityUtils.toString(entity);
        }catch (Exception e){
            logger.error("sendPostJson请求失败：",e);
        }
        return "error";
    }

    /**
     * 发送post  form 请求
     * @param url
     * @param paramMap
     * @param headers
     * @param ssl
     * @return
     */
    public static String sendPostform(String url, Map<String, Object> paramMap, Map<String,String> headers,String ssl) {

        try(CloseableHttpClient httpClient = HTTPS.equals(ssl)?HttpsUtils.createSSLClientDefault(): HttpClients.createDefault()){
            // 创建httpPost远程连接实例
            HttpPost httpPost = new HttpPost(url);
            // 为httpPost实例设置配置
            httpPost.setConfig(getRequestConfig());
            httpPost.addHeader(CONTENT_TYPE_NAME, MediaType.APPLICATION_FORM_URLENCODED_VALUE);

            // 设置请求头
            if (headers != null) {
                for (Map.Entry<String, String> entry : headers.entrySet()) {
                    httpPost.addHeader(entry.getKey(),entry.getValue());
                }
            }

            // 封装post请求参数
            if (null != paramMap && paramMap.size() > 0) {
                List<NameValuePair> nvps = new ArrayList<>();
                // 通过map集成entrySet方法获取entity
                Set<Map.Entry<String, Object>> entrySet = paramMap.entrySet();
                // 循环遍历，获取迭代器
                for (Map.Entry<String, Object> mapEntry : entrySet) {
                    nvps.add(new BasicNameValuePair(mapEntry.getKey(), mapEntry.getValue().toString()));
                }

                // 为httpPost设置封装好的请求参数
                httpPost.setEntity(new UrlEncodedFormEntity(nvps, StandardCharsets.UTF_8));
            }

            // httpClient对象执行post请求,并返回响应参数对象
            CloseableHttpResponse httpResponse = httpClient.execute(httpPost);
            // 从响应对象中获取响应内容
            org.apache.http.HttpEntity entity = httpResponse.getEntity();
            return EntityUtils.toString(entity);
        }catch (UnsupportedEncodingException e){
            logger.error("sendPostform设置请求参数失败：",e);
        }catch (Exception e){
            logger.error("sendPostform请求失败：",e);
        }
        return "error";
    }

    /**
     * 发送  get 请求
     * @param url
     * @param headers
     * @param ssl
     * @return
     */
    public static String sendGet(String url, Map<String,String> headers, String ssl) {

        try(CloseableHttpClient httpClient = HTTPS.equals(ssl)?HttpsUtils.createSSLClientDefault(): HttpClients.createDefault()){
            // 创建httpGet远程连接实例
            HttpGet httpGet = new HttpGet(url);
            if (headers != null) {
                for (Map.Entry<String, String> entry : headers.entrySet()) {
                    httpGet.addHeader(entry.getKey(),entry.getValue());
                }
            }

            // 为httpGet实例设置配置
            httpGet.setConfig(getRequestConfig());
            // 执行get请求得到返回对象
            CloseableHttpResponse response = httpClient.execute(httpGet);
            // 通过返回对象获取返回数据
            org.apache.http.HttpEntity entity = response.getEntity();
            // 通过EntityUtils中的toString方法将结果转换为字符串
            return EntityUtils.toString(entity);
        }catch (Exception e){
            logger.error("sendGet请求失败：",e);
        }
        return "error";
    }

    public static void main(String[] args) {
        String s = sendGet("https://10.0.39.253:31666/api_server/cv-console-iam/oauth/token?grant_type=client_credentials&client_secret=2Ym7iBGgRX3XmpJ4BeRCG.kiFbU.XwX5AQ6lpyV2N.e6CmUjdWFNS&client_id=hpc", null, HTTPS);
        logger.info("sss {}",s);
    }

    /**
     * 发送 delete请求
     * @param url
     * @param headers
     * @param ssl
     * @return
     */
    public static String httpDelete(String url,Map<String,String> headers, String ssl){

        try(CloseableHttpClient httpClient = HTTPS.equals(ssl)?HttpsUtils.createSSLClientDefault(): HttpClients.createDefault()){
            HttpDelete httpdelete = new HttpDelete(url);
            //设置header
            if (headers != null && headers.size() > 0) {
                for (Map.Entry<String, String> entry : headers.entrySet()) {
                    httpdelete.setHeader(entry.getKey(),entry.getValue());
                }
            }

            // 为httpGet实例设置配置
            httpdelete.setConfig(getRequestConfig());
            CloseableHttpResponse httpResponse = httpClient.execute(httpdelete);
            HttpEntity entity = httpResponse.getEntity();
            return EntityUtils.toString(entity, StandardCharsets.UTF_8);

        }catch (Exception e){
            logger.error("httpDelete请求失败：",e);
        }
        return "error";
    }

    /**
     * 发送删除  带body参数
     * @param url
     * @param json
     * @param headers
     * @param ssl
     * @return
     */
    public static String httpDelete(String url,String json,Map<String,String> headers, String ssl){

        try (CloseableHttpClient httpClient = HTTPS.equals(ssl)?HttpsUtils.createSSLClientDefault(): HttpClients.createDefault()){

            HttpDeleteWithBody httpdelete = new HttpDeleteWithBody(url);
            //设置header
            if (headers != null && headers.size() > 0) {
                for (Map.Entry<String, String> entry : headers.entrySet()) {
                    httpdelete.setHeader(entry.getKey(),entry.getValue());
                }
            }
            if(StringUtils.isNotEmpty(json)){
                // 给httppost对象设置json格式的参数
                StringEntity httpEntity = new StringEntity(json,StandardCharsets.UTF_8);
                // 传参
                httpdelete.setEntity(httpEntity);
                // 设置请求格式
                httpdelete.setHeader(CONTENT_TYPE_NAME,MediaType.APPLICATION_JSON_VALUE);
            }

            // 为httpGet实例设置配置
            httpdelete.setConfig(getRequestConfig());
            CloseableHttpResponse httpResponse = httpClient.execute(httpdelete);
            org.apache.http.HttpEntity entity = httpResponse.getEntity();
            return EntityUtils.toString(entity, StandardCharsets.UTF_8);
        }catch (Exception e){
            logger.error("httpDelete请求失败：",e);
        }
        return "error";
    }

    /**
     * 发送 put请求
     * @param url
     * @param json
     * @param headers
     * @param ssl
     * @return
     */
    public static String sendPutJson(String url,String json, Map<String,String> headers, String ssl) {

        try (CloseableHttpClient httpClient = HTTPS.equals(ssl)?HttpsUtils.createSSLClientDefault(): HttpClients.createDefault()){

            // 创建httppost对象
            HttpPut httpPut = new HttpPut(url);
            // 为httpPost实例设置配置
            httpPut.setConfig(getRequestConfig());
            // 设置请求头
            if (headers != null) {
                for (Map.Entry<String, String> entry : headers.entrySet()) {
                    httpPut.addHeader(entry.getKey(),entry.getValue());
                }
            }
            if(StringUtils.isNotEmpty(json)){
                // 给httppost对象设置json格式的参数
                StringEntity httpEntity = new StringEntity(json,StandardCharsets.UTF_8);
                // 传参
                httpPut.setEntity(httpEntity);
            }

            // 设置请求格式
            httpPut.setHeader(CONTENT_TYPE_NAME,MediaType.APPLICATION_JSON_VALUE);

            CloseableHttpResponse httpResponse = httpClient.execute(httpPut);
            // 将response转成String并存储在result中
            HttpEntity entity = httpResponse.getEntity();
            return EntityUtils.toString(entity);

        }catch (Exception e){
            logger.error("sendPutJson请求失败：",e);
        }

        return "error";
    }
}
