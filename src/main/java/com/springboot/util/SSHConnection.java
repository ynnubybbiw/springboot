package com.springboot.util;

import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.common.IOUtils;
import net.schmizz.sshj.connection.channel.direct.Session;
import net.schmizz.sshj.sftp.SFTPClient;
import net.schmizz.sshj.transport.verification.PromiscuousVerifier;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SSHConnection {

    private static final Logger logger = LoggerFactory.getLogger(SSHConnection.class);

    public static final String EOF_TEXT = "cat>/opt/tmp/%s<<EOF\n%s\nEOF";

    public static SSHClient connect(){

        SSHClient sshClient = new SSHClient();
        long start = System.currentTimeMillis();
        try {
            logger.info("-----------------  {}  {} ", sshClient.isAuthenticated(), sshClient.isConnected());
            sshClient.addHostKeyVerifier(new PromiscuousVerifier());
            sshClient.setConnectTimeout(10000);
            sshClient.setTimeout(10000);
            sshClient.connect("65.49.201.254",27700);
            sshClient.authPassword("root","ynnubybbiw");

            logger.info("-----------------  {}  {}; {}", sshClient.isAuthenticated(), sshClient.isConnected(),System.currentTimeMillis()-start);
        } catch (Exception e) {
            logger.error("connect error",e);
            logger.info("时间：{}",System.currentTimeMillis()-start);
        }
        return sshClient;
    }


    public static String exce(){
        String s = "";
        String code = "#!/usr/bin/python\n" +
                "\n" +
                "import os\n" +
                "import sys\n" +
                "try:\n" +
                "    directory = sys.argv[1]\n" +
                "except IndexError:\n" +
                "    sys.exit(\"Must provide an argument.\")\n" +
                "\n" +
                "dir_size = 0\n" +
                "fsizedicr = {'Bytes': 1,\n" +
                "             'Kilobytes': float(1) / 1024,\n" +
                "             'Megabytes': float(1) / (1024 * 1024),\n" +
                "             'Gigabytes': float(1) / (1024 * 1024 * 1024)}\n" +
                "for (path, dirs, files) in os.walk(directory):\n" +
                "    for file in files:\n" +
                "        filename = os.path.join(path, file)\n" +
                "        dir_size += os.path.getsize(filename)\n" +
                "\n" +
                "fsizeList = [str(round(fsizedicr[key] * dir_size, 2)) + \" \" + key for key in fsizedicr]\n" +
                "\n" +
                "if dir_size == 0: print (\"File Empty\")\n" +
                "else:\n" +
                "  for units in sorted(fsizeList)[::-1]:\n" +
                "      print (\"Folder Size: \" + units)";
        try(SSHClient connect = connect();
            Session session = connect.startSession()){
            Session.Command exec = session.exec(String.format(EOF_TEXT,"count_size.py",code));
            //Session.Command exec = session.exec("ls -l");
            s = IOUtils.readFully(exec.getInputStream()).toString();
        }catch (Exception e){
            logger.error("exceLL error ", e);
        }
        return s;
    }

    public static String exceCodePython(){
        String s = "";
        String code = "#!/usr/bin/python\n" +
                "\n" +
                "import os\n" +
                "import sys\n" +
                "try:\n" +
                "    directory = sys.argv[1]\n" +
                "except IndexError:\n" +
                "    sys.exit(\"Must provide an argument.\")\n" +
                "\n" +
                "dir_size = 0\n" +
                "fsizedicr = {'Bytes': 1,\n" +
                "             'Kilobytes': float(1) / 1024,\n" +
                "             'Megabytes': float(1) / (1024 * 1024),\n" +
                "             'Gigabytes': float(1) / (1024 * 1024 * 1024)}\n" +
                "for (path, dirs, files) in os.walk(directory):\n" +
                "    for file in files:\n" +
                "        filename = os.path.join(path, file)\n" +
                "        dir_size += os.path.getsize(filename)\n" +
                "\n" +
                "fsizeList = [str(round(fsizedicr[key] * dir_size, 2)) + \" \" + key for key in fsizedicr]\n" +
                "\n" +
                "if dir_size == 0: print (\"File Empty\")\n" +
                "else:\n" +
                "  for units in sorted(fsizeList)[::-1]:\n" +
                "      print (\"Folder Size: \" + units)";
        try(SSHClient connect = connect();
            Session session = connect.startSession()){
            Session.Command exec = session.exec(String.format(EOF_TEXT,"count_size.py",code));
            s = IOUtils.readFully(exec.getInputStream()).toString();
        }catch (Exception e){
            logger.error("exceCode error ", e);
        }
        return s;
    }

    public static String exceCodeEofPython(){
        String s = "";
        try(SSHClient connect = connect();
            Session session = connect.startSession()){
            Session.Command exec = session.exec("python /opt/tmp/count_size.py /opt/tmp");
            s = IOUtils.readFully(exec.getInputStream()).toString();
        }catch (Exception e){
            logger.error("exceCodeEof error ", e);
        }
        return s;
    }

    public static String exceCodeShell(){
        String s = "";
        String code = "#!/bin/bash  \n" +
                "IP_LIST=\"10.0.38.197\"\n" +
                "for IP in $IP_LIST; do\n" +
                "    NUM=1\n" +
                "    while [ $NUM -le 3 ]; do\n" +
                "        if ping -c 1 $IP > /dev/null; then\n" +
                "            echo \"$IP Ping is successful.\"\n" +
                "            break\n" +
                "        else\n" +
                "            # echo \"$IP Ping is failure $NUM\"\n" +
                "            FAIL_COUNT[$NUM]=$IP\n" +
                "            let NUM++\n" +
                "        fi\n" +
                "    done\n" +
                "    if [ ${#FAIL_COUNT[*]} -eq 3 ];then\n" +
                "        echo \"${FAIL_COUNT[1]} Ping is failure!\"\n" +
                "        unset FAIL_COUNT[*]\n" +
                "    fi\n" +
                "done";
        try(SSHClient connect = connect();
            Session session = connect.startSession()){
            Session.Command exec = session.exec(String.format(EOF_TEXT,"check_host.sh",code.replace("$","\\$")));
            s = IOUtils.readFully(exec.getInputStream()).toString();
        }catch (Exception e){
            logger.error("exceCodeShell error ", e);
        }
        return s;
    }

    public static String exceCodeEofShell(){
        String s = "";
        try(SSHClient connect = connect();
            Session session = connect.startSession()){
            Session.Command exec = session.exec("sh /opt/tmp/check_host.sh");
            s = IOUtils.readFully(exec.getInputStream()).toString();
        }catch (Exception e){
            logger.error("exceCodeEofShell error ", e);
        }
        return s;
    }


    public static void uploadFile(){

        String local = "C:\\Users\\29678\\Desktop\\sugon-emos-sys-2.0.3.RELEASE.jar";
        String remotePath = "/opt/tmp/sugon-emos-sys-2.0.3.RELEASE123.jar";
        try(SSHClient connect = connect();
            SFTPClient sftpClient = connect.newSFTPClient()
        ){
            long start = System.currentTimeMillis();
            logger.info("开始上传文件 {}",start);
            sftpClient.put(local, remotePath);
            logger.info("上传文件完成 {}", System.currentTimeMillis()-start);

        }catch (Exception e){

        }
    }

    public static boolean checkFileExist(){
        boolean sgin = Boolean.FALSE;
        try(SSHClient connect = connect();
            Session session = connect.startSession()){
            Session.Command exec = session.exec(String.format("stat %s", "/opt/tmp"));
            String s = IOUtils.readFully(exec.getInputStream()).toString();
            logger.info("文件检测完成 ：{}",s);
            if(s.contains("File:")){
                sgin = Boolean.TRUE;
            }
        }catch (Exception e){
            logger.error("checkFileExist error ", e);
        }
        return sgin;
    }

}
