package com.springboot.util;


import java.text.SimpleDateFormat;

/**
 * 时间格式化线程安全工具类
 *
 * 获取方法  SimpleDateFormat simpleDateFormat = ThreadSafeDateFormatter.dateFormatThreadLocal.get();
 * @author 29678
 */
public class ThreadSafeDateFormatter {

    public static ThreadLocal<SimpleDateFormat> simpleDateFormatThreadLocal = ThreadLocal.withInitial(() -> new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));


}
