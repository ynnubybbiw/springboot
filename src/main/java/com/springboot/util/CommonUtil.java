package com.springboot.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;


/**
 * 后台接口返回值封装类
 *
 * @author LUOYUAN
 * @date 2019-07-19-9:53
 */

public class CommonUtil {


    public static String successJsonStr() {
        return successJsonStr(new JSONObject());
    }

    public static String successJsonStr(Object returnData) {
        JSONObject resultJson = new JSONObject();
        resultJson.put(ReturnData.RETURN_CODE, ReturnData.SUCCESS_CODE);
        resultJson.put(ReturnData.RETURN_MSG, ReturnData.SUCCESS_MSG);
        resultJson.put(ReturnData.RETURN_DATA, returnData);
        return JSON.toJSONString(resultJson, SerializerFeature.WriteDateUseDateFormat,SerializerFeature.WriteMapNullValue);
    }
    public static String successJsonString(Object returnData) {
        JSONObject resultJson = new JSONObject();
        resultJson.put(ReturnData.RETURN_CODE, ReturnData.SUCCESS_CODE);
        resultJson.put(ReturnData.RETURN_MSG, ReturnData.SUCCESS_MSG);
        resultJson.put(ReturnData.RETURN_DATA, returnData);
        return JSON.toJSONString(resultJson);
    }

    public static JSONObject successJson(Object returnData) {
        JSONObject resultJson = new JSONObject();
        resultJson.put(ReturnData.RETURN_CODE, ReturnData.SUCCESS_CODE);
        resultJson.put(ReturnData.RETURN_MSG, ReturnData.SUCCESS_MSG);
        //  resultJson.put("returnData",JSON.parse(JSON.toJSONString(returnData, SerializerFeature.WriteDateUseDateFormat)));
        resultJson.put(ReturnData.RETURN_DATA, returnData);
        return resultJson;
    }

    /*public static String errorJsonStr(ErrorEnum errorEnum) {
        JSONObject resultJson = new JSONObject();
        resultJson.put(ReturnData.RETURN_CODE, errorEnum.getErrorCode());
        resultJson.put(ReturnData.RETURN_MSG, errorEnum.getErrorMsg());
        resultJson.put(ReturnData.RETURN_DATA, new JSONObject());
        return resultJson + "";
    }*/

    public static JSONObject errorJsonStr(String errorMsg) {
        JSONObject resultJson = new JSONObject();
        resultJson.put(ReturnData.RETURN_CODE, ReturnData.ERROR_CODE);
        resultJson.put(ReturnData.RETURN_MSG, errorMsg);
        resultJson.put(ReturnData.RETURN_DATA, new JSONObject());
        return resultJson;
    }

    public static String errorJsonString(String errorMsg) {
        JSONObject resultJson = new JSONObject();
        resultJson.put(ReturnData.RETURN_CODE, ReturnData.ERROR_CODE);
        resultJson.put(ReturnData.RETURN_MSG, errorMsg);
        resultJson.put(ReturnData.RETURN_DATA, new JSONObject());
        return JSON.toJSONString(resultJson);
    }

    /*public static String errorJsonStr(ErrorEnum errorEnum, Object... args) {
        JSONObject resultJson = new JSONObject();
        resultJson.put(ReturnData.RETURN_CODE, errorEnum.getErrorCode());
        resultJson.put(ReturnData.RETURN_MSG, String.format(errorEnum.getErrorMsg(), args));
        resultJson.put(ReturnData.RETURN_DATA, new JSONObject());
        return resultJson + "";
    }*/

    public static String errorJsonStr(Integer returnCode, String returnMsg) {
        JSONObject resultJson = new JSONObject();
        resultJson.put(ReturnData.RETURN_CODE, returnCode);
        resultJson.put(ReturnData.RETURN_MSG, returnMsg);
        resultJson.put(ReturnData.RETURN_DATA, new JSONObject());
        return resultJson + "";
    }

    //验证是否含有全部必填字段
    /*public static void hasAllRequired(final JSONObject jsonObject, String requiredColumns) {
        if (StringUtils.isNotBlank(requiredColumns)) {
            // 验证字段非空
            String[] columns = requiredColumns.split(",");
            String missCol = "";
            for (String column : columns) {
                Object val = jsonObject.get(column.trim());
                if (StringUtils.isBlank(val.toString())) {
                    missCol += column + "  ";
                }
            }
            if (StringUtils.isNotBlank(missCol)) {
                jsonObject.clear();
                jsonObject.put(ReturnData.RETURN_CODE, "90003");
                jsonObject.put(ReturnData.RETURN_MSG, "缺少必填参数:" + missCol.trim());
                jsonObject.put(ReturnData.RETURN_DATA, new JSONObject());
                throw new CommonJsonException(jsonObject);
            }
        }
    }*/

}
