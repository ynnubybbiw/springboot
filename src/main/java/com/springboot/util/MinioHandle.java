package com.springboot.util;

import com.google.common.base.Splitter;
import com.google.common.collect.Multimap;
import com.springboot.bean.MinioParam;
import com.springboot.config.CustomizeMinioClient;
import com.springboot.config.MinioProperties;
import io.minio.*;
import io.minio.http.Method;
import io.minio.messages.Part;
import org.apache.commons.compress.utils.Lists;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Component
public class MinioHandle {

    Logger logger= LoggerFactory.getLogger(MinioHandle.class);

    @Autowired
    public MinioProperties minioProperties;

    @Autowired
    private CustomizeMinioClient minioClient;

    /**
     * 获取文件下载链接
     * @param objectName   文件名
     * @return
     */
    public String getDownloadUrl(String objectName) {

        try {
            String downloadUrl = minioClient.getPresignedObjectUrl(GetPresignedObjectUrlArgs.builder()
                    .method(Method.GET)
                    .bucket(minioProperties.getBucket())
                    .object(objectName)
                    .expiry(7, TimeUnit.DAYS)
                    .build());
            return getUrlNoParam(downloadUrl);
        } catch (Exception e) {
            logger.error("获取文件下载地址失败：",e);
            return "";
        }
    }

    /**
     * 删除文件
     * @param objectName 文件名
     */
    public void deleteFile(String objectName) {
        logger.info("开始删除minio文件：{}",objectName);
        try {
            minioClient.removeObject(RemoveObjectArgs.builder()
                    .bucket(minioProperties.getBucket())
                    .object(objectName)
                    .build());
        } catch (Exception e) {
            logger.error("删除文件失败：",e);
        }
    }

    /**
     * 查询 分片信息
     * @param minioParam
     * @return
     */
    public List<Part> getPartList(MinioParam minioParam){
        try {
            ListPartsResponse listPartsResponse = minioClient.listParts(
                    minioParam.getBucketName(),
                    minioParam.getRegion(),
                    minioParam.getObjectName(),
                    minioParam.getMaxParts(),
                    minioParam.getPartNumberMarker(),
                    minioParam.getUploadId(),
                    minioParam.getHeaders(),
                    minioParam.getExtraQueryParams());
            return listPartsResponse.result().partList();
        } catch (Exception e) {
            logger.error("获取分片信息失败：",e);
        }
        return new LinkedList<>();
    }

    /**
     * 获取分片，如果大于1000，循环获取所有
     * @param objectName
     * @param uploadId
     * @param partNum
     * @return
     */
    public List<Part> getPartListByPagination(String objectName, String uploadId, Integer partNum){
        logger.info("开始获取分片，分片数是：{}; objectName是：{}；uploadId是：{}",partNum,objectName,uploadId);
        int maxParts = partNum;
        int partNumberMarker = 0;

        //初始每页取1000  minio限制每次最多能取出1000
        int initPageSize = 1000;
        List<Part> allList = Lists.newArrayList();

        //如果大于1000，循环取所有。如果小于1000，则取一次
        if(partNum>initPageSize){
            logger.info("分片数大于每页初始化数{}",initPageSize);
            maxParts = initPageSize;
            while (true){
                logger.info("maxParts的值是：{}; partNumberMarker的值是：{}；",maxParts,partNumberMarker);
                List<Part> partList = getPartList(MinioParam.builder()
                        .bucketName(minioProperties.getBucket())
                        .objectName(objectName)
                        .uploadId(uploadId)
                        .maxParts(maxParts)
                        .partNumberMarker(partNumberMarker)
                        .build());
                //将每一次的结果放入
                allList.addAll(partList);
                int partSize = partList.size();
                logger.info("取到的分片数量 ：{}",partSize);
                //如果分片取出来的数量不够1000,说明已经取完
                if(partSize < initPageSize){
                    break;
                }

                partNumberMarker = partNumberMarker+maxParts;

            }
        }else {
            allList = getPartList(MinioParam.builder()
                    .bucketName(minioProperties.getBucket())
                    .objectName(objectName)
                    .uploadId(uploadId)
                    .maxParts(maxParts)
                    .partNumberMarker(0)
                    .build());
        }
        return allList;
    }

    /**
     * 初始化上传
     * @param bucket
     * @param region
     * @param objectName
     * @param headers
     * @param extraQueryParams
     * @return
     */
    public String initMultiPartUpload(String bucket, String region, String objectName, Multimap<String, String> headers, Multimap<String, String> extraQueryParams){
        try {
            CreateMultipartUploadResponse multipartUpload = minioClient.createMultipartUpload(bucket, region, objectName, headers, extraQueryParams);
            return multipartUpload.result().uploadId();
        } catch (Exception e) {
            logger.error("初始化上传失败：",e);
        }
        return null;
    }

    /**
     * 获取分片url上传地址
     * @param build
     * @return
     */
    public String getPresignedObjectUrl(GetPresignedObjectUrlArgs build) {
        try {
            return minioClient.getPresignedObjectUrl(build);
        } catch (Exception e) {
            logger.error("获取上传url失败：",e);
        }
        return null;
    }

    public Boolean mergeParts(MinioParam minioParam){
        Boolean flat = Boolean.FALSE;
        try {
            List<Part> partList = getPartListByPagination(minioParam.getObjectName(), minioParam.getUploadId(),minioParam.getPartNum());
            minioParam.setParts(partList.toArray(new Part[]{}));
            ObjectWriteResponse objectWriteResponse = minioClient.completeMultipartUpload(minioParam);
            flat = Boolean.TRUE;
        } catch (Exception e) {
            logger.error("合并文件块失败：",e);
        }
        return flat;
    }

    public Boolean checkFile(String minioFileName) {
        Boolean check = Boolean.TRUE;
        try {
            StatObjectResponse statObjectResponse = minioClient.statObject(StatObjectArgs.builder().bucket(minioProperties.getBucket()).object(minioFileName).build());
            logger.info("检测文件完成");
        } catch (Exception e) {
            check = Boolean.FALSE;
            logger.error("检测文件是否存在错误");
        }
        return check;
    }


    /**
     * 从url中获取问号后的参数
     * @param url
     * @param name
     * @return
     */
    public static String getUrlParam(String url, String name) {
        if(url.contains(name)){
            String params = url.substring(url.indexOf("?") + 1);
            Map<String, String> split = Splitter.on("&").withKeyValueSeparator("=").split(params);
            return split.get(name);
        }
        return "";
    }

    /**
     * 去掉上传地址的ip和端口
     * @param url
     * @return
     */
    public String getUrlNoIpAndPort(String url){
        if(StringUtils.isNotEmpty(url) && url.contains(minioProperties.getEndpoint())){
            return url.replace(minioProperties.getEndpoint(),"");
        }
        return url;
    }

    /**
     * 去掉url参数
     * @param url
     * @return
     */
    public static String getUrlNoParam(String url){
        if(StringUtils.isNotEmpty(url) && url.contains("?")){
            return url.substring(0,url.indexOf("?"));
        }
        return url;
    }
}
