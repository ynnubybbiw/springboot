package com.springboot.util;

import com.springboot.exception.CustomizException;
import org.apache.commons.lang3.StringUtils;

import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class CIDRUtils {

    private final String cidr;
    private InetAddress inetAddress;
    private InetAddress startAddress;
    private InetAddress endAddress;
    private Integer prefixLength;

    public CIDRUtils(String cidr) throws UnknownHostException {
        this.cidr = cidr;
        if (this.cidr.contains("/")) {
            int index = this.cidr.indexOf("/");
            String addressPart = this.cidr.substring(0, index);
            String networkPart = this.cidr.substring(index + 1);
            this.inetAddress = InetAddress.getByName(addressPart);
            this.prefixLength = Integer.valueOf(networkPart);
            this.calculate();
        }
    }

    private void calculate() throws UnknownHostException {
        ByteBuffer maskBuffer;
        byte targetSize;
        if (this.inetAddress.getAddress().length == 4) {
            maskBuffer = ByteBuffer.allocate(4).putInt(-1);
            targetSize = 4;
        } else {
            maskBuffer = ByteBuffer.allocate(16).putLong(-1L).putLong(-1L);
            targetSize = 16;
        }

        BigInteger mask = (new BigInteger(1, maskBuffer.array())).not().shiftRight(this.prefixLength);
        ByteBuffer buffer = ByteBuffer.wrap(this.inetAddress.getAddress());
        BigInteger ipVal = new BigInteger(1, buffer.array());
        BigInteger startIp = ipVal.and(mask);
        BigInteger endIp = startIp.add(mask.not());
        byte[] startIpArr = this.toBytes(startIp.toByteArray(), targetSize);
        byte[] endIpArr = this.toBytes(endIp.toByteArray(), targetSize);
        this.startAddress = InetAddress.getByAddress(startIpArr);
        this.endAddress = InetAddress.getByAddress(endIpArr);
    }

    private byte[] toBytes(byte[] array, int targetSize) {
        int counter = 0;

        ArrayList newArr;
        for(newArr = new ArrayList(); counter < targetSize && array.length - 1 - counter >= 0; ++counter) {
            newArr.add(0, array[array.length - 1 - counter]);
        }

        int size = newArr.size();

        for(int i = 0; i < targetSize - size; ++i) {
            newArr.add(0, (byte)0);
        }

        byte[] ret = new byte[newArr.size()];

        for(int i = 0; i < newArr.size(); ++i) {
            ret[i] = (Byte)newArr.get(i);
        }

        return ret;
    }


    public boolean isInRange(String ipAddress) throws UnknownHostException {
        InetAddress address = InetAddress.getByName(ipAddress);
        BigInteger start = new BigInteger(1, this.startAddress.getAddress());
        BigInteger end = new BigInteger(1, this.endAddress.getAddress());
        BigInteger target = new BigInteger(1, address.getAddress());
        int st = start.compareTo(target);
        int te = target.compareTo(end);
        return (st < 0 || st == 0) && (te < 0 || te == 0);
    }

    /**
     * 不包含边界   比如192.168.30.0/24   不包含30.0    30.255
     * @param ipAddress
     * @return
     * @throws UnknownHostException
     */
    public boolean isInRangeNoContainBorder(String ipAddress) throws UnknownHostException {
        InetAddress address = InetAddress.getByName(ipAddress);
        BigInteger start = new BigInteger(1, this.startAddress.getAddress());
        BigInteger end = new BigInteger(1, this.endAddress.getAddress());
        BigInteger target = new BigInteger(1, address.getAddress());
        int st = start.compareTo(target);
        int te = target.compareTo(end);
        return st < 0 && te < 0;
    }

    /**
     * 定义公有静态方法convertIP实现转换，点分十进制IP地址为参数，转换后的二进制IP为返回值，在方法内检查参数IP地址的合法性
     * @param ip
     * @return
     */
    public static String convertIP(String ip){
        String[] ipstr = ip.split("\\.");
        String returnIp = null;
        if (ipstr.length!=4){
            throw new CustomizException("ip输入错误："+ip);
        }

        int[] array = Arrays.stream(ipstr).mapToInt(Integer::parseInt).toArray();
        String[] ipa = new String[4];
        for(int i=0;i<array.length;i++)
        {
            if (array[i]<0 || array[i]>255){
                throw new CustomizException("各段数值不是0~255范围!"); // 出现字段不在1-255之间，抛出异常
            }else{
                String x = Integer.toBinaryString(array[i]);
                String ip8 = "00000000";
                ipa[i] = ip8.substring(0, ip8.length() - x.length()) + x;
                if(!Arrays.asList(ipa).contains(null)){
                    returnIp = StringUtils.join(Arrays.asList(ipa), ".");
                }
            }
        }

        return returnIp;
    }

    /**
     * 计算ip数
     * @param ip
     * @return
     */
    public static long ipToLong(String ip) {
        long result = 0;
        try {
            InetAddress beginIp = InetAddress.getByName(ip);
            byte[] bytes = beginIp.getAddress();
            for (byte b : bytes) {
                result = (result << 8) | (b & 0xFF);
            }
            return result;
        } catch (UnknownHostException e) {
            return result;
        }
    }

    /**
     * 根据数字ip计算出ip
     * @param ip
     * @return
     */
    public static String longToIPAddress(long ip) {
        StringBuilder sb = new StringBuilder(15);
        for (int i = 3; i >= 0; i--) {
            sb.append((ip >> (i * 8)) & 0xFF);
            if (i > 0) {
                sb.append(".");
            }
        }
        return sb.toString();
    }

    public static String randomMacAddress() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < 6; i++) {
            if (sb.length() > 0) {
                sb.append(":");
            }

            Random random = new Random();
            int val = random.nextInt(256);
            String element = Integer.toHexString(val);
            if (element.length() < 2) {
                sb.append(0);
            }

            sb.append(element);
        }

        return sb.toString();
    }

    public static void main(String[] args) {
       /* String s = convertIP("192.168.10.0");
        System.out.printf(s);*/

        for(int i = 0;i < 10;i++){
            String s = randomMacAddress();
            System.out.println(s);
        }

    }
}
