package com.springboot.util;

import cn.hutool.core.lang.Assert;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;

/**
 * @author 29678
 */
public class MockUtil {

    private static final Logger logger = LoggerFactory.getLogger(MockUtil.class);

    public static String readJson(String jsonSrc) {
        String json = "";
        try {
            jsonSrc = String.format("/mock/%s",jsonSrc);
            InputStream stream = MockUtil.class.getResourceAsStream(jsonSrc);
            Assert.notNull(stream);
            json = IOUtils.toString(stream, StandardCharsets.UTF_8);
        } catch (IOException e) {
            logger.error(e.getMessage(),e);
        }
        return json;
    }
}
