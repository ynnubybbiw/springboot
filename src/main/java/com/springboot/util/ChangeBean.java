package com.springboot.util;



import org.apache.commons.lang3.StringUtils;

import java.util.Locale;

public class ChangeBean {

    /**
     * 小驼峰转化成数据库字段
     * @param name
     * @return
     */
    public static String changeToDbName(String name){

        if(StringUtils.isEmpty(name)){
            return "";
        }
        StringBuilder result = new StringBuilder();

        result.append(lowerCaseName(name.substring(0, 1)));

        for(int i = 1; i < name.length(); ++i) {
            String s = name.substring(i, i + 1);
            String slc = lowerCaseName(s);
            if (!s.equals(slc)) {
                result.append("_").append(slc);
            } else {
                result.append(s);
            }
        }
        return result.toString();
    }

    /**
     * 数据库字段 转小驼峰
     * @param name
     * @returnC
     */
    private static String changeToamelName(String name) {
        StringBuilder result = new StringBuilder();
        // 快速检查
        if (StringUtils.isEmpty(name)) {
            // 没必要转换
            return "";
        } else if (!name.contains("_")) {
            // 不含下划线，仅将首字母小写
            return name.substring(0, 1).toLowerCase() + name.substring(1);
        }
        // 用下划线将原始字符串分割
        String[] camels = name.split("_");
        for (String camel :  camels) {
            // 跳过原始字符串中开头、结尾的下换线或双重下划线
            if (camel.isEmpty()) {
                continue;
            }
            // 处理真正的驼峰片段
            if (result.length() == 0) {
                // 第一个驼峰片段，全部字母都小写
                result.append(camel.toLowerCase());
            } else {
                // 其他的驼峰片段，首字母大写
                result.append(camel.substring(0, 1).toUpperCase());
                result.append(camel.substring(1).toLowerCase());
            }
        }
        return result.toString();
    }

    private static String lowerCaseName(String name) {
        return name.toLowerCase(Locale.US);
    }

    public static void main(String[] args) {
        System.out.println(changeToDbName("aaddTyyIkk"));
        System.out.println(changeToamelName("ddd_bbb_eee"));
        System.out.println(changeToamelName("ddd_bbb_eeef"));
    }

}
