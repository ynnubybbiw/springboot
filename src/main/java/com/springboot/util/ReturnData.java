package com.springboot.util;

/**
 *  返回对象获取 常量字段
 */
public class ReturnData {

    private ReturnData() {
    }

    public static final String RETURN_CODE = "returnCode";
    public static final String RETURN_DATA = "returnData";
    public static final String RETURN_MSG = "returnMsg";

    public static final int SUCCESS_CODE = 200;
    public static final int ERROR_CODE = 500;
    public static final int REDIRECT_CODE = 302;
    public static final String SUCCESS_MSG = "请求成功";

    public static final String TEST = "测试";

}
