package com.springboot.util;

import cn.hutool.core.net.Ipv4Util;
import com.alibaba.fastjson.JSON;
import com.springboot.bean.User;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * list 模糊搜索
 */
public class ListFilterUtil {

    private static final Logger logger = LoggerFactory.getLogger(ListFilterUtil.class);

    /**
     * @description: 模糊查找
     * @param condition 状态
     * @param target 筛选目标
     * @param list 数据集合
     * @param function 函数
     * @return: java.util.List<T>
     * @author: evildoer
     * @datetime: 2021/3/2 11:32
     */
    public static <T> List<T> listFilter(boolean condition, String target, List<T> list, Function<T, ?> function) {
        List<T> result = null;
        if (condition) {
            result = listFilter(target, list, function);
        }
        // 不满足条件直接返回原数据集合
        return Optional.ofNullable(result).orElse(list);
    }


    public static <T> List<T> listFilter(String target, List<T> list, Function<T, ?> function) {
        List<T> result = new LinkedList<>();
        //该处决定大小写是否敏感
        if(target.contains(".")){
            target = target.replace(".","\\.");
        }
        logger.info("替换后的目的字符：{}", target);
        Pattern pattern = Pattern.compile(target, Pattern.CASE_INSENSITIVE);
        for (T t : list) {
            Matcher matcher = pattern.matcher(function.apply(t).toString());
            if (matcher.find()) {
                result.add(t);
            }
        }
        return result;
    }

    public static void main(String[] args) {
        // 通过用户姓名模糊查找
        String name = "1.0";
        List<User> list = new ArrayList<>();
        list.add(new User("1.0.1.111", 18));
        list.add(new User("1.2.1.121", 18));
        list.add(new User("1.0.0.1", 18));
        list.add(new User("1.0.1.141", 19));
        list.add(new User("1.0.0.111", 20));
        logger.info("name-----   {}",name);
        list = ListFilterUtil.listFilter(
                // 判断name是否为空，空则不执行
                StringUtils.isNotEmpty(name),
                name,
                list,
                User::getUserName);
        logger.info(JSON.toJSONString(list));

        /*List<String> list1 = Ipv4Util.list("192.168.10.0", 16, true);

        logger.info(JSON.toJSONString(list1));*/

    }
}
