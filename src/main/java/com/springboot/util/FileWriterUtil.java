package com.springboot.util;

import org.apache.commons.lang3.StringUtils;

import java.io.FileWriter;
import java.io.IOException;

/**
 * 字符串写入文件
 * @author 29678
 */
public class FileWriterUtil {

    public static final String PATH = "D:/token.txt";

    public static final String FULL_PATH = "D:/%s";



    /**
     * 将字符串写入文件
     * @param str
     */
    public static void writerFile(String str, String fileName){

        if(StringUtils.isEmpty(fileName)){
            fileName = PATH;
        }else {
            fileName = String.format(FULL_PATH,fileName);
        }

        try(FileWriter writer = new FileWriter(fileName)){
            writer.write(str);
            writer.flush();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

}
