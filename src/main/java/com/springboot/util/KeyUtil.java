package com.springboot.util;

/**
 * 生成唯一id   不太适用于数据库主键 因为同步，可能效率会慢
 */
public class KeyUtil {
    public static KeyUtil ins = new KeyUtil();
    private int count;
    private synchronized int genUniqueCode() {
        return count++ & 0x7fffffff;
    }
    private KeyUtil() {

    }
    /**
     * 生成唯一的的主键
     * 格式:时间毫秒值+自增数
     * 加线程锁,防止高并发时id冲突
     * @return
     */
    public static String genUniqueKey() {
        return  System.currentTimeMillis() +Long.toString((long) ins.genUniqueCode());
    }
}
