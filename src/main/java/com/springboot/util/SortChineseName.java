package com.springboot.util;

import com.google.common.collect.Lists;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.Collator;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * 按中文方式排序 拼音  ()
 */
public class SortChineseName implements Comparator<String> {

    public static final Logger logger = LogManager.getLogger(SortChineseName.class.getName());

    Collator collator = Collator.getInstance(java.util.Locale.CHINA);

    @Override
    public int compare(String o1, String o2) {
        if (collator.compare(o1, o2)>0){
            return 1;
        }else if (collator.compare(o1, o2)<0){
            return -1;
        }
        return 0;
    }

    public static void main(String[] args) {
        List<String> list = Lists.newArrayList();
        list.add("你");
        list.add("我");
        list.add("他");
        list.add("在");
        list.add("来");

        logger.info("start:   {}", Arrays.toString(list.toArray()));

        list.sort(new SortChineseName());

        logger.info("end:   {}", Arrays.toString(list.toArray()));

    }
}
