package com.springboot.util;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Maps;
import com.springboot.bean.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.Map;

/**
 * 通过反射处理对象
 */
public class ClassFieldUtil {

    private static final Logger logger = LoggerFactory.getLogger(ClassFieldUtil.class);

    /**
     * 获取传入对象的   属性和值
     * @param object
     * @return
     */
    public static Map<String,Object> getField(Object object){
        try {
            Map<String,Object> map = Maps.newHashMap();
            Class<?> clazz = object.getClass();
            Field[] fields = clazz.getDeclaredFields();
            for(Field field: fields){
                field.setAccessible(true);
                String name = field.getName();
                Object o = field.get(object);
                map.put(name,o);
            }
            return map;
        } catch (Exception e) {
            logger.error("获取对象属性和值错误：",e);
        }
        return null;
    }

    public static void getField(String name,Object object){
        try {
            Class<?> clazz = object.getClass();
            Field declaredField = clazz.getDeclaredField(name);
            //Field field = clazz.getField(name);
            logger.info(declaredField.getName());
            //logger.info(field.getName());
        } catch (NoSuchFieldException e) {
            logger.error("error",e);
        }
    }

    public static void main(String[] args) {
        User user = new User();
        user.setId(123);
        user.setUserName("test");

        Map<String, Object> field = getField(user);

        logger.info(JSON.toJSONString(field));

        getField("userName",user);
    }
}
