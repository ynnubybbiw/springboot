package com.springboot.util;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;

public class IsExistField {
    public static Boolean isExistField(String field, Object obj) {
        if (obj == null || StringUtils.isEmpty(field)) {
            return null;
        }

        Object o = JSON.toJSON(obj);

        JSONObject jsonObj = new JSONObject();

        if (o instanceof JSONObject) {
            jsonObj = (JSONObject) o;

        }

        return jsonObj.containsKey(field);

    }

}
