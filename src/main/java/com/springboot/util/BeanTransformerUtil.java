package com.springboot.util;

import org.hibernate.transform.BasicTransformerAdapter;
import org.springframework.util.ConcurrentReferenceHashMap;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.ref.SoftReference;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;
import java.util.Map;

/**
 * 替换Transformers.aliasToBean方法，实现小驼峰字段转换
 * jpa字段转换
 */
public class BeanTransformerUtil extends BasicTransformerAdapter {

    //set方法缓存
    private static final Map<String, SoftReference<Map<String, Method>>> beanCache = new ConcurrentReferenceHashMap<>(2 >> 6);

    private Class<?> clazz;

    private BeanTransformerUtil(Class<?> clazz) {
        this.clazz = clazz;
    }

    /**
     * 暴露公开调用 方法
     * @param clazz
     * @return
     */
    public static BeanTransformerUtil aliasToBean(Class<?> clazz){
        return new BeanTransformerUtil(clazz);
    }

    @Override
    public Object transformTuple(Object[] tuple, String[] aliases) {
        Object o = null;
        try{
            o = clazz.newInstance();
            Map<String, Method> cacheBeans = getMethods(o);

            for(int i =0;i<aliases.length;i++){
                String dataCode = camelName(aliases[i]);
                Object val = tuple[i];
                if(val != null){
                    cacheBeans.get(dataCode).invoke(o,val);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return o;
    }

    /**
     * 获得bean的所有set方法，并保存
     * @param bean
     * @return
     * @throws IntrospectionException
     */
    Map<String,Method> getMethods(Object bean) throws IntrospectionException {
        //判断缓存中是否存在bean的解析
        SoftReference<Map<String,Method>> methodsCacheSoftReference = beanCache.get(bean.getClass().getName());
        if(methodsCacheSoftReference!=null && methodsCacheSoftReference.get() != null){
            return methodsCacheSoftReference.get();
        }
        //没有缓存，解析bean
        Field[] fields = bean.getClass().getDeclaredFields();
        Map<String,Method> cacheBean = new HashMap<>();
        SoftReference<Map<String,Method>> cacheBeanSoftRef=new SoftReference<>(cacheBean);
        for(Field field : fields){
            //判断是否为final，如果是则跳过
            if(Modifier.isFinal(field.getModifiers())){
                continue;
            }
            Method method = getMethod(bean.getClass(),field.getName());
            if(method !=null){
                cacheBean.put(field.getName(),method);
            }
        }
        //保存解析结果
        beanCache.put(bean.getClass().getName(),cacheBeanSoftRef);
        return cacheBean;
    }

    /**
     * 获得属性的相应set方法
     * @param clazz
     * @param code
     * @return
     * @throws IntrospectionException
     */
    Method getMethod(Class clazz,String code) throws IntrospectionException {
        PropertyDescriptor pd = new PropertyDescriptor(code,clazz);
        //获得get方法
        return pd.getWriteMethod();
    }

    /**
     * 转换为小驼峰
     * @param name
     * @return
     */
    private  String camelName(String name) {
        StringBuilder result = new StringBuilder();
        // 快速检查
        if (name == null || name.isEmpty()) {
            // 没必要转换
            return "";
        } else if (!name.contains("_")) {
            // 不含下划线，仅将首字母小写
            return name.substring(0, 1).toLowerCase() + name.substring(1);
        }
        // 用下划线将原始字符串分割
        String[] camels = name.split("_");
        for (String camel :  camels) {
            // 跳过原始字符串中开头、结尾的下换线或双重下划线
            if (camel.isEmpty()) {
                continue;
            }
            // 处理真正的驼峰片段
            if (result.length() == 0) {
                // 第一个驼峰片段，全部字母都小写
                result.append(camel.toLowerCase());
            } else {
                // 其他的驼峰片段，首字母大写
                result.append(camel.substring(0, 1).toUpperCase());
                result.append(camel.substring(1).toLowerCase());
            }
        }
        return result.toString();
    }

}
