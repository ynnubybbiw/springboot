package com.springboot.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 微信服务号推送模板信息
 */
public class WxTokenUtil {

    public static final Logger logger = LogManager.getLogger(WxTokenUtil.class.getName());

    public static void main(String[] args) {
        /*JSONObject token = WxTokenUtil.getAccessToken("wwac3738d20624f3f6", "eyFHHjFyrzT58fKM8IitQupijxJfRPkLlHAdz6RzzIw");
        logger.info(token);*/

        WxTokenUtil.sendMsg("wwac3738d20624f3f6", "eyFHHjFyrzT58fKM8IitQupijxJfRPkLlHAdz6RzzIw");

    }

    /**
     * 获取企业微信token
     * @param appId
     * @param appSecret
     * @return
     */
    public static JSONObject getAccessToken(String appId, String appSecret) {
        //String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + appId + "&secret=" + appSecret;
        String url = "https://qyapi.weixin.qq.com/cgi-bin/gettoken?corpid=" + appId + "&corpsecret=" + appSecret;
        String accessToken = null;
        JSONObject jsonObj = null;
        try {
            URL urlGet = new URL(url);
            HttpURLConnection http = (HttpURLConnection) urlGet.openConnection();
            http.setRequestMethod("GET"); // 必须是get方式请求
            http.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            http.setDoOutput(true);
            http.setDoInput(true);
            http.connect();
            InputStream is = http.getInputStream();
            int size = is.available();
            byte[] jsonBytes = new byte[size];
            is.read(jsonBytes);
            accessToken = new String(jsonBytes, StandardCharsets.UTF_8);
            jsonObj = JSONObject.parseObject(accessToken);
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return jsonObj;
    }

    /**
     * 通过企业微信发送消息
     * @param appid
     * @param appSecret
     */
    public static void sendMsg(String appid, String appSecret){
        JSONObject jsonObject = getAccessToken(appid,appSecret);
        logger.info("1--->{}", jsonObject);
        String accessToken = jsonObject.getString("access_token");

        String msg = "{\"touser\":\"@all\",\"msgtype\":\"textcard\",\"agentid\":1000002,\"textcard\":{\"title\":\"领奖通知\",\"description\":\"<div style='font-weight:bold;'>2016年9月26日</div> <div class='normal'>恭喜你抽中iPhone 7一台，领奖码：xxxx</div><div class='highlight'>请于2016年10月10日前联系行政同事领取</div>\",\"url\":\"www.baidu.co\",\"btntxt\":\"更多\"}}";

        String r = HttpUtil.getJsonData(JSONObject.parseObject(msg), "https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token=" + accessToken);
        logger.info("result:{}",r);
    }

    public static void sendTemp(String appid, String appSecret, Double realAmount, String shopName, String openId) {
        /*String url = "https://api.weixin.qq.com/cgi-bin/token";
        String param = "grant_type=client_credential" + "&appid=" + appid + "&secret=" + appSecret;
        String accTemp = HttpSend.sendGet(url, param);
        JSONObject jsonObject = JSONObject.parseObject(accTemp);*/

        JSONObject jsonObject = getAccessToken(appid,appSecret);

        System.out.println("1--->" + jsonObject);
        String accessToken = jsonObject.getString("access_token"); // 获取到了access_token，调用接口都要用到的，有时效
        // 封装要发送的json
        Map<String, Object> map = Maps.newHashMap();
        map.put("touser", openId);//你要发送给某个用户的openid 前提是已关注该公众号,该openid是对应该公众号的，不是普通的openid
        map.put("template_id", "VkxwzizOa2YS_M2RQbVQHPHzHTbabAqLvqU5FNFqQVs");//模板消息id
        //map.put("url","https://www.vipkes.cn");//用户点击模板消息，要跳转的地址
        // 封装miniprogram 跳转小程序用,不跳不要填
        Map<String, String> mapA = new HashMap<>();
        mapA.put("appid", ""); //小程序appid
        mapA.put("pagepath", ""); //小程序页面pagepath
        map.put("miniprogram", mapA);

        // 以下就是根据模板消息的格式封装好，我模板的是：问题反馈结果通知  可以和我一样试试
        // 封装first
        Map<String, Object> firstMap = Maps.newHashMap();
        firstMap.put("value", "新的消费通知！"); //内容
        firstMap.put("color", "#173177"); //字体颜色

        // 封装keyword1 提交的问题
        Map<String, Object> keyword1Map = Maps.newHashMap();
        keyword1Map.put("value", shopName);
        keyword1Map.put("color", "#fff");

        // 封装keyword2此处也可以是商品名
        Map<String, Object> keyword2Map = Maps.newHashMap();
        keyword2Map.put("value", "-");
        keyword2Map.put("color", "#fff");

        // 封装keyword2此处可以是商品价格
        Map<String, Object> keyword3Map = Maps.newHashMap();
        keyword3Map.put("value", realAmount + "元");
        keyword3Map.put("color", "#fff");

        // 封装keyword4
        // 封装remark
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Map<String, Object> keyword4Map = Maps.newHashMap();
        keyword4Map.put("value", simpleDateFormat.format(date));
        keyword4Map.put("color", "#fff");


        Map<String, Object> remarkMap = Maps.newHashMap();
        remarkMap.put("value", "尊敬的用户,您于" + simpleDateFormat.format(date) + "在" + shopName + "消费了" + realAmount + "元");
        remarkMap.put("color", "#fff");

        // 封装data
        Map<String, Object> dataMap = Maps.newHashMap();
        dataMap.put("first", firstMap);
        dataMap.put("keyword1", keyword1Map);
        dataMap.put("keyword2", keyword2Map);
        dataMap.put("keyword3", keyword3Map);
        dataMap.put("keyword4", keyword4Map);
        dataMap.put("remark", remarkMap);

        map.put("data", dataMap);
        String r = HttpUtil.getJsonData(JSONObject.parseObject(JSON.toJSONString(map)), "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" + accessToken); //发送模板消息，这里有个工具类，我给你哟
        System.out.println("-->" + r);
    }

    public static void sendTempPoint(String appid, String accessToken, String appSecret, Integer Id, String name, String mobile, Double amount, String wechatOpenId) {
        /*String url = "https://api.weixin.qq.com/cgi-bin/token";
        String param = "grant_type=client_credential" + "&appid=" + appid + "&secret=" + appSecret;
        String accTemp = HttpSend.sendGet(url, param);
        JSONObject jsonObject = JSONObject.parseObject(accTemp);*/

        JSONObject jsonObject = getAccessToken(appid,appSecret);

        System.out.println("1--->" + jsonObject);
        accessToken = jsonObject.getString("access_token"); // 获取到了access_token，调用接口都要用到的，有时效
        // 封装要发送的json
        Map<String, Object> map = Maps.newHashMap();
        map.put("touser", wechatOpenId);//你要发送给某个用户的openid 前提是已关注该公众号,该openid是对应该公众号的，不是普通的openid
        map.put("template_id", "672dPs0Jwl9zLvhVrncI0nyEHxX3Mx-fPNLBcIEb05A");//模板消息id
        //map.put("url","https://www.vipkes.cn");//用户点击模板消息，要跳转的地址
        // 封装miniprogram 跳转小程序用,不跳不要填
        Map<String, String> mapA = Maps.newHashMap();
        mapA.put("appid", ""); //小程序appid
        mapA.put("pagepath", ""); //小程序页面pagepath
        map.put("miniprogram", mapA);

        // 以下就是根据模板消息的格式封装好，我模板的是：问题反馈结果通知  可以和我一样试试
        // 封装first
        Map<String,String> firstMap = Maps.newHashMap();
        firstMap.put("value", "会员积分消费提醒！"); //内容
        firstMap.put("color", "#173177"); //字体颜色
        String newname;
        try {
            newname = (java.net.URLDecoder.decode(name, "UTF-8"));
        } catch (UnsupportedEncodingException e) {
            newname = name;
        }

        // 封装keyword1 提交的问题
        Map<String,String> XM = Maps.newHashMap();
        XM.put("value", newname);
        XM.put("color", "#fff");

        // 封装keyword2此处也可以是商品名
        Map<String,Object> KH = Maps.newHashMap();
        KH.put("value", Id);
        KH.put("color", "#fff");
        Date date = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        // 封装keyword3此处可以是商品价格
        Map<String,String> CONTENTS = Maps.newHashMap();
        CONTENTS.put("value", "尊敬的用户,您的账户于" + simpleDateFormat.format(date) + "增加了" + amount + "积分");
        CONTENTS.put("color", "#fff");


        Map<String,String> remarkMap = Maps.newHashMap();
        remarkMap.put("value", "消息惠顾");
        remarkMap.put("color", "#fff");

        // 封装data
        Map<String,Object> dataMap = Maps.newHashMap();
        dataMap.put("first", firstMap);
        dataMap.put("XM", XM);
        dataMap.put("KH", KH);
        dataMap.put("CONTENTS", CONTENTS);
        dataMap.put("remark", remarkMap);

        map.put("data", dataMap);
        String r = HttpUtil.getJsonData(JSONObject.parseObject(JSON.toJSONString(map)), "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=" + accessToken); //发送模板消息，这里有个工具类，我给你哟
        System.out.println("-->" + r);
    }


}
