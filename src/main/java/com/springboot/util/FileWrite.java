package com.springboot.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

/**
 * @author 29678
 */
public class FileWrite {

    private static final Logger logger = LoggerFactory.getLogger(FileWrite.class);


    public static void mkdirPath(String filePath) {
        File file = new File(filePath);
        String parent = file.getParent();
        logger.info("parent is {}", parent);
        File parentFile = file.getParentFile();
        if (!parentFile.exists()) {
            boolean mkdirs = parentFile.mkdirs();
            logger.info("mkdirs is {}", mkdirs);
        }
    }

    public static void fileWrite(String filePath, List<String> lines) {

        mkdirPath(filePath);


        try (FileOutputStream fos = new FileOutputStream(filePath);
             FileChannel channel = fos.getChannel()) {

            for (String line : lines) {
                ByteBuffer buffer = ByteBuffer.wrap((line + System.lineSeparator()).getBytes());
                int write = channel.write(buffer);
                logger.info("write {}" , write);
            }
        } catch (Exception e) {
            logger.error(e.getMessage(),e);
        }
    }


    public static void deleteFile(String filePath){
        try {
            File file = new File(filePath);
            if (file.exists()) {
                Files.delete(file.toPath());
            }
        } catch (IOException e) {
            logger.error(e.getMessage(),e);
        }
    }

    public static void writeToFile(String scriptContent, String filePath){
        mkdirPath(filePath);
        File file = new File(filePath);
        try (FileWriter fileWriter = new FileWriter(file)) {
            fileWriter.write(scriptContent);
        } catch (IOException e) {
            logger.error(e.getMessage(),e);
        }
    }


    public static void main(String[] args) {

        String path = "D:\\aa\\bb\\cc\\output.txt";

        List<String> lines = new ArrayList<>();
        lines.add("hello");
        lines.add("world");
        lines.add("hello13");
        lines.add("world25");
        lines.add("world55");

        //fileWrite(path,lines);


        //deleteFile(path);

        String scriptContent = "import org.springframework.beans.factory.annotation.Autowired;\n" +
                "import org.springframework.stereotype.Service;\n" +
                "\n" +
                "import java.io.File;\n" +
                "import java.io.FileWriter;\n" +
                "import java.io.IOException;\n" +
                "import java.util.Optional;\n" +
                "\n" +
                "@Service\n" +
                "public class ScriptService {\n" +
                "\n" +
                "    @Autowired\n" +
                "    private ScriptRepository scriptRepository;\n" +
                "\n" +
                "    // 从数据库中获取脚本，并写入文本文件\n" +
                "    public void writeScriptToFile(Long scriptId, String filePath) {\n" +
                "        // 根据ID从数据库中查找脚本\n" +
                "        Optional<ScriptEntity> scriptEntityOptional = scriptRepository.findById(scriptId);\n" +
                "        if (scriptEntityOptional.isPresent()) {\n" +
                "            ScriptEntity scriptEntity = scriptEntityOptional.get();\n" +
                "            String scriptContent = scriptEntity.getScriptContent();\n" +
                "            \n" +
                "            // 将脚本写入文件\n" +
                "            try {\n" +
                "                writeToFile(scriptContent, filePath);\n" +
                "                System.out.println(\"Script written to file successfully.\");\n" +
                "            } catch (IOException e) {\n" +
                "                System.err.println(\"Error writing script to file: \" + e.getMessage());\n" +
                "            }\n" +
                "        } else {\n" +
                "            System.err.println(\"Script with ID \" + scriptId + \" not found.\");\n" +
                "        }\n" +
                "    }\n" +
                "\n" +
                "    // 实现将脚本写入文件的方法\n" +
                "    private void writeToFile(String scriptContent, String filePath) throws IOException {\n" +
                "        File file = new File(filePath);\n" +
                "        try (FileWriter fileWriter = new FileWriter(file)) {\n" +
                "            fileWriter.write(scriptContent);\n" +
                "        }\n" +
                "    }\n" +
                "}\n";
        writeToFile(scriptContent,path);
    }

}
