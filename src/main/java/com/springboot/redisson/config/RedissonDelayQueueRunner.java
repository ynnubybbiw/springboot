package com.springboot.redisson.config;

import com.springboot.redisson.RedissonDelayQueue;
import com.springboot.redisson.execute.RedissonDelayQueueExecute;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import java.time.LocalTime;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 *
 * 启动监听队列
 *
 * 或可改为第三方定时调用
 * @author 29678
 */
@Component
public class RedissonDelayQueueRunner implements CommandLineRunner {

    private static final Logger logger = LogManager.getLogger(RedissonDelayQueueRunner.class.getName());

    @Autowired
    private RedissonDelayQueue redissonDelayQueue;

    @Autowired
    private ApplicationContext applicationContext;

    //初始延迟时间  0,则任务会立即开始执行
    private static final int INITIAL_DELAY = 0;

    // 毫秒   表示在一次任务执行结束到下一次任务执行开始之间的时间间隔。
    private static final int FIXED_DELAY = 1000;

    private final ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();

    /**
     * 定时任务调用
     * @param args
     */
    @Override
    public void run(String... args){
        CompletableFuture.runAsync(this::processQueuesAsync);
    }

    /**
     * 定时调用
     */
    private void processQueuesAsync() {
        //下次任务会在这次执行完才执行
        scheduler.scheduleWithFixedDelay(this::processQueues, INITIAL_DELAY, FIXED_DELAY, TimeUnit.MILLISECONDS);
    }

    /**
     * 不能使用递归，递归一次，内存会涨一次
     *
     *
     * 递归调用
     * @param args
     */
   /* @Override
    public void run(String... args){
        CompletableFuture.runAsync(this::processQueues);
    }*/


    private void processQueues() {
        logger.info("调用processQueues，时间 : {}", LocalTime.now().getSecond());

            try {
                for (RedisDelayQueueEnum queueEnum : RedisDelayQueueEnum.values()) {
                    processQueue(queueEnum);
                }
            }catch (Exception e) {
                logger.error("处理Redis延迟队列时发生错误", e);
            }finally {

                //递归调用时，需要执行完调用，因下边有阻塞队列
                /*processQueues();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    logger.error("InterruptedException错误", e);
                }*/
            }

    }

    private void processQueue(RedisDelayQueueEnum queueEnum) {
        try {
            //阻塞队列
            Object value = redissonDelayQueue.getDelayQueue(queueEnum.getCode());
            if (value != null) {
                RedissonDelayQueueExecute handler = applicationContext.getBean(queueEnum.getBeanId(), RedissonDelayQueueExecute.class);
                handler.execute(value);
            }
        } catch (Exception e) {
            logger.error("处理队列 {} 时发生错误", queueEnum.getCode(), e);
        }
    }
}
