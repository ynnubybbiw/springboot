package com.springboot.redisson;

import com.springboot.exception.TestException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.redisson.api.RBlockingDeque;
import org.redisson.api.RDelayedQueue;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Redisson 添加信息到延时队列
 */
@Component
public class RedissonDelayQueue {

    private static final Logger logger = LogManager.getLogger(RedissonDelayQueue.class.getName());


    @Autowired
    RedissonClient redissonClient;



    /**
     * 添加延迟队列
     *
     * @param value     队列值
     * @param delay     延迟时间
     * @param timeUnit  时间单位
     * @param queueCode 队列键
     * @param <T>
     */
    public <T> void addDelayQueue(T value, long delay, TimeUnit timeUnit, String queueCode) {
        try {
            RBlockingDeque<Object> blockingDeque = redissonClient.getBlockingDeque(queueCode);
            RDelayedQueue<Object> delayedQueue = redissonClient.getDelayedQueue(blockingDeque);
            delayedQueue.offer(value, delay, timeUnit);
            logger.info("添加延时队列成功 队列键：{}，队列值：{}，延迟时间：{} 秒", queueCode, value, timeUnit.toSeconds(delay));
        } catch (Exception e) {
            logger.error("添加延时队列失败 {}", e.getMessage());
            throw new TestException("添加延时队列失败");
        }
    }

    /**
     * 获取延迟队列
     *
     * @param queueCode
     * @param <T>
     * @return
     * @throws InterruptedException
     */
    public <T> T getDelayQueue(String queueCode) throws InterruptedException {
        //阻塞队列
        RBlockingDeque<Map> blockingDeque = redissonClient.getBlockingDeque(queueCode);
        T value = (T) blockingDeque.take();
        return value;
    }
}
