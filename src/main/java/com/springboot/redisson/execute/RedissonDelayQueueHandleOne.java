package com.springboot.redisson.execute;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author 29678
 */
@Component
public class RedissonDelayQueueHandleOne implements RedissonDelayQueueExecute<Map<String,Object>> {


    private static final Logger logger = LogManager.getLogger(RedissonDelayQueueHandleOne.class.getName());

    @Override
    public void execute(Map<String, Object> map) {
        logger.info("{} {}", "RedissonDelayQueueHandleOne 处理", map);
    }
}
