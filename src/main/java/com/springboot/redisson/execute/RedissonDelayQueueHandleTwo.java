package com.springboot.redisson.execute;

import com.alibaba.fastjson.JSONObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

/**
 * @author 29678
 */
@Component
public class RedissonDelayQueueHandleTwo implements RedissonDelayQueueExecute<JSONObject>{


    private static final Logger logger = LogManager.getLogger(RedissonDelayQueueHandleTwo.class.getName());

    @Override
    public void execute(JSONObject jsonObject) {
        logger.info("RedissonDelayQueueHandleTwo 队列处理,{}", jsonObject);
    }
}
