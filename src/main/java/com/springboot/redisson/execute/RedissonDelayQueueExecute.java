package com.springboot.redisson.execute;

/**
 * 延迟队列执行器
 * @param <T>
 */
public interface RedissonDelayQueueExecute<T> {

    void execute(T t);


}
