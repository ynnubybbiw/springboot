package com.springboot.multithreading;

import com.springboot.bean.Book;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.security.SecureRandom;
import java.util.concurrent.atomic.AtomicReference;

/**
 * 原子性 更新 操作
 *
 * 使用场景：一个线程使用book对象，另一个线程负责定时读表，更新这个对象。那么就可以用AtomicReference这个类。
 */
public class AtomicReferenceTest {

    private static final Logger logger = LogManager.getLogger(AtomicReferenceTest.class.getName());

    public static final AtomicReference<Book> ATOMIC_STUDENT = new AtomicReference<>();

    public static void main(String[] args) {

        Book book1 = new Book("book1","author1");
        Book book2 = new Book("book2","author2");

        //主线程设置初始值
        ATOMIC_STUDENT.set(book1);
        logger.info("main 设置初始值完成");

        for (int i = 0; i < 10; i++)
        {
            //其他线程更改。
            new Thread(() -> {
                //noinspection AlibabaAvoidManuallyCreateThread
                try
                {
                    SecureRandom instanceStrong = SecureRandom.getInstanceStrong();
                    //为了 使得控制台打印的 更改student1的线程 能显示出不一样 每个线程随机停顿 多执行几次能看出效果
                    Thread.sleep(Math.abs(instanceStrong.nextInt()+1));
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    Thread.currentThread().interrupt();
                }
                //预期值 book1和 当前值（上面的atomicStudent.set(book1);）相等时候 赋予book2新的值
                if (ATOMIC_STUDENT.compareAndSet(book1,book2))
                {
                    logger.info("线程id ： {} 更改成功，新值 ：{} ：{}", Thread.currentThread().getId(),ATOMIC_STUDENT.get().getName(),ATOMIC_STUDENT.get().getAuthor());
                }else {
                    logger.info("线程id ： {} 更改失败。",Thread.currentThread().getId());
                }
            }).start();
        }

    }
}
