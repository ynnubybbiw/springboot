package com.springboot.multithreading;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DeepenSleep implements Runnable{

    private static final Logger logger = LogManager.getLogger(DeepenSleep.class.getName());

    @Override
    public void run() {
        try {
            logger.info( "run thread...");
            //firstMethod();
            firstMethodSync();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private int number = 10;

    /**
     * 不同点在于，主线程睡眠之后，没有释放锁，
     * dt线程执行firstMethod并不需要锁，
     * 因此先run firstMethod中的逻辑，先加100，
     * 然今，主线程睡醒之后，再执行剩下的逻辑，乘以200
     * @throws Exception
     */
    public void firstMethod() throws Exception {
        logger.info( "in first method");
        number += 100;
        logger.info( "+100=" + number);
    }

    /**
     * 主线程启动起来，因为创建线程等的资源消耗，所以主线程会先执行 dt.secondMethod()，
     * 因此会先输出prepare run second method，
     * 其后执行secondMehtod方法(注意该方法是要先闹到锁对象)，
     * 而该方法直接将线程睡眠2s（注意此处对象锁DeepenSleep的实例对象并没有释放），
     * 然后执行线程dt的run方法，该方刚发执行dt的firstMethod，
     * 然而，该方法也是需要获取锁对象的，而此时他没先不能获取到，
     * 因为secondMehtod没有释放锁（准确点讲，主线程没有释放锁）；然后等了2s，主线程睡眠时间已过，
     * 他warkup之后，因为还拥有锁，因此直接run secondMethod的剩下的方法，先输出”wake up”,
     * 然后执行 number*200，执行完，主线程释放掉锁，
     * 而dt线程拿到锁，执行run方法，拿到锁，执行run方法的synchronized的剩余方法：
     * 先输出”in first method”，然后执行加100的操作。
     * @throws Exception
     */
    public void firstMethodSync() throws Exception {
         synchronized (this) {
             logger.info( "in first method");
            number += 100;
             logger.info( "+100=" + number);
        }
    }

    public void secondMethod() throws Exception {
        synchronized ( this) {
            logger.info( "in second method, prepare sleep");
            /*
             * (休息2S,阻塞线程) 以验证当前线程对象的机锁被占用时, 是否被可以访问其他同步代码块
             */
            Thread. sleep(2000);
            logger.info( "wake up!!");
            // this.wait(2000);
            number *= 200;
            logger.info( "*200=" + number);
        }
    }

    public static void main(String[] args) throws Exception {
        DeepenSleep dt = new DeepenSleep();
        Thread thread = new Thread(dt);
        thread.start();
        logger.info("prepare run second method");
        dt.secondMethod();
    }
}
