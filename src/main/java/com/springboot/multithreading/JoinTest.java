package com.springboot.multithreading;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * join()⽅法是Thread类的⼀个实例⽅法。它的作⽤是让当前线程陷⼊“等待”状态，等
 * join的这个线程执⾏完成后，再继续执⾏当前线程。
 * 有时候，主线程创建并启动了⼦线程，如果⼦线程中需要进⾏⼤量的耗时运算，主
 * 线程往往将早于⼦线程结束之前结束。
 * 如果主线程想等待⼦线程执⾏完毕后，获得⼦线程中的处理完的某个数据，就要⽤
 * 到join⽅法了。
 */
public class JoinTest {

    private static final Logger logger = LogManager.getLogger(JoinTest.class.getName());

    static class ThreadA implements Runnable {
        @Override
        public void run() {
            try {
                logger.info("我是⼦线程，我先睡⼀秒");
                Thread.sleep(5000);
                logger.info("我是⼦线程，我睡完了⼀秒");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    public static void main(String[] args) throws InterruptedException {
        logger.info("主线程开始");
        Thread thread = new Thread(new ThreadA());
        thread.start();
        thread.join();
        logger.info("如果不加join⽅法，我会先被打出来，加了就不⼀样了");
    }
}
