package com.springboot.multithreading;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.PipedReader;
import java.io.PipedWriter;

/**
 * 管道 流写入读取测试
 */
public class PipeTest {

    private static final Logger logger = LogManager.getLogger(PipeTest.class.getName());

    static class ReaderThread implements Runnable {
        private PipedReader reader;
        public ReaderThread(PipedReader reader) {
            this.reader = reader;
        }
        @Override
        public void run() {
            logger.info("this is reader");
            int receive = 0;
            StringBuffer sb = new StringBuffer();
            try {
                //read()
                // 读取此传送流中的下一个数据字符。如果因为已经到达流的末尾而没有可用的字符，则返回值 -1。
                // 在输入数据可用、检测到流的末尾或者抛出异常之前，此方法一直阻塞。
                while ((receive = reader.read()) != -1) {
                    sb.append((char)receive);
                }
                logger.info("从流里读取到的字符：{}",sb);
            } catch (IOException e) {
                e.printStackTrace();
            }
            logger.info("读取完成！");
        }
    }

    static class WriterThread implements Runnable {
        private PipedWriter writer;
        public WriterThread(PipedWriter writer) {
            this.writer = writer;
        }
        @Override
        public void run() {
            logger.info("this is writer");
            try {
                writer.write("写入字符流：test");
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    public static void main(String[] args) throws IOException, InterruptedException{
        PipedWriter writer = new PipedWriter();
        PipedReader reader = new PipedReader();
        writer.connect(reader); // 这⾥注意⼀定要连接，才能通信
        new Thread(new ReaderThread(reader)).start();
        Thread.sleep(10000);
        new Thread(new WriterThread(writer)).start();
    }
}
