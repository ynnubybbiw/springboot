package com.springboot.multithreading;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

public class BlockedTest {

    private static final Logger logger = LogManager.getLogger(BlockedTest.class.getName());

    /**
     * 在这个例⼦中，由于main线程休眠，所以线程a的run()⽅法跟着执⾏，线程b再接
     * 着执⾏。
     * 在线程a执⾏run()调⽤testMethod()之后，线程a休眠了2000ms（注意这⾥是没有
     * 释放锁的），main线程休眠完毕，接着b线程执⾏的时候是争夺不到锁的，所以这
     * ⾥输出：
     * a:TIMED_WAITING
     * b:BLOCKED
     *
     *
     *  测试方法不加睡眠  1000 ms
     * 测试⽅法的main线程只保证了a，b两个线程调⽤start()⽅法（转化为
     * RUNNABLE状态），还没等两个线程真正开始争夺锁，就已经打印此时两个
     * 线程的状态（RUNNABLE）了。
     * @throws InterruptedException
     */
    @Test
    public void blockedTest() throws InterruptedException {
        Thread a = new Thread(() -> testMethod(), "a");
        Thread b = new Thread(() -> testMethod(), "b");
        a.start();
        Thread.sleep(1000L); // 需要注意这⾥main线程休眠了1000毫秒，⽽testMethod()⾥休眠2000
        b.start();
        logger.info(a.getName() + ":" + a.getState()); // 输出？
        logger.info(b.getName() + ":" + b.getState()); // 输出？
    }
    // 同步⽅法争夺锁
    private synchronized void testMethod() {
        try {
            Thread.sleep(2000L);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
