package com.springboot.multithreading;

import com.springboot.bean.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class WaitAndNotify {

    private static final Logger logger = LogManager.getLogger(WaitAndNotify.class.getName());

    private static final User lock = new User();

    static class ThreadA implements Runnable {
        @Override
        public void run() {
            synchronized (lock) {
                for (int i = 0; i < 5; i++) {
                    try {
                        logger.info("ThreadA: {}", i);
                        lock.notify();
                        lock.wait();
                    } catch (InterruptedException e) {
                       logger.error("ThreadA异常",e);
                    }
                }
                lock.notify();
            }
        }
    }

    static class ThreadB implements Runnable {
        @Override
        public void run() {
            synchronized (lock) {
                for (int i = 0; i < 5; i++) {
                    try {
                        logger.info("ThreadB: {}",i);
                        lock.notify();
                        lock.wait();
                    } catch (InterruptedException e) {
                        logger.error("ThreadB异常",e);
                    }
                }
                lock.notify();
            }
        }
    }
    public static void main(String[] args) throws InterruptedException {
        new Thread(new ThreadA()).start();
        Thread.sleep(10000);
        new Thread(new ThreadB()).start();
    }
}
