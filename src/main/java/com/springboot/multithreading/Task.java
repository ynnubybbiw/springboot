package com.springboot.multithreading;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import java.util.concurrent.*;

public class Task implements Callable<String> {

    private static final Logger logger = LogManager.getLogger(Task.class.getName());

    @Override
    public String call() throws Exception {
        // 模拟计算需要⼀秒
        Thread.sleep(5000);
        return Thread.currentThread().getThreadGroup().getName();
    }

    @Test
    public void methodOne() throws ExecutionException, InterruptedException {
        ExecutorService executor = Executors.newCachedThreadPool();
        Task task = new Task();
        Future<String> submit = executor.submit(task);
        // 注意调⽤get⽅法会阻塞当前线程，直到得到结果。
        // 所以实际编码中建议使⽤可以设置超时时间的重载get⽅法。
        logger.info("result: {}",submit.get());
    }

    @Test
    public void methodTwo() throws ExecutionException, InterruptedException {
        // 使⽤
        ExecutorService executor = Executors.newCachedThreadPool();
        FutureTask<String> futureTask = new FutureTask<>(new Task());
        executor.submit(futureTask);
        logger.info("result: {}",futureTask.get());
    }
}
