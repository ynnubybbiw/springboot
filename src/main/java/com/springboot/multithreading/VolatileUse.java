package com.springboot.multithreading;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * volatile 关键字使用
 * volitile关键字能够保证内存的可⻅性，如果⽤volitile关键字声明了⼀个变
 * 量，在⼀个线程⾥⾯改变了这个变量的值，那其它线程是⽴⻢可⻅更改后的
 * 值的。
 */
public class VolatileUse {

    private static final Logger logger = LogManager.getLogger(VolatileUse.class.getName());

    private static volatile int signal = 0;

    static class ThreadA implements Runnable {
        @Override
        public void run() {
            while (signal < 5) {
                if (signal % 2 == 0) {
                    logger.info("threadA: {}" ,signal);
                    synchronized (this) {
                        signal++;
                    }
                }
            }
        }
    }

    static class ThreadB implements Runnable {
        @Override
        public void run() {
            while (signal < 5) {
                if (signal % 2 == 1) {
                    logger.info("threadB: {}",signal);
                    synchronized (this) {
                        signal = signal + 1;
                    }
                }
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {
        new Thread(new ThreadA()).start();
        Thread.sleep(5000);
        new Thread(new ThreadB()).start();
    }
}
