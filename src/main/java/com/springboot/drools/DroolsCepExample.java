package com.springboot.drools;

import org.kie.api.KieBaseConfiguration;
import org.kie.api.KieServices;
import org.kie.api.conf.EventProcessingOption;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

public class DroolsCepExample {

    public static void main(String[] args) {
        KieServices ks = KieServices.Factory.get();
        KieContainer kContainer = ks.getKieClasspathContainer();
        KieSession kSession = kContainer.newKieSession();

        // 设置事件处理模式为 STREAM 模式
        KieBaseConfiguration kieBaseConfiguration = ks.newKieBaseConfiguration();
        kieBaseConfiguration.setOption(EventProcessingOption.STREAM);




        // 模拟插入一些交易事件
        kSession.insert(new TransactionEvent("T001", 1200, System.currentTimeMillis()));
        kSession.insert(new TransactionEvent("T002", 1500, System.currentTimeMillis() + 1000));
        kSession.insert(new TransactionEvent("T003", 2000, System.currentTimeMillis() + 5000));

        // 执行规则
        kSession.fireAllRules();
        kSession.dispose();
    }
}
