package com.springboot.drools;

import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * @author 29678
 */
@Slf4j
public class HandleTest {


    public static void handle(String msg){

        log.info("handle :{}", msg);

    }


    public static boolean checkout(List<String> bladeNodeList, List<String> targetNodeList){
        bladeNodeList.removeAll(targetNodeList);
        return bladeNodeList.isEmpty();
    }

}
