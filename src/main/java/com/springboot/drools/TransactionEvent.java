package com.springboot.drools;

public class TransactionEvent {

    private String transactionId;
    private double amount;
    private long timestamp; // 事件的时间戳

    private double number;

    // 构造函数、getters和setters
    public TransactionEvent(String transactionId, double amount, long timestamp) {
        this.transactionId = transactionId;
        this.amount = amount;
        this.timestamp = timestamp;
    }

    public String getTransactionId() { return transactionId; }
    public double getAmount() { return amount; }
    public long getTimestamp() { return timestamp; }
}
