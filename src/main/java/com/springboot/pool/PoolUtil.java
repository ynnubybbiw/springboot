package com.springboot.pool;

import com.springboot.bean.Book;
import org.apache.commons.pool2.impl.GenericKeyedObjectPool;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * pool 工具类
 */
public abstract class PoolUtil {

    public static final Logger logger = LogManager.getLogger(PoolUtil.class.getName());

    private static final ConnectionPoolFactory factory = new ConnectionPoolFactory();

    private static final GenericKeyedObjectPool<String, Book> pool = new GenericKeyedObjectPool<>(factory);

    private static final Lock lock = new ReentrantLock();

    static{
        pool.setMaxTotalPerKey(-1);
        pool.setTestOnBorrow(true);
    }

    public static Book getConnectionFromPool(String token) throws Exception {
        if (lock.tryLock(3L, TimeUnit.SECONDS)) {
            try {
                //此方法调用时，如果取不到，会调用 ConnectionPoolFactory 中的create去获取对象。
                return pool.borrowObject(token);
            } finally {
                lock.unlock();
            }
        } else {
            logger.info("获取锁失败！");
            return null;
        }
    }

    public static void returnConnectionToPool(String token, Book book) throws Exception {

        if (lock.tryLock(3L, TimeUnit.SECONDS)) {
            try {
                pool.returnObject(token, book);
            } finally {
                lock.unlock();
            }
        } else {
            logger.info("归还锁失败！");
        }

    }

}
