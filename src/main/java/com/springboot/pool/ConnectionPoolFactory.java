package com.springboot.pool;

import com.alibaba.fastjson.JSON;
import com.springboot.bean.Book;
import org.apache.commons.pool2.BaseKeyedPooledObjectFactory;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ConnectionPoolFactory  extends BaseKeyedPooledObjectFactory<String, Book> {

    public static final Logger logger = LogManager.getLogger(ConnectionPoolFactory.class.getName());

    @Override
    public Book create(String s) throws Exception {
        logger.info("create 被调用: {}", s);
        Book book = new Book();
        book.setName(s);
        return book;
    }

    @Override
    public PooledObject<Book> wrap(Book book) {
        logger.info("wrap 被调用: {}", JSON.toJSONString(book));
        return new DefaultPooledObject<>(book);
    }
}
