package com.springboot.mqtt;

import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author 29678
 */
public class Publisher {

    private static final Logger logger = LoggerFactory.getLogger(Publisher.class);

    public static void main(String[] args) {


        String topic = "mqtt_test";
        String content = "hello 哈哈 成功 "+new LocalDateTime();
        int qos = 1;
        String broker = "tcp://hznqqg.shop:1883";
        String userName = "admin";
        String password = "admin";
        String clientId = "clientId_test_2";


        try(
            // 内存存储
            MemoryPersistence persistence = new MemoryPersistence();
            // 创建客户端
            MqttClient sampleClient = new MqttClient(broker, clientId, persistence);
        ){

            // 创建链接参数
            MqttConnectOptions connOpts = new MqttConnectOptions();
            // 在重新启动和重新连接时记住状态
            connOpts.setCleanSession(false);
            // 设置连接的用户名
            connOpts.setUserName(userName);
            connOpts.setPassword(password.toCharArray());
            // 建立连接
            sampleClient.connect(connOpts);
            // 创建消息
            MqttMessage message = new MqttMessage(content.getBytes());
            // 设置消息的服务质量
            message.setQos(qos);
            // 发布消息
            sampleClient.publish(topic, message);
            // 断开连接
            sampleClient.disconnect();
            // 关闭客户端
        } catch (MqttPersistenceException e) {
            logger.error("MqttPersistenceException", e);
        } catch (MqttException me) {
            logger.error("reason {}", me.getReasonCode());
            logger.error("msg {}", me.getMessage());
            logger.error("loc {}", me.getLocalizedMessage());
            logger.error("stack trace", me);
        }

    }
}
