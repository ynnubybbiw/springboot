package com.springboot.liteflow;

import com.yomahub.liteflow.core.NodeBooleanComponent;
import com.yomahub.liteflow.slot.Slot;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

/**
 * @author 29678
 *
 * 判断结果是否正确，正确则跳出循环
 */
@Component("handleBooleanC")
public class HandleBooleanC extends NodeBooleanComponent {

    public static final Logger logger = LogManager.getLogger(HandleBooleanC.class.getName());

    @Override
    public boolean processBoolean() throws Exception {

        logger.info("进入 HandleBooleanC");
        Slot slot = this.getSlot();

        Thread.sleep(5*1000);

        return true;
    }
}
