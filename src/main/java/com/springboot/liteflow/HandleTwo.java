package com.springboot.liteflow;

import com.springboot.bean.Book;
import com.yomahub.liteflow.core.NodeComponent;
import com.yomahub.liteflow.slot.Slot;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

@Component("b")
public class HandleTwo extends NodeComponent {

    public static final Logger logger = LogManager.getLogger(HandleTwo.class.getName());

    @Override
    public void process() throws Exception {
        Slot slot = this.getSlot();
        Book requestData = slot.getRequestData();
        logger.info("执行b的方法 requestData: {}", requestData);

        Thread.sleep(5*1000);

        requestData.setData("执行b的方法 b放入的信息");
        requestData.setCount(1);
        slot.setRequestData(requestData);
    }
}
