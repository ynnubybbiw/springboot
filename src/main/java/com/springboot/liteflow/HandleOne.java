package com.springboot.liteflow;

import com.springboot.bean.Book;
import com.yomahub.liteflow.core.NodeComponent;
import com.yomahub.liteflow.slot.Slot;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

@Component("a")
public class HandleOne extends NodeComponent {

    public static final Logger logger = LogManager.getLogger(HandleOne.class.getName());

    @Override
    public void process() throws Exception {

        logger.info("进入 HandleOne，  处理dcu/cpu/内存异常");

        Slot slot = this.getSlot();
        Book requestData = slot.getRequestData();
        logger.info("requestData: {}", requestData);

        Thread.sleep(5*1000);
        requestData.setCount(1);
        requestData.setData("执行a的方法 a放入的信息");
        slot.setRequestData(requestData);
    }
}
