package com.springboot.liteflow;

import com.yomahub.liteflow.core.NodeBooleanComponent;
import com.yomahub.liteflow.slot.Slot;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

/**
 * @author 29678
 *
 * 判断结果是否正确，正确则跳出循环
 */
@Component("handleBooleanA")
public class HandleBooleanA extends NodeBooleanComponent {

    public static final Logger logger = LogManager.getLogger(HandleBooleanA.class.getName());

    @Override
    public boolean processBoolean() throws Exception {

        logger.info("进入 HandleBooleanA   液位低告警");

        Slot slot = this.getSlot();
        String tag = this.getTag();
        logger.info("handleBooleanA  tag : {}",tag);

        Thread.sleep(5*1000);

        slot.setRequestData("pdm");

        return true;
    }
}
