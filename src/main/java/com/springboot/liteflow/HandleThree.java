package com.springboot.liteflow;

import com.springboot.bean.Book;
import com.springboot.exception.TestException;
import com.springboot.util.SpringBeanFactoryUtils;
import com.yomahub.liteflow.core.NodeComponent;
import com.yomahub.liteflow.slot.Slot;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

@Component("c")
public class HandleThree extends NodeComponent {

    public static final Logger logger = LogManager.getLogger(HandleThree.class.getName());

    @Override
    public void process() throws Exception {
        Slot slot = this.getSlot();
        Book requestData = slot.getRequestData();
        logger.info("requestData: {}", requestData);

        Thread.sleep(5*1000);

        requestData.setData("执行c的方法 c放入的信息");
        slot.setRequestData(requestData);
        logger.info("执行完成");
        requestData.setCount(1);
        throw new TestException("测试错误情况");

    }


    @Override
    public void onError(Exception e) throws Exception {
        HandleError handleError = (HandleError) SpringBeanFactoryUtils.getBean("handleError");


        Slot slot = this.getSlot();
        Book requestData = slot.getRequestData();
        logger.error("HandleError: {}", requestData);
        handleError.error("报错了："+e.getMessage());
    }
}
