package com.springboot.liteflow;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

@Component("handleError")
public class HandleError {

    public static final Logger logger = LogManager.getLogger(HandleError.class.getName());

    public void error(String message){
        logger.error("error:{}",message);
    }
}
