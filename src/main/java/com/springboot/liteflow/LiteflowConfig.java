package com.springboot.liteflow;

import com.yomahub.liteflow.builder.el.LiteFlowChainELBuilder;


/**
 * @author 29678
 *
 * 生成chain 工具类
 */
public class LiteflowConfig {


    /*public static void getLiteflowChain(String chain, String str){
        LiteFlowChainELBuilder.createChain().setChainId(chain).setEL(
                String.format("THEN(%s)", str)
        ).build();
    }*/

    public static void getLiteflowChain(String chain, String str){
        LiteFlowChainELBuilder.createChain().setChainId(chain).setEL(
                "THEN(a, WHILE(handleBoolean).DO(THEN(getTaskResult)), b, WHILE(handleBoolean).DO(THEN(getTaskResult)) , c  )"
        ).build();
    }

    public static void getFreedomChain(String chain){
        LiteFlowChainELBuilder.createChain().setChainId(chain).setEL(
                "IF(AND(handleBooleanA,handleBooleanA,handleBooleanA),a,b);  "
        ).build();
    }

    public static void getFreedomChain1(String chain) {
        LiteFlowChainELBuilder.createChain().setChainId(chain).setEL(
                "IF(handleBooleanA,a,b);"
        ).build();
    }

    public static void getFreedomChain2(String chain) {
        LiteFlowChainELBuilder.createChain().setChainId(chain).setEL(
                "IF(AND(node(\"handleBooleanA\").tag(\"b\"),node(\"handleBooleanA\").tag(\"c\"),node(\"handleBooleanA\").tag(\"e\"),node(\"handleBooleanB\").tag(\"f\")),a,b);"
        ).build();
    }
}
