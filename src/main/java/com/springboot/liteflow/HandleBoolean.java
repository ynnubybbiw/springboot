package com.springboot.liteflow;

import com.springboot.bean.Book;
import com.yomahub.liteflow.core.NodeBooleanComponent;
import com.yomahub.liteflow.slot.Slot;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

/**
 * @author 29678
 *
 * 判断结果是否正确，正确则跳出循环
 */
@Component("handleBoolean")
public class HandleBoolean extends NodeBooleanComponent {

    public static final Logger logger = LogManager.getLogger(HandleBoolean.class.getName());

    @Override
    public boolean processBoolean() throws Exception {

        Slot slot = this.getSlot();
        Book requestData = slot.getRequestData();

        Integer count = requestData.getCount();

        logger.info("进入循环组件，{}", requestData);

        if (count > 10) {
            return false;
        }

        slot.setRequestData(requestData);

        Thread.sleep(5*1000);

        return true;
    }
}
