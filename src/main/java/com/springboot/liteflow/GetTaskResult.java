package com.springboot.liteflow;

import com.springboot.bean.Book;
import com.yomahub.liteflow.core.NodeComponent;
import com.yomahub.liteflow.slot.Slot;
import org.springframework.stereotype.Component;

/**
 * 获取结果组件 可根据任务id获取结果
 * @author 29678
 */

@Component("getTaskResult")
public class GetTaskResult extends NodeComponent {
    @Override
    public void process() throws Exception {

        Slot slot = this.getSlot();
        Book requestData = slot.getRequestData();

        Integer count = requestData.getCount();

        count++;

        requestData.setCount(count);

        slot.setRequestData(requestData);

    }
}
