package com.springboot.liteflow;

import com.yomahub.liteflow.annotation.LiteflowMethod;
import com.yomahub.liteflow.core.NodeComponent;
import com.yomahub.liteflow.enums.LiteFlowMethodEnum;
import com.yomahub.liteflow.enums.NodeTypeEnum;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * b&c&e&f     拿到表达式后，查询各个条件，根据lable_condition 对组件 进行编排
 * 比如编排为   IF(AND(node("equal").tag("b"),node("equal").tag(c),node("contain").tag("e"),node("eq").tag("f")),a,b);
 * @author 29678
 */
//@LiteflowComponent
public class CmpConfig {

    public static final Logger logger = LogManager.getLogger(CmpConfig.class.getName());

    @LiteflowMethod(value = LiteFlowMethodEnum.PROCESS_BOOLEAN, nodeId = "equalCmp",  nodeType = NodeTypeEnum.BOOLEAN)
    public boolean processEqual(NodeComponent bindCmp) throws InterruptedException {
        String tag = bindCmp.getTag();

        logger.info("checkoutEqual:{}",tag);
        Thread.sleep(2000L);
        return true;
    }

    @LiteflowMethod(value = LiteFlowMethodEnum.PROCESS_BOOLEAN, nodeId = "containCmp", nodeType = NodeTypeEnum.BOOLEAN)
    public boolean processContain(NodeComponent bindCmp) throws InterruptedException {
        String tag = bindCmp.getTag();
        Thread.sleep(2000L);
        logger.info("checkoutContain:{}",tag);
        return true;
    }

    @LiteflowMethod(value = LiteFlowMethodEnum.PROCESS_BOOLEAN, nodeId = "notContainCmp", nodeType = NodeTypeEnum.BOOLEAN)
    public boolean processNotContain(NodeComponent bindCmp) throws InterruptedException {
        String tag = bindCmp.getTag();
        Thread.sleep(2000L);
        logger.info("checkoutNotContain:{}",tag);
        return true;
    }

    @LiteflowMethod(value = LiteFlowMethodEnum.PROCESS_BOOLEAN, nodeId = "eqCmp", nodeType = NodeTypeEnum.BOOLEAN)
    public boolean processEq(NodeComponent bindCmp) throws InterruptedException {
        String tag = bindCmp.getTag();
        Thread.sleep(2000L);
        logger.info("checkoutEq:{}",tag);
        return true;
    }

    @LiteflowMethod(value = LiteFlowMethodEnum.PROCESS_BOOLEAN, nodeId = "neCmp", nodeType = NodeTypeEnum.BOOLEAN)
    public boolean processNe(NodeComponent bindCmp) throws InterruptedException {
        String tag = bindCmp.getTag();
        Thread.sleep(2000L);
        logger.info("checkoutNe:{}",tag);
        return true;
    }

    @LiteflowMethod(value = LiteFlowMethodEnum.PROCESS_BOOLEAN, nodeId = "likeCmp", nodeType = NodeTypeEnum.BOOLEAN)
    public boolean processLike(NodeComponent bindCmp) throws InterruptedException {
        String tag = bindCmp.getTag();
        Thread.sleep(2000L);
        logger.info("checkoutLike:{}",tag);
        return true;
    }

    @LiteflowMethod(value = LiteFlowMethodEnum.PROCESS_BOOLEAN, nodeId = "notLikeCmp", nodeType = NodeTypeEnum.BOOLEAN)
    public boolean processNotLike(NodeComponent bindCmp) throws InterruptedException {
        String tag = bindCmp.getTag();
        Thread.sleep(2000L);
        logger.info("checkoutNotLike:{}",tag);
        return true;
    }

}
