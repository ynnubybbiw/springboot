package com.springboot.activiti;

import com.google.common.collect.Lists;
import com.springboot.util.StreamUtil;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.ProcessDefinition;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Component
@Order(value = 1)
public class InitProcess  implements CommandLineRunner {

    private static final Logger logger = LoggerFactory.getLogger(InitProcess.class);

    @Autowired
    private RepositoryService repositoryService;

    @Override
    public void run(String... args) throws Exception {

        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();

        executor.execute(() -> {


            InputStream fileInputStream = null;

            try {

                List<String> fileList = getFile();

                for(String file : fileList){
                    String xmlFileName =  file.replace("/processes/","");
                    String xmlname = xmlFileName.replace(".bpmn20.xml","");

                    /*WorkflowDefinitionE workflowDefinitionE = workflowDefinitionService.findByFileName(xmlFileName);
                    if(null != workflowDefinitionE){
                        continue;
                    }*/

                    logger.info("start read file ");
                    fileInputStream = InitProcess.class.getResourceAsStream(file);

                    Assert.notNull(fileInputStream,"读取流失败！");
                    byte[] bpmnBytes = StreamUtil.toByteArray(fileInputStream);
                    Deployment deployment = repositoryService.createDeployment().name(xmlname).addString(xmlFileName, new String(bpmnBytes, StandardCharsets.UTF_8)).deploy();
                    List<ProcessDefinition> list = repositoryService.createProcessDefinitionQuery().deploymentId(deployment.getId()).orderByDeploymentId().desc().list();
                    if(null != list && !list.isEmpty()){
                        ProcessDefinition processDefinition = list.get(0);
                        String key = processDefinition.getKey();
                        logger.info("processDefinition key：{}",key);
                        /*workflowDefinitionE = new WorkflowDefinitionE();
                        workflowDefinitionE.setCreateTime(new Date());
                        workflowDefinitionE.setUpdateTime(new Date());
                        workflowDefinitionE.setCreateUser("admin");
                        workflowDefinitionE.setUpdateUser("admin");
                        workflowDefinitionE.setDescription(JSON.parseObject(getDetail().getString(key)).getString("description"));
                        workflowDefinitionE.setIdentification(JSON.parseObject(getDetail().getString(key)).getString("identification"));
                        workflowDefinitionE.setName(JSON.parseObject(getDetail().getString(key)).getString("name"));
                        workflowDefinitionE.setProcessType(ApplicationType.valueOf(JSON.parseObject(getDetail().getString(key)).getString("processType")));
                        workflowDefinitionE.setProcessDefinitionId(processDefinition.getId());
                        workflowDefinitionE.setFileName(xmlFileName);
                        workflowDefinitionE.setId(JSON.parseObject(getDetail().getString(key)).getString("id"));
                        workflowDefinitionE.setDelete(false);
                        workflowDefinitionService.save(workflowDefinitionE);*/
                    }

                }
            } catch (Exception e) {
                logger.error("InitProcess error：",e);
            }finally {
                if(null != fileInputStream){
                    try {
                        fileInputStream.close();
                    } catch (IOException e) {
                        logger.error("error :",e);
                    }
                }
            }
        });

    }

    private List<String> getFile(){
        List<String> fileList = Lists.newArrayList();
        fileList.add("/bpmn/quatoapprovalprocess.bpmn20.xml");
        return fileList;
    }

}
