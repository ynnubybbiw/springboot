package com.springboot.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.CustomExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * rabbitmq 延时队列配置   通过插件实现   rabbitmq_delayed_message_exchange
 * @author dubin
 */
@Configuration
public class RabbitmqDelayedConfig {

    public static final String QUEUE_NAME = "delayed.alarm.msg";
    public static final String EXCHANGE_NAME = "delayed_alarm_msg";
    @Bean
    public Queue queue() {
        return new Queue(RabbitmqDelayedConfig.QUEUE_NAME);
    }

    // 配置默认的交换机
    @Bean
    public CustomExchange customExchange() {
        Map<String, Object> args = new HashMap<>();
        args.put("x-delayed-type", "direct");
        //参数二为类型：必须是x-delayed-message
        return new CustomExchange(RabbitmqDelayedConfig.EXCHANGE_NAME, "x-delayed-message", true, false, args);
    }
    // 绑定队列到交换器
    @Bean
    public Binding binding(Queue queue, CustomExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(RabbitmqDelayedConfig.QUEUE_NAME).noargs();
    }

}
