package com.springboot.config;

import com.google.common.collect.Multimap;
import com.springboot.bean.MinioParam;
import io.minio.CreateMultipartUploadResponse;
import io.minio.ListPartsResponse;
import io.minio.MinioClient;
import io.minio.ObjectWriteResponse;
import io.minio.errors.*;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * 自定义minio
 */
@Component
public class CustomizeMinioClient extends MinioClient {

    public CustomizeMinioClient(MinioClient client) {
        super(client);
    }

    /**
     * 创建分片上传请求   uploadId  uploadUrls
     *
     * @param bucketName       存储桶
     * @param region           区域
     * @param objectName       对象名
     * @param headers          消息头
     * @param extraQueryParams 额外查询参数
     */
    @Override
    public CreateMultipartUploadResponse createMultipartUpload(String bucketName, String region, String objectName, Multimap<String, String> headers, Multimap<String, String> extraQueryParams) throws NoSuchAlgorithmException, InsufficientDataException, IOException, XmlParserException, ErrorResponseException, InvalidResponseException, ServerException, InvalidKeyException, InternalException {
        return super.createMultipartUpload(bucketName, region, objectName, headers, extraQueryParams);
    }

    /**
     * 完成分片上传，执行合并文件
     * @param minioParam
     *  bucketName       存储桶
     *  region           区域
     *  objectName       对象名
     *  uploadId         上传ID
     *  parts            分片
     *  headers     额外消息头
     *  extraQueryParams 额外查询参数
     */
    public ObjectWriteResponse completeMultipartUpload(MinioParam minioParam) throws NoSuchAlgorithmException, InsufficientDataException, IOException, XmlParserException, ErrorResponseException, InvalidResponseException, ServerException, InvalidKeyException, InternalException {
        return super.completeMultipartUpload(minioParam.getBucketName(), minioParam.getRegion(), minioParam.getObjectName(), minioParam.getUploadId(), minioParam.getParts(), minioParam.getHeaders(), minioParam.getExtraQueryParams());
    }

    /**
     * 查询分片数据
     *
     * @param bucketName       存储桶
     * @param region           区域
     * @param objectName       对象名
     * @param maxParts         最大分片数
     * @param partNumberMarker  分片起始数
     * @param uploadId         上传ID
     * @param extraHeaders     额外消息头
     * @param extraQueryParams 额外查询参数
     */
    @Override
    public ListPartsResponse listParts(String bucketName, String region, String objectName, Integer maxParts, Integer partNumberMarker, String uploadId, Multimap<String, String> extraHeaders, Multimap<String, String> extraQueryParams) throws NoSuchAlgorithmException, InsufficientDataException, IOException, ServerException, ErrorResponseException, InvalidKeyException, XmlParserException, InvalidResponseException, InternalException {
        return super.listParts(bucketName, region, objectName, maxParts, partNumberMarker, uploadId, extraHeaders, extraQueryParams);
    }
}
