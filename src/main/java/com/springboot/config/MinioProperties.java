package com.springboot.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "minio")
public class MinioProperties {

    private String endpoint;

    private String accessKey;

    private String secretKey;

    private String bucket;

    //生成的上传url过期时间  天
    private Integer expiryday;

    //数据库上传记录是否超过此时间  天
    private Integer overflowExpiryday;

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public String getBucket() {
        return bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public Integer getExpiryday() {
        return expiryday;
    }

    public void setExpiryday(Integer expiryday) {
        this.expiryday = expiryday;
    }

    public Integer getOverflowExpiryday() {
        return overflowExpiryday;
    }

    public void setOverflowExpiryday(Integer overflowExpiryday) {
        this.overflowExpiryday = overflowExpiryday;
    }
}
