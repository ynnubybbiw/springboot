package com.springboot.config;

import com.springboot.service.impl.UserDetailServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 *
 * 请求登陆接口地址   localhost:8081/testLogin
 *
 * 此类用于 Spring Security 登陆接口配置
 *
 * 1.需要 UserDetailServiceImpl  类实现 UserDetailsService   如果请求接口进来，会调用  loadUserByUsername 方法获取数据库用户信息   密码必需是加密的
 *
 * 2.获取到用户信息后，会进入 DaoAuthenticationProvider  的方法 additionalAuthenticationChecks   进行用户名密码验证
 *
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    public static final Logger logger = LogManager.getLogger(SecurityConfig.class.getName());

    @Autowired
    private CustomAuthenticationSuccessHandler customAuthenticationSuccessHandler;
    @Autowired
    private CustomAuthenticationFailureHandler customAuthenticationFailureHandler;
    @Autowired
    private UserDetailServiceImpl userDetailService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        logger.info("进入配置方法");
        http.formLogin()
                .loginProcessingUrl("/testLogin")
                .usernameParameter("userName")
                .passwordParameter("password")
                .successHandler(customAuthenticationSuccessHandler)
                .failureHandler(customAuthenticationFailureHandler)
                //.permitAll()
                .and().httpBasic().and().csrf().disable();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailService).passwordEncoder(new BCryptPasswordEncoder());
        // 配置用户名密码，这里采用内存方式，生产环境需要从数据库获取

        /*BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        auth.inMemoryAuthentication()
                .withUser("haolb")
                .password(passwordEncoder.encode("123"))
                .roles("USER");*/
    }
}
