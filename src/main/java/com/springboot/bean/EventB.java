package com.springboot.bean;

import lombok.Data;

@Data
public class EventB {

    private String name;

    private long timestamp;

}
