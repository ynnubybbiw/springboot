package com.springboot.bean;

import java.util.Collections;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class TreeNodeNew {

    private final int id;
    private final String name;
    private final Set<TreeNodeNew> children;  // 优化2: 使用Set代替List避免重复
    private final Set<TreeNodeNew> parents;

    private TreeNodeNew(Builder builder) {
        this.id = builder.id;
        this.name = builder.name;
        this.children = Collections.newSetFromMap(new ConcurrentHashMap<>());
        this.parents = Collections.newSetFromMap(new ConcurrentHashMap<>());
    }

    public static class Builder {
        private final int id;
        private final String name;

        public Builder(int id, String name) {
            this.id = id;
            this.name = name;
        }

        public TreeNodeNew build() {
            return new TreeNodeNew(this);
        }
    }

    public void addChild(TreeNodeNew child) {
        children.add(child);
        child.parents.add(this);
    }

    public Set<TreeNodeNew> getChildren() {
        return children;
    }

    public Set<TreeNodeNew> getParents() {
        return parents;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return id + ":" + name;
    }

    // 优化3: 添加equals和hashCode方法以支持Set操作
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TreeNodeNew treeNode = (TreeNodeNew) o;
        return id == treeNode.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
