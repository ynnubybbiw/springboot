package com.springboot.bean;

public class Singleton {

    private Singleton(){}

    private static class SingletonIn{
        private static final Singleton SINGLETON = new Singleton();
    }

    public static Singleton getInstance(){
        return SingletonIn.SINGLETON;
    }
}
