package com.springboot.bean;


public class Person {

    private final String name;
    private final int age;
    private final String phone;
    private final String addtrss;
    private final String sex;

    public Person(PersonBuilder builder) {
        this.name = builder.getName();
        this.age = builder.getAge();
        this.phone = builder.getPhone();
        this.addtrss = builder.getAddtrss();
        this.sex = builder.getSex();
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddtrss() {
        return addtrss;
    }

    public String getSex() {
        return sex;
    }
}
