package com.springboot.bean;

public class PersonBuilder {

    private String name;
    private int age;
    private String phone;
    private String addtrss;
    private String sex;

    public Person build(){
        return new Person(this);
    }

    public String getName() {
        return name;
    }

    public PersonBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public int getAge() {
        return age;
    }

    public PersonBuilder setAge(int age) {
        this.age = age;
        return this;
    }

    public String getPhone() {
        return phone;
    }

    public PersonBuilder setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public String getAddtrss() {
        return addtrss;
    }

    public PersonBuilder setAddtrss(String addtrss) {
        this.addtrss = addtrss;
        return this;
    }

    public String getSex() {
        return sex;
    }

    public PersonBuilder setSex(String sex) {
        this.sex = sex;
        return this;
    }
}
