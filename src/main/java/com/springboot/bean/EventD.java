package com.springboot.bean;

import lombok.Data;

@Data
public class EventD {

    private String resourceId;

    private String alarmName;

    private long timestamp;
}
