package com.springboot.bean;

import lombok.Data;

@Data
public class EventA {

    private String name;

    private long timestamp;
}
