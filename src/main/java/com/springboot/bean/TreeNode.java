package com.springboot.bean;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;


@Data
public class TreeNode {
    private int value;
    private List<TreeNode> children;  // 表示节点的子节点（分支）

    public TreeNode(int value) {
        this.value = value;
        this.children = new ArrayList<>();
    }

    // 打印树结构的辅助方法
    public void printTree(String prefix) {
        System.out.println(prefix + value);
        for (TreeNode child : children) {
            child.printTree(prefix + "→ ");
        }
    }
}
