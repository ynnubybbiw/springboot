package com.springboot.bean;

/**
 * 利用建造者模式，将传入的参数，用对象封装成一个list返回
 */
public class QuotaBuilder {

    private Integer cpu;
    private Integer gpu;
    private Integer disk;
    private Integer vmNum;
    private Integer ram;
    private String tenantId;


    public Quota build(){
        return new Quota(this);
    }

    public QuotaBuilder setCpu(Integer cpu) {
        this.cpu = cpu;
        return this;
    }

    public QuotaBuilder setGpu(Integer gpu) {
        this.gpu = gpu;
        return this;
    }

    public QuotaBuilder setDisk(Integer disk) {
        this.disk = disk;
        return this;
    }

    public QuotaBuilder setVmNum(Integer vmNum) {
        this.vmNum = vmNum;
        return this;
    }

    public QuotaBuilder setRam(Integer ram) {
        this.ram = ram;
        return this;
    }

    public QuotaBuilder setTenantId(String tenantId) {
        this.tenantId = tenantId;
        return this;
    }

    public Integer getVmNum() {
        return vmNum;
    }

    public Integer getCpu() {
        return cpu;
    }

    public Integer getGpu() {
        return gpu;
    }

    public Integer getDisk() {
        return disk;
    }

    public Integer getRam() {
        return ram;
    }

    public String getTenantId() {
        return tenantId;
    }
}
