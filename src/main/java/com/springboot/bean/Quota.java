package com.springboot.bean;

import com.google.common.collect.Lists;

import java.util.List;

public class Quota{

    public static final String CPU_CODE = "QUOTA_VCPU";
    public static final String GPU_CODE = "QUOTA_VGPU";
    public static final String RAM_CODE = "QUOTA_RAM";
    public static final String VM_NUM_CODE = "QUOTA_CLOUD_HOST";
    public static final String DISK_CODE = "QUOTA_CLOUD_DISK";

    private final List<Asset> list = Lists.newArrayList();

    public Quota(QuotaBuilder quotaBuilder){
        list.add(new Asset(CPU_CODE, quotaBuilder.getCpu(), quotaBuilder.getTenantId()));
        list.add(new Asset(GPU_CODE, quotaBuilder.getGpu(), quotaBuilder.getTenantId()));
        list.add(new Asset(RAM_CODE, quotaBuilder.getRam(), quotaBuilder.getTenantId()));
        list.add(new Asset(VM_NUM_CODE, quotaBuilder.getVmNum(), quotaBuilder.getTenantId()));
        list.add(new Asset(DISK_CODE, quotaBuilder.getDisk(), quotaBuilder.getTenantId()));

    }

    public List<Asset> getList() {
        return list;
    }

    static class Asset{
        private Integer num;
        private String code;
        private String tenantId;

        public Asset() {
        }

        public Asset(String code, Integer num, String tenantId) {
            this.num = num;
            this.code = code;
            this.tenantId = tenantId;
        }

        public Integer getNum() {
            return num;
        }

        public void setNum(Integer num) {
            this.num = num;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getTenantId() {
            return tenantId;
        }

        public void setTenantId(String tenantId) {
            this.tenantId = tenantId;
        }
    }
}
