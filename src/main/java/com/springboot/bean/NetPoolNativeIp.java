package com.springboot.bean;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
public class NetPoolNativeIp {

    @TableId(type = IdType.ASSIGN_UUID)
    private String id;

    private String netpoolid;

    private String platformId;

    private String ip;

    private String status;

    private Boolean sync;

    private int expire;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
}
