package com.springboot.bean;

import lombok.Data;

@Data
public class Alarm {
    private String alarmName;
    private String alarmId;
    private String resource;

    public Alarm(String alarmName, String alarmId, String resource) {
        this.alarmName = alarmName;
        this.alarmId = alarmId;
        this.resource = resource;
    }

    public String getAlarmName() {
        return alarmName;
    }
}
