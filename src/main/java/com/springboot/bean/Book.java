package com.springboot.bean;

public class Book {

    public static final String TAG = "BookTag";

    private String name;
    private String author;
    private String test;

    private String data;

    private Integer count = 1;

    @Override
    public String toString() {
        return "Book{" +
                "name='" + name + '\'' +
                ", author='" + author + '\'' +
                ", test='" + test + '\'' +
                ", data='" + data + '\'' +
                ", count=" + count +
                '}';
    }

    public Book() {
    }

    public Book(String name, String author) {
        this.name = name;
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTest() {
        return test;
    }

    public void setTest(String test) {
        this.test = test;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    private String testMethod(int index) {
        String string = null;
        switch (index) {
            case 1:
                string = "I am declaredMethod 1 !";
                break;
            case 2:
                string = "I am declaredMethod 2 !";
                break;
            default:
                string = "I am declaredMethod 1000 !";
        }

        return string;
    }
}
