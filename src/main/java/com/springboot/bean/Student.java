package com.springboot.bean;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class Student {

    @NotEmpty(message = "姓名不能为空")
    private String name;
    @NotNull(message = "年龄不能为空")
    private Integer age;
    @NotEmpty(message = "电话不能为空")
    private String phone;
    private String addtrss;
    @NotEmpty(message = "性别不能为空")
    private String sex;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddtrss() {
        return addtrss;
    }

    public void setAddtrss(String addtrss) {
        this.addtrss = addtrss;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}
