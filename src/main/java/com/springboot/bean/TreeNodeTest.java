package com.springboot.bean;

import java.util.ArrayList;
import java.util.List;

public class TreeNodeTest {
    private int id;
    private String name;
    private List<TreeNodeTest> children;
    private List<TreeNodeTest> parents;  // 添加parents列表以存储上游节点

    public TreeNodeTest(int id, String name) {
        this.id = id;
        this.name = name;
        this.children = new ArrayList<>();
        this.parents = new ArrayList<>();
    }

    public void addChild(TreeNodeTest child) {
        children.add(child);
        child.parents.add(this);
    }

    public List<TreeNodeTest> getChildren() {
        return children;
    }

    public List<TreeNodeTest> getParents() {
        return parents;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return id + ":" + name;
    }
}
