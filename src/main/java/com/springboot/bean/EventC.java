package com.springboot.bean;

import lombok.Data;

@Data
public class EventC {

    private String name;

    private long timestamp;

}
