package com.springboot.bean;

import org.apache.commons.compress.utils.Lists;

import java.util.List;

/**
 * @author 29678
 */
@lombok.Data
public class Data {

    //刀片
    private List<String> bladeNodeList = Lists.newArrayList();

    //刀箱
    private List<String> boxNodeList = Lists.newArrayList();

    //机柜
    private List<String> cabinetNodeList = Lists.newArrayList();


    //目的节点
    private List<String> targetNodeList = Lists.newArrayList();

    //源节点列表
    //private List<String> sourceNodeList = Lists.newArrayList();

    public Data() {

    }

    /**
     * 节点名
     */
    private String nodeName;

    /**
     * 产生时间
     */
    private long timestamp; // 事件的时间戳

    public Data(String nodeName, long timestamp) {
        this.timestamp = timestamp;
        this.nodeName = nodeName;
    }
}
