package com.springboot.bean;

import com.springboot.aspect.Desensitization;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;
import java.util.Objects;

/**
 * @author 29678
 */
@Data
@AllArgsConstructor
public class User {

    private Integer id;
    private String userName;
    private Integer age;

    @Desensitization(type = DesensitizationTypeEnum.PASSWORD)
    private String userPassword;

    @Desensitization(type = DesensitizationTypeEnum.MY_RULE, startInclude = 0, endExclude = 2)
    private String address;

    @Desensitization(type = DesensitizationTypeEnum.MOBILE_PHONE)
    private String phone;

    private Date createTime;
    private Date updateTime;

    public User(){}

    public User(String userName, Integer age){
        this.userName = userName;
        this.age = age;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        User user = (User) obj;
        return Objects.equals(id, user.id);
    }
}
