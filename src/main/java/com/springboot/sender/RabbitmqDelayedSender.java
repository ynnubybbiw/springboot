package com.springboot.sender;

import com.springboot.config.RabbitmqDelayedConfig;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 延时消息发送
 */
@Component
public class RabbitmqDelayedSender {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 发送延时消息
     * @param msg
     * @param msTime
     */
    public void send(String msg, String msTime) {

        rabbitTemplate.convertAndSend(RabbitmqDelayedConfig.EXCHANGE_NAME, RabbitmqDelayedConfig.QUEUE_NAME, msg, message -> {
            message.getMessageProperties().setHeader("x-delay", String.valueOf(msTime));
            return message;
        });
    }


}
