package com.springboot.queue;

import com.alibaba.fastjson.JSON;
import com.springboot.bean.Menu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import java.util.concurrent.ArrayBlockingQueue;

/**
 * 阻塞队列使用
 * @author 29678
 */
@Component
public class QueueBlocking {

    private static final Logger logger = LogManager.getLogger(QueueBlocking.class.getName());

    private final ArrayBlockingQueue<Menu> menuBlockingQueue = new ArrayBlockingQueue<>(1024);

    public void produce(Menu menu) {
        String jsonString = JSON.toJSONString(menu);
        logger.info("start queue : {}", jsonString);
        menuBlockingQueue.add(menu);
    }

    public Menu consume() {
        return menuBlockingQueue.poll();
    }

}
