package com.springboot.tree;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class TreeCausalNode {

    private String nodeId;
    private String nodeName;
    private String parentId;
    private List<TreeCausalNode> children;

    // 默认构造函数
    public TreeCausalNode() {
        this.children = new ArrayList<>();
    }

    // Getter 和 Setter 方法
    public String getNodeId() { return nodeId; }
    public void setNodeId(String nodeId) { this.nodeId = nodeId; }

    public String getNodeName() { return nodeName; }
    public void setNodeName(String nodeName) { this.nodeName = nodeName; }

    public String getParentId() { return parentId; }
    public void setParentId(String parentId) { this.parentId = parentId; }

    public List<TreeCausalNode> getChildren() { return children; }
    public void setChildren(List<TreeCausalNode> children) { this.children = children; }

    public void addChild(TreeCausalNode child) {
        if (this.children == null) {
            this.children = new ArrayList<>();
        }
        this.children.add(child);
    }

    // Builder 内部类
    public static class Builder {
        private String nodeId;
        private String nodeName;
        private String parentId;

        public Builder(String nodeId, String nodeName) {
            this.nodeId = nodeId;
            this.nodeName = nodeName;
        }

        public Builder parentId(String parentId) {
            this.parentId = parentId;
            return this;
        }

        public TreeCausalNode build() {
            TreeCausalNode node = new TreeCausalNode();
            node.nodeId = this.nodeId;
            node.nodeName = this.nodeName;
            node.parentId = this.parentId;
            node.children = new ArrayList<>();
            return node;
        }
    }
}
