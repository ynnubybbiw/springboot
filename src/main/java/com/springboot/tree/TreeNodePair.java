package com.springboot.tree;

import lombok.Data;

@Data
public class TreeNodePair {

    TreeCausalNode original;  // 原始节点
    TreeCausalNode copy;      // 副本节点

    TreeNodePair(TreeCausalNode original, TreeCausalNode copy) {
        this.original = original;
        this.copy = copy;
    }

}
