package com.springboot.tree;

import java.util.*;

/**
 * @author 296787
 */
public class TreeCausalNodeHandle {

    public TreeCausalNode getDownstreamEffectTree(Map<String, TreeCausalNode> nodeMap, String targetId, List<String> nodeIdList) {
        TreeCausalNode source = nodeMap.get(targetId);
        if (source == null) {
            return null;
        }

        // 根节点不需要parentId
        TreeCausalNode rootNode = new TreeCausalNode.Builder(targetId, source.getNodeName()).build();

        // 记录已访问的节点ID
        Set<String> visited = new HashSet<>();

        // 使用队列进行广度优先遍历
        Deque<TreeNodePair> stack = new ArrayDeque<>();
        stack.push(new TreeNodePair(source, rootNode));

        // 记录每个节点可能有的多个父节点ID
        Map<String, Set<String>> nodeToParents = new HashMap<>();

        while (!stack.isEmpty()) {
            TreeNodePair current = stack.pop();
            String currentId = current.original.getNodeId();

            // 如果这个节点已经处理过，只需更新其父节点集合，然后继续下一个
            if (!visited.add(currentId)) {
                // 如果节点有多个父节点，需要记录所有父节点ID
                if (current.copy.getParentId() != null) {
                    nodeToParents.computeIfAbsent(currentId, k -> new HashSet<>())
                            .add(current.copy.getParentId());
                }
                continue;
            }

            nodeIdList.add(currentId);

            // 初始化这个节点的父节点集合
            if (current.copy.getParentId() != null) {
                nodeToParents.computeIfAbsent(currentId, k -> new HashSet<>())
                        .add(current.copy.getParentId());
            }

            // 处理子节点
            for (TreeCausalNode child : current.original.getChildren()) {
                String childId = child.getNodeId();

                // 为子节点创建副本
                TreeCausalNode childCopy = new TreeCausalNode.Builder(childId, child.getNodeName())
                        .parentId(current.copy.getNodeId())  // 设置当前父节点ID
                        .build();

                current.copy.addChild(childCopy);

                // 继续处理子节点
                stack.push(new TreeNodePair(child, childCopy));
            }
        }

        // 可以在这里处理多父节点的情况
        // 例如，可以在TreeCausalNode类中添加一个parentIds字段来存储所有父节点ID
        for (Map.Entry<String, Set<String>> entry : nodeToParents.entrySet()) {
            String nodeId = entry.getKey();
            Set<String> parentIds = entry.getValue();

            if (parentIds.size() > 1) {
                // 在这里处理多父节点情况
                // 例如：findNodeInTree(rootNode, nodeId).setParentIds(parentIds);
                System.out.println("Node " + nodeId + " has multiple parents: " + parentIds);
            }
        }

        return rootNode;
    }

    // 查找树中的节点
    private TreeCausalNode findNodeInTree(TreeCausalNode root, String nodeId) {
        if (root.getNodeId().equals(nodeId)) {
            return root;
        }

        for (TreeCausalNode child : root.getChildren()) {
            TreeCausalNode found = findNodeInTree(child, nodeId);
            if (found != null) {
                return found;
            }
        }

        return null;
    }

}
