package com.springboot.exception;

public class TestException extends RuntimeException{

    private final int code;

    public TestException(int code, String message) {
        super(message);
        this.code = code;
    }

    public TestException(String message) {
        super(message);
        this.code = 500;
    }

    public int getCode() {
        return code;
    }

}
