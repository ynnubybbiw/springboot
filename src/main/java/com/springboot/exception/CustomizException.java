package com.springboot.exception;

public class CustomizException extends RuntimeException{
    private final int code;

    public CustomizException(int code, String message) {
        super(message);
        this.code = code;
    }

    public CustomizException(String message) {
        super(message);
        this.code = 500;
    }

    public int getCode() {
        return code;
    }

}
