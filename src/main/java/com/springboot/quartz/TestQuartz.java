package com.springboot.quartz;

import com.springboot.service.impl.UserServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import java.util.Date;

public class TestQuartz {

    private static final Logger logger = LogManager.getLogger(UserServiceImpl.class.getName());

    /*@Autowired
    private QuartzManager quartzManager;*/

    private final Long TIME_START = 2*1000L;

    @Test
    public void test1(){
        QuartzManager quartzManager = new QuartzManager();
        quartzManager.startJobs();
        Date oldDate = new Date();
        long startTime = oldDate.getTime()+TIME_START;
        Date startDate = new Date(startTime);
        quartzManager.addJob("test1",MyJob.class,startDate);
        logger.info("。。。。。。");
    }
}
