package com.springboot.proxy;

import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class ProxyHandler implements InvocationHandler {

    private static final Log log = LogFactory.get();

    private Object target;

    public Object bind(Object target){
        this.target = target;
        return Proxy.newProxyInstance(
                target.getClass().getClassLoader(),
                target.getClass().getInterfaces(),
                this
        );
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        long start = System.currentTimeMillis();
        Object invoke = method.invoke(target, args);
        long end = System.currentTimeMillis();

        log.debug("执行方法用时：{}",end-start);

        return invoke;
    }
}
