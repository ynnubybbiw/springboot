package com.springboot.proxy;

public interface Subject {

    void doSomethingOne(String arg);

    void doSomethingtwo(String arg);

    void doSomethingthree(String arg);
}
