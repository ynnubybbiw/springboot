package com.springboot.proxy;

import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;

public class RealSubject implements Subject{

    private static final Log log = LogFactory.get();

    @Override
    public void doSomethingOne(String arg) {
        log.debug("doSomethingOne: {}",arg);
    }

    @Override
    public void doSomethingtwo(String arg) {
        log.debug("doSomethingtwo: {}",arg);
    }

    @Override
    public void doSomethingthree(String arg) {
        log.debug("doSomethingthree: {}",arg);
    }
}
