package com.springboot.proxy;

/**
 * jdk动态代理 测试
 */
public class TestProxy {

    public static void main(String[] args) {

        ProxyHandler proxyHandler = new ProxyHandler();

        Subject subject = (Subject)proxyHandler.bind(new RealSubject());

        subject.doSomethingOne("one");
    }
}
