package com.springboot.listener;

import com.rabbitmq.client.Channel;
import com.springboot.config.RabbitmqDelayedConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;

/**
 * 延时队列监听
 */
@Component
@RabbitListener(queues = RabbitmqDelayedConfig.QUEUE_NAME)
public class RabbitmqDelayed {

    private static final Logger logger = LoggerFactory.getLogger(RabbitmqDelayed.class);


    @RabbitHandler
    public void process(@Payload String alarmMsg, Channel channel, @Headers Map<String,Object> headers) {
        long deliveryTag = (Long)headers.get(AmqpHeaders.DELIVERY_TAG);
        try {
            logger.info("接收到的消息内容：{}",alarmMsg);
        } catch (Exception e) {
            logger.error("处理延时消息失败：",e);
        } finally {
            try {
                channel.basicAck(deliveryTag,true);
            } catch (IOException e) {
                logger.error("手动标记失败！",e);
            }
        }
    }



}
