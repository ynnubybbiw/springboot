package com.springboot.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.metadata.data.ReadCellData;
import com.alibaba.excel.read.listener.ReadListener;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;

public class ExcelListener implements ReadListener<Object> {

    private static final Logger logger = LoggerFactory.getLogger(ExcelListener.class);

    private List<JSONObject> dataList = Lists.newArrayList();

    private Map<Integer, String> importHeads = Maps.newLinkedHashMap();

    @Override
    public void invokeHead(Map<Integer, ReadCellData<?>> headMap, AnalysisContext context) {
        headMap.forEach((k,v)-> importHeads.put(k,v.getStringValue()));
    }

    @Override
    public void invoke(Object data, AnalysisContext analysisContext) {
        String headStr = JSON.toJSONString(data);
        if(!"{}".equals(headStr)){
            dataList.add(JSON.parseObject(headStr));
        }
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {
        logger.info("读取完成!");
    }

    public List<JSONObject> getDataList() {
        return dataList;
    }

    public void setDataList(List<JSONObject> dataList) {
        this.dataList = dataList;
    }

    public Map<Integer, String> getImportHeads() {
        return importHeads;
    }

    public void setImportHeads(Map<Integer, String> importHeads) {
        this.importHeads = importHeads;
    }
}
