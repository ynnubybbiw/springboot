package com.springboot.listener.serviceimpl;

import com.alibaba.fastjson.JSONObject;
import com.springboot.listener.service.ExecutionService;
import com.springboot.listener.service.ExecutionServiceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;

@Service
public class OneServiceImpl implements ExecutionService, InitializingBean {

    private static final Logger logger = LoggerFactory.getLogger(OneServiceImpl.class);

    @Override
    public void execution(JSONObject jsonObject) {
        logger.info("进入OneServiceImpl ： {}", jsonObject);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        ExecutionServiceFactory.register("oneServiceImpl",this);
    }
}
