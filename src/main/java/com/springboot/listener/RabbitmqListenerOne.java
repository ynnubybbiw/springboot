package com.springboot.listener;

import com.alibaba.fastjson.JSONObject;
import com.rabbitmq.client.Channel;
import com.springboot.listener.service.ExecutionService;
import com.springboot.listener.service.ExecutionServiceFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.*;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
@RabbitListener(bindings = @QueueBinding(exchange = @Exchange(value = "rabbitmq-test-exchange"),
        value = @Queue(value = "rabbitmq-test-queue", durable = "true"),
        key = "test.one"))
public class RabbitmqListenerOne {

    private static final Logger logger = LoggerFactory.getLogger(RabbitmqListenerOne.class);

    @RabbitHandler
    public void receive(@Payload String data, Channel channel, @Headers Map<String,Object> headers){
        long deliveryTag = (Long)headers.get(AmqpHeaders.DELIVERY_TAG);
        logger.info("接收到的rabbitmq消息：{}",data);
        try {
            //延迟手动应答时间    延迟接收 下一条消息
            //Thread.sleep(1000*60*5L);
            Thread.sleep(1000*5L);
            logger.info("手动应答：{}",deliveryTag);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("deliveryTag",deliveryTag);
            jsonObject.put("data",data);
            //根据不同的类型，进入不同的serviceimpl执行
            ExecutionService executionService = ExecutionServiceFactory.getExcutionServiceByType("twoServiceImpl");
            executionService.execution(jsonObject);

            channel.basicAck(deliveryTag,true);
        } catch (InterruptedException e) {
            logger.error("InterruptedException",e);
            Thread.currentThread().interrupt();
        } catch (Exception e){
            logger.error("异常：",e);
        }
    }

}
