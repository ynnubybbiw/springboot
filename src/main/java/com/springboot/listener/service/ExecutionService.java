package com.springboot.listener.service;

import com.alibaba.fastjson.JSONObject;

public interface ExecutionService {

    void execution(JSONObject jsonObject);

}
