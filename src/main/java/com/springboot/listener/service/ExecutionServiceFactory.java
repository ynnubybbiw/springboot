package com.springboot.listener.service;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 将执行的 service 根据类型 保存到 map ，并可以获取
 */
public class ExecutionServiceFactory {

    private static final Map<String,ExecutionService> SERVICES = new ConcurrentHashMap<>();

    public  static ExecutionService getExcutionServiceByType(String type){
        return SERVICES.get(type);
    }

    public static void register(String executionType,ExecutionService executionService){
        SERVICES.put(executionType,executionService);
    }
}
