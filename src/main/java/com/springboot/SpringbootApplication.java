package com.springboot;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;


/**
 * @EnableAutoConfiguration
 * @ComponentScan(basePackages = "com.springboot")    //需要手动设置扫描包的路径
 *
 * 这两个注解可以用 @SpringBootApplication 代替
 *
 * 支持 @Scheduled定时任务
 *
 *
 *
 * http://localhost:8081/user/testAsync
 */

@MapperScan(basePackages = {"com.springboot.mapper"} )
@EnableGlobalMethodSecurity(prePostEnabled=true)
@EnableScheduling
@SpringBootApplication
@EnableAsync
public class SpringbootApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootApplication.class, args);
    }
}
