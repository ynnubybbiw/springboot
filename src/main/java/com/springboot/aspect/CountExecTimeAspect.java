package com.springboot.aspect;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.Configuration;

/**
 * 切面，方法执行时间计算
 */
@Aspect
@Configuration
public class CountExecTimeAspect {

    private static final Logger logger = LogManager.getLogger(CountExecTimeAspect.class.getName());

    @Pointcut("@annotation(com.springboot.aspect.CountExecTime)")
    public void countExecTime(){}

    @Around(value = "countExecTime()")
    public Object exec(ProceedingJoinPoint proceedingJoinPoint){
        logger.info("执行计时，环绕通知的目标方法名：{}",proceedingJoinPoint.getSignature().getName());

        logger.info("执行方法前");
        Object obj = null;
        //执行被切方法  obj 这执行方法返回
        try {
            long start = System.currentTimeMillis();
            logger.info("执行方法开始时间：{}",start);
            obj = proceedingJoinPoint.proceed();
            long end = System.currentTimeMillis();
            logger.info("执行方法结束时间：{}", end);
            logger.info("执行方法时间：{}",end-start);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        return obj;
    }
}
