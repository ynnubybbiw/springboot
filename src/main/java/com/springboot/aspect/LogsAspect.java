package com.springboot.aspect;

import com.alibaba.fastjson.JSON;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

/**
 * @author 29678
 */
@Aspect
@Configuration
public class LogsAspect {

    private static final Logger logger = LogManager.getLogger(LogsAspect.class.getName());

    /**
     * 定义切入点，切入点为com.example.aop下的所有函数
     */
    //@Pointcut("execution(public * com.springboot..*.*(..))")
    @Pointcut("@annotation(com.springboot.aspect.Logs)")
    public void webLog(){}


    @Before("webLog()")
    public void doBefore(JoinPoint joinPoint){
        // 接收到请求，记录请求内容
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();

        // 记录下请求内容
        logger.info("URL : {}", request.getRequestURL());
        logger.info("HTTP_METHOD : {}", request.getMethod());
        logger.info("IP : {}", request.getRemoteAddr());
        logger.info("CLASS_METHOD : {}", joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName());
        logger.info("ARGS : {}", Arrays.toString(joinPoint.getArgs()));
    }

    /**
     * 环绕通知：
     *   环绕通知非常强大，可以决定目标方法是否执行，什么时候执行，执行时是否需要替换方法参数，执行完毕是否需要替换返回值。
     *   环绕通知第一个参数必须是org.aspectj.lang.ProceedingJoinPoint类型
     */
    @Around("webLog()")
    public Object doAroundAdvice(ProceedingJoinPoint proceedingJoinPoint){
        logger.info("环绕通知的目标方法名：{}", proceedingJoinPoint.getSignature().getName());
        Object obj = null;
        try {
            logger.info("执行方法前");
            //执行被切方法  obj 这执行方法返回
            obj = proceedingJoinPoint.proceed();
            logger.info("执行方法返回值：{}", JSON.toJSONString(obj));
            String targetName = proceedingJoinPoint.getTarget().getClass().getName();
            String methodName = proceedingJoinPoint.getSignature().getName();
            logger.info("obj : {}", JSON.toJSONString(obj));
            logger.info("targetName : {}", targetName);
            logger.info("methodName : {}", methodName);

        } catch (Throwable throwable) {
            logger.error("error", throwable);
        }
        return obj;
    }

}
